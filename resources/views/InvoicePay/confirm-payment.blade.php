@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
    <div class="container em-booking-content-box">
        <div class="row em-booking-content-main ml-0 mr-0">
            <div class="col-12 em-booking-content make_payment_wrapper pl-0 pr-0">
                <div class="col-12 pl-0 pr-0">
                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                        <h5>Confirm Payment</h5>
                            <div class="col-sm-12 em-field-main-set"> 
                                <div class="row m-0">
                                <input type="hidden" id="checkout_secret" value="{{Config::get('values.checkout_primary_key')}}">
                                <?php $address_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['address']);?>
                                    <div class="col-sm-12 em-field-main">
                                        <h4>Hi {{$data['customer_name']}},</h4>
                                    </div>

                                    <div class="col-sm-12 em-field-main">
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <span>Please confirm your requested payment of <strong>AED {{number_format($data['amount'],2,'.','')}}</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 em-field-main">
                                        <table class="payment_table">
                                            <tbody>
                                                <tr class="borderless">
                                                    <th colspan="2">Address</th>
                                                </tr>

                                                <tr>
                                                    <td colspan="2">{{$address_new}} , {{$data['area']}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Service Amount</th>
                                                    <td>AED {{number_format($data['amount'],2,'.','')}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Transaction Charge</th>
                                                    <td>AED {{number_format($data['transaction_charge'],2,'.','')}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Payable Amount</th>
                                                    <td>AED {{number_format($data['gross_amount'],2,'.','')}}</td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                    </div>
									
									<div class="col-sm-12 em-field-main pl-0 pr-0"> 
										<div class="col-sm-12 em-extra-ser-set">
											<p>Payment Mode</p>
											<div class="row" id="payment_div">
												<input type="hidden" id="inv_payment_mode" name="inv_payment_mode">
												<input type="hidden" id="apple_merchant_id" value="{{Config::get('values.apple_merchant_id')}}">
												<!--<input type="hidden" id="phonetamarapay" name="phonetamarapay" value="{{session('phone')}}">-->
												<style>
												.n-upi-set.em-extra-ser-img img { width: 80%; margin: 0 auto;}

												</style> 
												
												<div class="col-lg-4 col-sm-6 col-6" id="card" onclick="getInvPayment(this.id)">
													 <div class="row ml-0 mr-0 em-extra-ser-tmb">
														<div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{asset('images/card-payment.jpg')}}" alt=""></div>
													</div>
												</div>
												
												
												<div class="col-lg-4 col-sm-6 col-6" id="applepay" onclick="getInvApplePayment(this.id)">
														<div class="row ml-0 mr-0 em-extra-ser-tmb" id="applepaytmb">
															<div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{asset('images/n-applepay.jpg')}}" alt=""></div>
															<!--<div class="col-sm-8 em-extra-ser-cont">
															<span id="gpaybtn">Apple</span> <br>
																Pay 
															</div>-->
														</div>
												</div>

												<div class="col-lg-4 col-sm-6 col-6" id="gpay" onclick="getInvPayment(this.id)">
													<div class="row ml-0 mr-0 em-extra-ser-tmb">
														<div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{asset('images/n-gpay.jpg')}}" alt=""></div>
														<!--<div class="col-sm-8 em-extra-ser-cont">
														<span id="gpaybtn">Google</span> <br>
															Pay 
														</div>-->
													</div>
												</div>
												
												<!--<div class="col-lg-4 col-md-4 col-sm-4 col-4" id="tamara" onclick="getTamaraPayment(this.id)">
													<div class="row ml-0 mr-0 em-extra-ser-tmb ">
														<div class="n-upi-set em-extra-ser-img" style="padding-left: 5px !important; padding-right: 5px !important;"><img src="{{asset('images/n-tamara.jpg')}}" alt=""></div>
													</div> 
												</div>-->
												
											</div>
										</div>
									</div>
                                    
                                </div> 
                            </div>
                        </div>
                    </div>  
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <a class="cursor" href="{{URL::to('house-cleaning-dubai')}}"><span class="em-back-arrow back-to1-btn" title="Home"></span></a>
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                            <button id="book_payment_button" class="text-field-button show6-step reset-button" type="button">Confirm Payment</button>
							<button type="button" id="applePayInvbtn" class="text-field-button" style="display:none;"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
<div class="col-sm-12 popup-main" id="payment_popup">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
          <form id="payment-form" method="POST" action="{{url('make-pay-checkout')}}">
               {{ csrf_field() }}
                    <h5 class="">Make Payment <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                         <div class="row m-0">
                            <input type="hidden" id="token_req" name="token_req" value=''>
                            <?php
                                    $pay_id = (1000 + $data['pay_details']['payment_id']);
                                    $name_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['customer_name']);
                                    ?>
                                    <input type="hidden" id="customerId" name="customerId" value="">

										<input type="hidden" name="order_id" id="order_id" value="<?php echo $data['pay_details']['payment_id'];?>"/>
										<input type="hidden" name="amount" id="amount" value="<?php echo number_format($data['gross_amount'],2,'.','');  ?>"/>
										<input type="hidden" name="currency" value="AED"/>                        
										<input type="hidden" name="language" value="EN"/>
                                        <input type="hidden" id="retry_online" name="retry_online" value="invoice">                                         
										<input type="hidden" name="billing_zip" value=""/>
										<input type="hidden" name="billing_country" value="UAE"/>
										<input type="hidden" name="customer_name" id="billing_name" value="<?php echo $name_new; ?>"/>
										<input type="hidden" name="billing_tel" id="billing_tel" value="<?php echo $data['phone_number']; ?>"/>
										<input type="hidden" name="customer_email" id="billing_email" value="<?php echo $data['customer_email']; ?>"/>
										<input type="hidden" name="billing_address" id="billing_address" value="<?php echo $address_new; ?>"/>
										<input type="hidden" name="billing_state" id="billing_state" value="Dubai"/>
										<input type="hidden" name="billing_city" id="billing_city" value="Dubai"/>
										 <?php if(isset($_GET['mobile'])) 
                                   $mobileParam = '?mobile=active';
                              ?>
                              <?php if(!isset($_GET['mobile'])) 
                                   $mobileParam = '';
                              ?>
                              <input type="hidden" name="mobile_view" id="mobile_view" value="{{$mobileParam}}"/>
                                <div class="one-liner">
                                <div class="card-frame">
                                </div>                               
                                </div>
                         </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <button id="pay-button" disabled>
                            PAY AED {{number_format($data['gross_amount'],2,'.','')}}   </button>   
                                                 </div>  
               </form>    
          </div>
     </div>
</div><!--popup-main end-->

<div class="col-sm-12 popup-main" id="payment_popup_applepay">
	<div class="row min-vh-100 d-flex flex-column justify-content-center">
		<div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
			<h5 class="">Make Payment <span class="em-forgot-close-btn close-pay-poup"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
			<div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
				<div class="row m-0">
					<input type="hidden" name="order_id" id="ap_order_id" value="<?php echo $data['pay_details']['payment_id'];?>">
					<input type="hidden" name="amount" id="ap_amount" value="<?php echo number_format($data['gross_amount'],2,'.','');  ?>">
					<input type="hidden" name="customer_name" id="ap_billing_name" value="<?php echo $name_new; ?>">
					<input type="hidden" name="customer_tel" id="ap_billing_tel" value="<?php echo $data['phone_number']; ?>">
					<input type="hidden" name="customer_email" id="ap_billing_email" value="<?php echo $data['customer_email']; ?>">
					<input type="hidden" name="customer_address" id="ap_billing_address" value="<?php echo $address_new; ?>">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
				<button type="button" id="applePaybtn1"></button>
			</div> 
		</div>
	</div>
</div><!--popup-main end-->
<style>
.make_payment_wrapper {
    max-width: 600px;
    margin: 0px auto;
}
.make_payment_wrapper form{
    width: 100%;
}
table.payment_table {
    width: 100%;
    border: 1px solid #eee;
}
table.payment_table td, table.payment_table th {
    padding: 6px 10px;
}
.reset-button {
    width: 100% !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
#applePayInvbtn {  
	width: 150px;  
	height: 45px;  
	display: none;   
	border-radius: 5px;    
	margin-left: auto;
	margin-right: auto;
	background-image: -webkit-named-image(apple-pay-logo-white); 
	background-position: 50% 50%;
	background-color: black;
	background-size: 60%; 
	background-repeat: no-repeat;  
}
</style>

@endsection
@push('scripts')
<script src="https://cdn.checkout.com/js/framesv2.min.js"></script>
<script async src="https://pay.google.com/gp/p/js/pay.js"></script>
<script src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
<script>
var debug = true;
var merchantIdentifier = $('#apple_merchant_id').val();
var checkoutpkey = $('#checkout_secret').val();

if (window.ApplePaySession)
{
	//var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
	var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
	promise.then(function (canMakePayments) {
		if (canMakePayments) {
			///document.getElementById("applePaybtn").style.display = "block";
			logit('hi, I can do ApplePay');
		} else {
			logit('ApplePay is possible on this browser, but not currently activated.');
		}
	}); 
} else {
	logit('ApplePay is not available on this browser');
}

document.getElementById("applePayInvbtn").onclick = function(evt) {
	$("#payment_popup_applepay").hide();
	var ap_amount = $("#ap_amount").val();
	
	var runningAmount 	= ap_amount;
	var runningPP		= 0;
	getShippingCosts('domestic_std', true);
	var runningTotal	= function() { return runningAmount + runningPP; }
	var shippingOption = "";
 
	var subTotalDescr	= "Payment against Cleaning Service";
 
	function getShippingOptions(shippingCountry)
	{
		logit('getShippingOptions: ' + shippingCountry );
		if( shippingCountry.toUpperCase() == "AE" ) {
			shippingOption = [{label: 'Standard Shipping', amount: getShippingCosts('domestic_std', true), detail: '3-5 days', identifier: 'domestic_std'},{label: 'Expedited Shipping', amount: getShippingCosts('domestic_exp', false), detail: '1-3 days', identifier: 'domestic_exp'}];
		} else {
			shippingOption = [{label: 'International Shipping', amount: getShippingCosts('international', true), detail: '5-10 days', identifier: 'international'}];
		}
	}
 
	function getShippingCosts(shippingIdentifier, updateRunningPP )
	{
		var shippingCost = 0;
		
			 switch(shippingIdentifier) {
		case 'domestic_std':
			shippingCost = 0;
			break;
		case 'domestic_exp':
			shippingCost = 0;
			break;
		case 'international':
			shippingCost = 0;
			break;
		default:
			shippingCost = 0;
		}
	
		if (updateRunningPP == true) {
			runningPP = shippingCost;
		}
			
		logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost +"|"+ runningPP );
		
		return shippingCost;
	}

	var paymentRequest = {
	   currencyCode: 'AED',
	   countryCode: 'AE',
	   requiredShippingContactFields: ['postalAddress'],
	   lineItems: [{label: subTotalDescr, amount: runningAmount }, {label: 'P&P', amount: runningPP }],
	   total: {
		  label: 'Elitemaids',
		  amount: runningTotal()
	   },
	   supportedNetworks: ['amex', 'masterCard', 'visa' ],
	   merchantCapabilities: [ 'supports3DS', 'supportsCredit', 'supportsDebit' ]
	};

	var session = new ApplePaySession(1, paymentRequest);

	// Merchant Validation
	session.onvalidatemerchant = function (event) {
		logit(event);
		var promise = performValidation(event.validationURL);
		promise.then(function (merchantSession) {
			session.completeMerchantValidation(merchantSession);
		}); 
	}


	function performValidation(valURL) {
		return new Promise(function(resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var data = JSON.parse(this.responseText);
				logit(data);
				resolve(data);
			};
			xhr.onerror = reject;
			xhr.open('GET', 'https://elitemaids.emaid.info/applepay/apple_pay_comm.php?u=' + valURL);
			// xhr.open('GET', 'https://elitemaids.emaid.info/applepay/sandbox/apple_pay_comm.php?u=' + valURL);
			xhr.send();
		});
	}

	session.onshippingcontactselected = function(event) {
		logit('starting session.onshippingcontactselected');
		logit('NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
		logit(event);
		
		getShippingOptions( event.shippingContact.countryCode );
		
		var status = ApplePaySession.STATUS_SUCCESS;
		var newShippingMethods = shippingOption;
		var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
		var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
		
		session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems );
	}

	session.onshippingmethodselected = function(event) {
		logit('starting session.onshippingmethodselected');
		logit(event);
		
		getShippingCosts( event.shippingMethod.identifier, true );
		
		var status = ApplePaySession.STATUS_SUCCESS;
		var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
		var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
		
		session.completeShippingMethodSelection(status, newTotal, newLineItems );
		
		
	}

	session.onpaymentmethodselected = function(event) {
		logit('starting session.onpaymentmethodselected');
		logit(event);
		
		var newTotal = { type: 'final', label: 'Elitemaids', amount: runningTotal() };
		var newLineItems =[{type: 'final',label: subTotalDescr, amount: runningAmount }, {type: 'final',label: 'P&P', amount: runningPP }];
		
		session.completePaymentMethodSelection( newTotal, newLineItems );
		
		
	}

	session.onpaymentauthorized = function (event) {

		logit('starting session.onpaymentauthorized');
		logit('NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
		logit(event.payment.token);
		var eventToken = JSON.stringify(event.payment.token.paymentData);
		
		var ap_amount = $("#ap_amount").val();
		var ap_billing_name = $("#ap_billing_name").val();
		var ap_billing_tel = $("#ap_billing_tel").val();
		var ap_billing_email = $("#ap_billing_email").val();
		var ap_order_id = $("#ap_order_id").val();
		$.ajax({
			method: 'post',
			url: '{{url('apple-invoice-process-pay')}}',
			data:{'paymentToken':eventToken,'ap_order_id':ap_order_id,'ap_amount':ap_amount,'ap_billing_name':ap_billing_name,'ap_billing_tel':ap_billing_tel,'ap_billing_email':ap_billing_email,_token:'{{csrf_token()}}'},
			
			success: function (result) {
					console.log(result);
					if (result.status == 'success') {
						$(".preloader").fadeOut(1000);
						session.completePayment(ApplePaySession.STATUS_SUCCESS);
						window.location.href = '{{url('apple-invoice-payment-success')}}'+'/'+result.refId+'/'+result.id;
					} else {
						$(".preloader").fadeOut(1000);
						session.completePayment(ApplePaySession.STATUS_FAILURE);
						$("#payment_button").hide();
						$("#success_pay_message").text("Something went wrong. Try again.");
						$("#paymenterrormessagepopup").show();
					}
			}
		});
	}

	function sendPaymentToken(paymentToken) {
		return new Promise(function(resolve, reject) {
			logit('starting function sendPaymentToken()');
			
			logit("this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
			logit("defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
			if ( debug == true )
			resolve(true);
			else
			reject;
		});
	}

	session.oncancel = function(event) {
		$("#book_payment_button").hide();
		$("#success_pay_message").text("Cancelled. Try again.");
		$("#paymenterrormessagepopup").show();
	}

	session.begin();

};

function logit( data ){
	
	if( debug == true ){
		console.log(data);
	}	

};
	
var payButton = document.getElementById("pay-button");

var form = document.getElementById("payment-form");
Frames.init($('#checkout_secret').val());
Frames.addEventHandler(
  Frames.Events.CARD_VALIDATION_CHANGED,
  function (event) {
    payButton.disabled = !Frames.isCardValid();
  }
);
Frames.addEventHandler(
Frames.Events.CARD_TOKENIZED,
function (event) {
$('#token_req').val(event.token);
$("#payment-form").submit();
}
);
if(form) {
form.addEventListener("submit", function (event) {
    event.preventDefault();
    Frames.submitCard();
});
}
$(document).ready(function() {
    $('.logo').addClass('logo_left');
    if($('#customerId').val() == 0 ) {
        var customerId = localStorage.getItem('customerId');
        if ((customerId) && customerId != '' && customerId != 0 ) {
            $('.login').hide();
            $('.logout').show();
            $('.userPersonel').show();
            $('.make_pay_head').show();
            $('#customerId').val(customerId);
        } else{
            $('.login').show();
            $('.logout').hide();
            $('.userPersonel').hide();
            $('.make_pay_head').hide();
        }
    } else {
        localStorage.setItem('customerId',$('#customerId').val());
        $('.login').hide();
        $('.logout').show();
        $('.userPersonel').show();
        $('.make_pay_head').show();
    }  
});

$('#book_payment_button').click(function() {
	var payid = $("#inv_payment_mode").val();
	if(payid != "")
	{
		if(payid == 'card')
		{
			$('#payment_popup').show(500);
		} else if(payid == 'gpay')
		{
			var total_amount_payable = $("#amount").val();
			var refId = $("#order_id").val();
			var email = $("#billing_email").val();
			var name = $("#billing_name").val();
			const baseRequest = {
			  apiVersion: 2,
			  apiVersionMinor: 0
			};
			
			const tokenizationSpecification = {
			  type: 'PAYMENT_GATEWAY',
			  parameters: {
				'gateway': 'checkoutltd',
				'gatewayMerchantId': checkoutpkey
			  }
			};
			
			const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];
			
			const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];
			
			const baseCardPaymentMethod = {
			  type: 'CARD',
			  parameters: {
				allowedAuthMethods: allowedCardAuthMethods,
				allowedCardNetworks: allowedCardNetworks
			  }
			};
			
			const cardPaymentMethod = Object.assign(
			  {tokenizationSpecification: tokenizationSpecification},
			  baseCardPaymentMethod
			);
			
			// const paymentsClient = new google.payments.api.PaymentsClient({environment: 'TEST'});
			const paymentsClient = new google.payments.api.PaymentsClient({environment: 'PRODUCTION'});
			
			const isReadyToPayRequest = Object.assign({}, baseRequest);
			isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];
			
			paymentsClient.isReadyToPay(isReadyToPayRequest)
			.then(function(response) {
			  if (response.result) {
				const button =
					paymentsClient.createButton({onClick: () => console.log('TODO: click handler'),
					allowedPaymentMethods: []}); // make sure to provide an allowed payment method
				//document.getElementById('gpaybtn').appendChild(button);
			  }
			})
			.catch(function(err) {
				$(".preloader").fadeOut(1000);
				$("#success_pay_message").text("Something went wrong. Try again.");
				$("#paymenterrormessagepopup").show();
			});
			
			const paymentDataRequest = Object.assign({}, baseRequest);
			paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
			paymentDataRequest.transactionInfo = {
			  totalPriceStatus: 'FINAL',
			  totalPrice: total_amount_payable,
			  currencyCode: 'AED',
			  countryCode: 'AE'
			};
			paymentDataRequest.merchantInfo = {
			  merchantName: 'Elite Advance Building Cleaning LLC',
			  merchantId: 'BCR2DN4TXLQ3FHS6'
			};
			
			paymentsClient.loadPaymentData(paymentDataRequest).then(function(paymentData){
			  paymentToken = paymentData.paymentMethodData.tokenizationData.token;
			  $.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					method: 'post',
					url: '{{url('make-invoice-gpay-payment')}}',
					data:{'amount':total_amount_payable,'referenceId':refId,'paymentToken':paymentToken,'email':email,'name':name,_token:'{{csrf_token()}}'},
					success: function (result) { 
						if (result.status == 'success')
						{
							$(".preloader").fadeOut(1000);
							window.location.href = result.url;
						} else if (result.status == 'failed')
						{
							$(".preloader").fadeOut(1000);
							$("#success_pay_message").text(result.messages);
							$("#paymenterrormessagepopup").show();
						}
					}
				});
			}).catch(function(err){
				console.log(err);
				$(".preloader").fadeOut(1000);
				$("#success_pay_message").text("Something went wrong. Try again.");
				$("#paymenterrormessagepopup").show();
			});
		}
	} else {
		alert("Please select payment mode...");
	}
    
   });
   $('.em-forgot-close-btn').click(function(){
$('#payment_popup').hide(500);
});

function getInvPayment(id)
{
	$("#book_payment_button").show();
	$("#applePayInvbtn").hide();
   
    $('#inv_payment_mode').val(id);
    //$('#payment_mode-error').hide();
    if((id== 'card') || (id== 'gpay'))
    {
        $('#book_payment_button').val('Make Payment');
    } else if(id== 'tamara')
	{
        $('#book_payment_button').val('Make Payment');
	}
}

function getInvApplePayment(id)
{
	$('#payment_mode').val(id);
	if (window.ApplePaySession)
	{
		//var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
		var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
		promise.then(function (canMakePayments) {
			if (canMakePayments) {
				$(".preloader").fadeOut(1000);
				$("#book_payment_button").hide();
				$("#applePayInvbtn").show();
			} else {
				$(".preloader").fadeOut(1000);
				$("#book_payment_button").hide();
				$("#applePayInvbtn").hide();
				$("#success_pay_message").text('ApplePay is possible on this browser, but not currently activated.');
				$("#paymenterrormessagepopup").show();
				return false;
			}
		}); 
	} else {
		$(".preloader").fadeOut(1000);
		$("#book_payment_button").hide();
		$("#applePayInvbtn").hide();
		$("#success_pay_message").text('ApplePay is not available on this browser');
		$("#paymenterrormessagepopup").show();
		return false;
	}
}

$('#payment_div .em-extra-ser-tmb').on('click', function() {
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
});

function closeerrorpop()
{
	window.location.href = '{{url('house-cleaning-dubai')}}';
}

function closepaymenterrorpop()
{
	$("#success_pay_message").text("");
	$("#paymenterrormessagepopup").hide();
}
</script>
@endpush