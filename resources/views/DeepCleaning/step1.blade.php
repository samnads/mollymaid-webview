<?php if(!isset($_GET['mobile'])) { ?>

<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">

    <div class="col-md-12 col-sm-12 em-step-head em-head no-left-right-padding">

        <h2>Book Your Service</h2>

        <ul>

            <li class="active"><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

        </ul>

    </div>

</div>

<?php } ?>

<form id="step1DeeCleaning" method="post">

{{ csrf_field() }}

<input type="hidden" value="" id="noOfHours" name="noOfHours">

    <div class="col-12 em-booking-content-set pl-0 pr-0">

        <div class="row em-booking-content-set-main ml-0 mr-0">

            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">

                <div class="col-12 step1 pl-0 pr-0">

                    @include('common.services')

                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">

                        <div class="col-sm-12 book-det-cont-set-main deep-cleaning-service  ser-cont3 ser-cont" id="cont3" style="display:block;">

                            <h5>Deep Cleaning Services</h5>

                                <div class="col-sm-12 deep-cleaning-home pl-0 pr-0">

                                    <div class="col-sm-12 em-field-main-set"> 

                                            <div class="row m-0">

                                                    <div class="col-sm-6 em-field-main">

                                                        <p>Deep cleaning for</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                           <input type="hidden" id="service_category" value="3">



                                                            <ul class="clearfix" id="service_for_secondr">

                                                                <li class="selected" id="20">HOME</li>

                                                                <li class="show-deep-for-office-btn" id="21">OFFICE</li>

                                                            </ul>

                                                        </div>

                                                    </div>

                                                    <div class="col-sm-6 em-field-main" id="aprt_fur">

                                                        <p>Deep cleaning for</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                        <input type="hidden" id="furnished_hidden" value="1">

                                                            <ul class="clearfix" id="furnished_unfurnished">

                                                                <li class="selected" id="1">Furnished</li>

                                                                <li class="" id="2">Unfurnished</li>

                                                            </ul>

                                                        </div>

                                                    </div>

                                                    <div class="col-sm-6 em-field-main" style="display:none" id="villa_fur">

                                                        <p>Deep cleaning for</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                           <ul class="clearfix" id="furnished_unfurnished3">

                                                                <li class="selected" id="3">Furnished</li>

                                                                <li class="" id="4">Unfurnished</li>

                                                            </ul>

                                                        </div>

                                                    </div>

                                            </div> 

                                        </div>

                                    <!--<div class="col-sm-12 em-often-section-box pt-1">

                                        <div class="col-sm-12 how-often">

                                            <p>What is your building type?</p>

                                            <ul>

                                                    <li>

                                                        <input id="building-type3" value="3" name="building" class="buildingType" checked="" type="radio">

                                                        <label for="building-type3"> <span></span> &nbsp; Apartment</label>

                                                    </li>

                                                    

                                                    

                                                    <li>

                                                        <input id="building-type4" value="4" name="building" class="buildingType" type="radio">

                                                        <label for="building-type4"> <span></span> &nbsp; Villa</label>

                                                    </li>

                                            </ul>

                                        </div>

                                    </div>-->

                                    

                                    

                                    

                                    <div class="col-sm-12 em-field-main-set"> 

                                        <div class="row m-0">

                                        

                                              

                                              

                                              

                                              <div class="col-sm-6 em-often-section-box pt-1">

                                        <div class="col-sm-12 how-often p-0">

                                            <p>What is your building type?</p>

                                            <ul>

                                                    <li>

                                                        <input id="building-type3" value="3" name="building" class="buildingType" checked="" type="radio">

                                                        <label for="building-type3"> <span></span> &nbsp; Apartment</label>

                                                    </li>

                                                    

                                                    

                                                    <li>

                                                        <input id="building-type4" value="4" name="building" class="buildingType" type="radio">

                                                        <label for="building-type4"> <span></span> &nbsp; Villa</label>

                                                    </li>

                                            </ul>

                                        </div>

                                    </div>

                                    

                                    

                                    

                                    

                                        

                                                <div class="col-sm-6 em-field-main" id="apart_div">

                                                        <p>How many bedroom home you have?</p>

                                                        <div class="col-12 em-text-field-main em-box-1-5 how-many-bedroom pl-0 pr-0">

                                                        <input type="hidden" id="bedroom_count" value="STUDIO">

                                                            <ul class="clearfix border-0"  id="bedroom_ul">

                                                                <li class="border border-right-0 selected">STUDIO</li>

                                                                <li class="border border-right-0">1</li>

                                                                <li class="border border-right-0">2</li>

                                                                <li class="border border-right-0">3</li>

                                                                <li class="border border-right-0">4</li>

                                                                <li class="border">5</li>

                                                            </ul>

                                                        </div>

                                                </div>

                                                <div class="col-sm-6 em-field-main" id="villa_div" style="display:none;">

                                            <p>How many room you have?</p>

                                            <div class="col-12 em-text-field-main number-of-hours em-box-8 pl-0 pr-0">

                                            <input type="hidden" id="villa_id" value="1">

                                                    <ul class="clearfix border-0" id="villa_ul">

                                                        <li class="border border-right-0 selected">1</li>

                                                        <li class="border border-right-0">2</li>

                                                        <li class="border border-right-0">3</li>

                                                        <li class="border border-right-0">4</li>

                                                        <li class="border">5</li>

                                                        <!-- <li>6</li>

                                                        <li>7</li>

                                                        <li>8</li>

                                                        <li>9</li> -->

                                                        

                                                    </ul>

                                            </div>

                                        </div>    

                                                

                                        </div> 

                                    </div> 

                                </div><!--deep-cleaning-home end-->

                                <div class="col-sm-12 deep-cleaning-office pl-0 pr-0">

                                        <div class="col-sm-12 em-field-main-set"> 

                                            <div class="row m-0">

                                                    <div class="col-sm-6 em-field-main">

                                                        <p>Deep cleaning for</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                            <ul class="clearfix" id="service_for">

                                                                <li class="show-deep-for-home-btn" id="3">HOME</li>

                                                                <li class="selected" id="4">OFFICE</li>

                                                            </ul>

                                                        </div>

                                                    </div>

                                                      <div class="col-sm-6 em-field-main">

                                                        <p>Deep cleaning for</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                            <ul class="clearfix" id="furnished_unfurnished2">

                                                                <li class="selected" id="5">Furnished</li>

                                                                <li class="" id="6">Unfurnished</li>

                                                            </ul>

                                                        </div>

                                                    </div> 

                                        </div> 

                                        </div>

                                        

                                        <div class="col-sm-12 em-field-main-set"> 

                                            <div class="row m-0">          

                                                    <!--<div class="col-sm-6 em-field-main">

                                                        <p>Do you require Floor scrubbing and polishing?</p>

                                                        <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">

                                                            <ul class="clearfix" id="floor_scrubbing_ul">

                                                                <li id="0">NO</li>

                                                                <li class="selected" id="1">YES, PLEASE</li>

                                                            </ul>

                                                        </div>

                                                    </div>-->

                                                    <div class="col-sm-6 em-time-section-box pt-3">

                                                        <p>What is the size of your office?</p>

                                                        <select class="js-select2" id="officesqure" onChange="amountCalculation();">

                                                            <option>Select Square Feet</option>

                                                            @for ($i = 600; $i <= 25000; $i+=10)

                                                            <option value="{{$i}}" @if($i == '600') selected @endif>{{$i}} square feet</option>

                                                            @endfor

                                                        </select>

                                                    </div>

                                            </div> 

                                        </div>

                                       

                                       

                                

                                </div><!--deep-cleaning-office end-->

                        </div>

                        <div class="col-sm-12 em-terms-and-condition-main"> 

                        <h6>What to expect from the service?</h6>

                            <div class="col-sm-12 em-terms-and-condition">

                                <ul class="">

                                    <li>

                                        <div class="em-num">1</div>

                                        <div class="em-num-cont">
                                        <p>Please note that we will do our very best to arrive on the given time nonetheless due to traffic conditions please anticipate maximum 30 minutes delay. Please note that we will complete the full hours of booking upon arrival. </p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <li>

                                        <div class="em-num">2</div>

                                        <div class="em-num-cont">
                                        <p>Any cancellations with less than 24 hours’ notice is non-refundable.</p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <li>

                                        <div class="em-num">3</div>

                                        <div class="em-num-cont">
                                        <p>For any questions and clarifications on your booking please whatsApp us at <a target="_blank" href="https://api.whatsapp.com/send?phone=971543502510">+971 543502510</a> or email us at <a href="mailto:office@example.com">office@example.com</a></p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <li>

                                        <div class="em-num">4</div>

                                        <div class="em-num-cont">
                                        <p>
                                        Ideal for Move in / Move out clean and one time off deep cleaning<br>
                                        This service includes:
                                        Thorough dusting and vacuuming of entire space
                                        Deep Cleaning of all internal windows and window frames
                                        Grease removal from kitchen walls, and counters
                                        Thorough cleaning of all cabinetry and wardrobes
                                        Cleaning of balcony and balcony rails from inside.
                                        Thorough cleaning of all floors.
                                        Removal of grime build up in bathroom bath, toilet, shower heads and taps.
                                            <br>
                                        This service does not include:
                                        Cleaning Patio or Garage, outside window cleaning ( extra charges apply for this services) 
                                            <!-- Ideal for Move in / Move out clean and one time off deep cleaning, This service includes: Thorough dusting and vacuuming of entire space Deep Cleaning of all internal windows and window frames Grease removal from kitchen walls, and counters Thorough cleaning of all cabinetry and wardrobes Cleaning of balcony and balcony rails from inside. Thorough cleaning of all floors. Removal of grime build up in bathroom bath, toilet, shower heads and taps. This service does not include: Cleaning Patio or Garage, outside window cleaning ( extra charges apply for this services) -->
                                        </p>
                                        </div>

                                        <div class="clear"></div>

                                    </li>

                                    <!-- <li>

                                        <div class="em-num">5</div>

                                        <div class="em-num-cont"><p>Cleaning of balcony and balcony rails.</p></div>

                                        <div class="clear"></div>

                                    </li> 

                                    <li>

                                        <div class="em-num">6</div>

                                        <div class="em-num-cont"><p>Thorough cleaning of all floors.</p></div>

                                        <div class="clear"></div>

                                    </li>		

                                    <li>                                        

                                        <div class="em-num">7</div>

                                        <div class="em-num-cont"><p>Scale removal from bathroom tiles, bath, toilet, shower heads and taps.</p></div> 

                                        <div class="clear"></div>  

                                    </li>

                                    <li>                                        

                                        <div class="em-num">8</div>

                                        <div class="em-num-cont"><p>Remove the dust and grime that builds up around the appliances.</p></div> 

                                        <div class="clear"></div>  

                                    </li>-->

                                </ul>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 em-booking-det-cont em-next-btn">

                        <div class="row em-next-btn-set ml-0 mr-0">

                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">

                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->

                                <div class="col-12 sp-total-price-set pl-0 pr-0">

                                    Total<br>

                                    <span>AED <b class="mobile_net_p"></b></span> 

                                </div>

                                

                            

                            </div>

                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">

                                <input value="Next" class="text-field-button show2-step-q" id="submit_button" type="button">

                            </div>

                        </div>

                    </div>

                </div>

            </div>

             

            @include('includes.steps.summary')



        </div>

    </div>

</form>
<style>
#applePaybtn {  
	width: 150px;  
	height: 45px;  
	display: none;   
	border-radius: 5px;    
	margin-left: auto;
	margin-right: auto;
	background-image: -webkit-named-image(apple-pay-logo-white); 
	background-position: 50% 50%;
	background-color: black;
	background-size: 60%; 
	background-repeat: no-repeat;  
}
</style>

@push('scripts')

<script>

   $('.show2-step-q').click(function(){

        $('#deepCleaning1').fadeOut(300);

        $('#commonStep2').fadeIn(300);

        $('.date_time').show();

        $('.timeSelection li:first').trigger("click");

        $('header .logo').addClass('logo_left');

        $("html").animate({ scrollTop: 0 }, "slow");

    });

</script>

@endpush

