@extends('layouts.app')

@section('title') {{'Deep Cleaning Services Dubai'}} @endsection

@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          <input type="hidden" id="all_amount" value="{{serialize(@$allAmount)}}">
          <input type="hidden" id="number_hours" value="0">		  <input type="hidden" id="serviceNamePay" value="DeepCleaning">
               @include('DeepCleaning.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script  type="text/javascript" src="{{URL::asset('js/select2.full.min.js')}}"></script>
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(".buildingType").click(function(){ 
var selValue = $("input[type='radio']:checked").val();
if (selValue == '3') {
    $('#apart_div').show();
    $('#villa_div').hide();
    $('#aprt_fur').show();
    $('#villa_fur').hide();
    // $('#furnished_hidden').val(1);
   
} else {
    $('#apart_div').hide();
    $('#villa_div').show();
    $('#villa_fur').show();
    // $('#furnished_hidden').val(3);
    $('#aprt_fur').hide();

}
amountCalculation();
});
$('#furnished_unfurnished li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();
}); 
$('#furnished_unfurnished2 li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();
}); 
$('#furnished_unfurnished3 li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();

}); 
$('#floor_scrubbing_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#floor_id').val(this.id);
     amountCalculation();
}); 
$('#floor_scrubbing_ul1 li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#floor_id').val(this.id);
     amountCalculation();
}); 
$('#bedroom_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
});
$('#villa_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
}); 
$('#officesqure_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     var target=  $(this).children('.tick-text').attr("id");
     $('#officesqure').val(target);
     amountCalculation();

}); 
$(document).ready(function() {
    $('#noOfHours').val(4);
	getAvailability();
    amountCalculation();
    $(".js-select2").select2();

});
function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
}
var ul = document.getElementById('service_for_secondr');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#service_category').val(target.id);
    amountCalculation();
};
var ul = document.getElementById('service_for');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#service_category').val(target.id);
    amountCalculation();
};
var ul = document.getElementById('villa_ul');
ul.onclick = function(event) { 
    var target = getEventTarget(event);
    $('#villa_id').val(target.innerHTML);
    amountCalculation();
};
var ul = document.getElementById('bedroom_ul');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#bedroom_count').val(target.innerHTML);
    amountCalculation();
};



function amountCalculation()
{
    var furnished =  $('#furnished_hidden').val();
    var category = $('#service_category').val();
    if(category == 4) {
        var subCategory = 0;
        var bedroom =  1; 
        if (furnished == 2 ||furnished == 4  ) {
            furnished = 6;
        } else if (furnished == 1 || furnished == 3 ) {
            furnished = 5;
        }
    } else {
        var subCategory =  $("input[name='building']:checked").val();
        if(subCategory == 4) {
            var bedroom = $('#villa_id').val();
            var furnishedFirst =  $('#furnished_hidden').val();
            // if (furnishedFirst == 2) {
            //     furnished = 4;
            // } else if (furnishedFirst == 1) {
            //     furnished = 3;
            // }
             if (furnishedFirst == 2 || furnishedFirst == 5 ) {
                furnished = 4;
            } else if (furnishedFirst == 1 || furnishedFirst == 6) {
                furnished = 3;
            } 
        } else {
            var bedroom = $('#bedroom_count').val();
            var furnishedSecond=  $('#furnished_hidden').val();
            // if (furnishedSecond == 4) {
            //     furnished = 2;
            // } else if (furnishedSecond == 3) {
            //     furnished = 1;
            // }
             if (furnishedSecond == 4 ||  furnishedSecond == 6) {
                furnished = 2;
            } else if (furnishedSecond == 3 || furnishedSecond == 5) {
                furnished = 1;
            }
        }
    }
    var is_scrubbing = 0;
    var serviceId = $('#service_id').val();
    var selectedArray = [];
    var coupon = $('.couponClass').val();
    selectedArray.push({service_id : serviceId,category_id : category,sub_category_id : subCategory,name : bedroom,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    var intialArray = $('#all_amount').val();
    // console.log(selectedArray);
    // result2 =JSON.parse(array2);
 
    $.ajax({
          method: 'post',
          url: '{{url('calculate-load')}}',
          data:{'selectedArray':selectedArray,'intialArray':intialArray,'officesqure':$('#officesqure').val(),'category':category,_token:'{{csrf_token()}}'},  
         
          success: function (result) {
               if (result.status == 'success') {
                   $('.total_p').text(result.data['amount']);
                   if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                        $('.vat_p').text(result.data['vat_amount']);
                        $('.net_p').text(result.data['totalAmount']);
                        $('.mobile_net_p').text(result.data['totalAmount']);
                        $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }
                   $('#arrayVal').val(result.data['arrayVal']);
               }
          }
     }); 
}
    function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
    }



function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}
</script>
@endpush