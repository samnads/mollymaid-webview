@extends('layouts.app')
@section('title') {{'Edit Address'}} @endsection
@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
               <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                    <div class="col-sm-12 em-step-head no-left-right-padding">
                    <?php if(@$data['customerAddress']['customer_address_id'] && $data['customerAddress']['customer_address_id'] != '') {
                        $addOrEdit = "Update Your Address";
                    } else {
                        $addOrEdit = "Add New Address";
                    }?>
                         <h2>{{$addOrEdit}}</h2>
                    </div>
               </div>
                <form id="address_form" method="post" action="{{url('save-address-data')}}">
                {{ csrf_field() }}
                <input type="hidden" id="address_id" name="address_id" value="{{@$data['customerAddress']['customer_address_id']}}">
                <input type="hidden" id="customerId" name="customerId" value="{{@$data['customerId']}}">
                <div class="col-sm-12 my-account-main">
                    <div class="col-sm-12 em-booking-det-cont my-account-content-main pt-3">
                        <h5>Your Address Details</h5>
                        <div class="col-sm-12 em-field-main-set p-0"> 
                            <div class="row">
                                    <div class="col-sm-6 em-field-main">
                                        <p>Area</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                        <select name="area" type="text" placeholder="" class="el-text-field">
                                            <option value="">-- Select Area --</option>
                                            @foreach($data['area'] as $ar)
                                                <option @if(@$data['customerAddress']['area_id']==$ar['area_id']) selected @endif value="{{$ar['area_id']}}">{{$ar['area_name']}}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 em-field-main">
                                        <p>Address Details</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <input name="address" value="{{@$data['customerAddress']['customer_address']}}" class="text-field" type="text">
                                        </div>
                                    </div>
                            </div> 
                        </div>
                        <div class="col-sm-12 em-field-main-set map-view">
                            <input type="hidden"  class="us3-lat" name="latitude" id="latitude" value="{{@$data['customerAddress']['latitude']}}">
                            <input type="hidden"  class="us3-lon" name="longitude"id="longitude" value="{{@$data['customerAddress']['longitude']}}">
                            <input type="hidden" class="us3-radius" />

                            <div class="map-search"><input name="selected_address" placeholder="Address" class="text-field us3-address" type="text"></div>
                            <div class="us3" style="width: 550px; height: 400px;"></div>
                            <!-- <iframe id="us3" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" height="200px" frameborder="0"></iframe> -->
                        </div>
                        <div class="col-sm-12 em-field-main add-new-adrs pt-0 pl-0 pr-0">
                            <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often address-option pl-0 pr-0 mb-0">
                                <p>Address Option</p>
                                <ul class="pb-0">
                                        <li class="home">
                                        <input id="address1" @if(@$data['customerAddress']['address_category'] == 'HO') checked @endif value="HO" name="address_opt" class="" type="radio">
                                            <label for="address1"> <span></span> &nbsp; Home</label>
                                        </li>
                                        
                                        
                                        <li class="office">
                                        <input id="address2" @if(@$data['customerAddress']['address_category'] == 'OF') checked @endif value="OF" name="address_opt" class="" type="radio">
                                            <label for="address2"> <span></span> &nbsp; Office</label>
                                        </li>
                                        
                                        
                                        <li class="other">
                                        <input id="address3" @if(@$data['customerAddress']['address_category'] == 'OT') checked @endif value="OT" name="address_opt" class="" type="radio">
                                            <label for="address3"> <span></span> &nbsp; Other</label>
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                        <div class="col-md-6 col-sm-12 col-6 em-next-btn-left pl-0 pr-0">
                        <a class="cursor" href="{{URL::to('manage-address')}}"><span class="em-back-arrow back-to4-btn " title="My address list"></span></a>
                        </div>
                        <div class="col-sm-6 col-12 em-next-btn-right pl-0 pr-0">
                            <?php if(@$data['customerAddress']['customer_address_id'] && $data['customerAddress']['customer_address_id'] != '') {
                                $valueButton = "Update";
                            } else {
                                $valueButton = "Save";
                            }?>
                            <input value="{{$valueButton}}" class="text-field-button show6-step" type="submit">
                        </div>
                        </div>
                    </div>
               </div>
             </form>
          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5VeXXoHzVpT6ZWQN4Af5gibJYSPdoWRw&amp;libraries=places"></script>
<script type="text/javascript"  src="{{URL::asset('js/locationpicker.jquery.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('.logo').addClass('logo_left');
    if($('#latitude').val() =='' && $('#longitude').val() =='') {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    } else {
        locationPickr($('#latitude').val(),$('#longitude').val());
    }
});  
$.validator.addMethod("customemail",
        function(value, element) {
            return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        },
        "Please enter a valid email id. "
    );
$("#address_form").validate({
    ignore: [],
    rules: {
        area: {
            required:true,
        },
        address: {
                required:true,
        },
        address_opt: {
            required: true,
        },
    },
    messages: {
        area: "Please select your area.",
        address: "Please enter your address.",
        address_opt:"Please select address type.",
    },
    errorPlacement: function (error, element)
    {
        if(element.attr("name") == "area"){
               error.insertAfter(element.parent());
          }
          else if(element.attr("name") == "address"){
               error.insertAfter(element.parent());
          }
          else if(element.attr("name") == "address_opt"){
               error.insertAfter(element.parent().parent());
          }else{
               error.insertAfter(element);
          }
    },
    submitHandler: function (form,event) {
        document.getElementById('address_form').submit();
    }

});
$(document).ready(function() {
    $('select[name="area"]').select2();
    $(document.body).on("change",'select[name="area"]',function(){
        $(this).valid();
    });
});
</script>
@endpush