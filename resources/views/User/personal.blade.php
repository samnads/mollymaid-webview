@extends('layouts.app')
@section('title') {{'Personal Details'}} @endsection
@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
               <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                    <div class="col-sm-12 em-step-head no-left-right-padding">
                         <h2>Personal Details</h2>
                    </div>
               </div>
               <div class="col-sm-12 my-account-main">
                    <form id="edit_user_data_form" method="post" action="{{url('edit-user-data')}}">
                    {{ csrf_field() }}
                         <input type="hidden" value="{{@$data['customerId']}}" name="customerId">
                         <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                              <ul>
                              <li>
                                   <div class="col-sm-12 my-acc-cont-main p-0">
                                        <div class="my-acc-edit"><i class="fa fa-pencil"></i></div>
                                        <div class="col-sm-12 my-acc-que-cont p-0">Full Name</div>
                                        <div class="col-sm-12 my-acc-ans-cont p-0">
                                             <input name="fullName" class="text-field" type="text" value="{{@$data['name']}}" disabled>
                                        </div>
                                   </div>
                              </li>                             
                              <li>
                                   <div class="col-sm-12 my-acc-cont-main p-0">
                                        <div class="my-acc-edit"><i class="fa fa-pencil"></i></div>
                                        <div class="col-sm-12 my-acc-que-cont p-0">Mobile Number</div>
                                        <div class="col-sm-12 my-acc-ans-cont p-0">
                                             <input name="phone" class="text-field" type="tel" value="{{@$data['phone']}}" disabled>
                                        </div>
                                   </div>
                              </li>
                              <li>
                                   <div class="col-sm-12 my-acc-cont-main p-0">
                                        <div  class="my-acc-edit"><i class="fa fa-pencil"></i></div>
                                        <div class="col-sm-12 my-acc-que-cont p-0">Email ID</div>
                                        <div class="col-sm-12 my-acc-ans-cont p-0">
                                             <input name="email" class="text-field" type="text" value="{{@$data['emailId']}}" disabled> 
                                        </div>
                                   </div>
                              </li>
                              </ul>
                         </div>
                         <div class="col-12 em-booking-det-cont em-next-btn">
                              <div class="row em-next-btn-set ml-0 mr-0">
                                   <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                        <a class="cursor" href="{{URL::to('my-accounts')}}"><span class="em-back-arrow back-to1-btn" title="Home"></span></a>
                                   </div>
                                   <div class="col-sm-6 col-12 em-next-btn-right pl-0 pr-0">
                                        <input value="Update" class="text-field-button show6-step" type="submit">
                                   </div>
                              </div>
                         </div>
                    </form>
               </div>
          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script>
$(document).ready(function() {
    $('.logo').addClass('logo_left');
});  
function editClass(data)
{
     $(data).siblings().toggleClass("edit-details");
     $(data).siblings().removeAttr("disabled");

}
$( '.my-acc-edit' ).on( "click", function( event ) {
       $( event.target ).closest( "li" ).toggleClass( "edit-details" );
   $('.edit-details .text-field').removeAttr("disabled");
   });




$.validator.addMethod("customemail",
        function(value, element) {
            return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        },
        "Please enter a valid email id. "
    );
$("#edit_user_data_form").validate({
    ignore: [],
     rules: {
          fullName: {
               required:true,
          },
          phone: {
               required:true,
               minlength:{{Config::get('form.phone_min_digit')}},
               maxlength:{{Config::get('form.phone_max_digit')}},
               number:true,
          },
          email: {
               customemail:true,
               required: true,
          },
     },
    messages: {
          fullName: "Please enter your name.",
          email: "Please enter your email id.",
          phoneNumber:{
                required: "Please enter your phone number.",
                minlength: "Phone number must contain atleast {{Config::get('form.phone_min_digit')}} digits.",
                maxlength: "Only {{Config::get('form.phone_max_digit')}} digits is allowed.",
                number:"Only numbers are allowed."
            },
    },
    errorPlacement: function (error, element)
    {
    
        error.insertAfter(element.parent().parent());
        },
    submitHandler: function (form,event) {
     $('.text-field').removeAttr("disabled");

        document.getElementById('edit_user_data_form').submit();
    }
});
</script>
@endpush