<?php if(!isset($_GET['mobile'])) {
?>
<div id="" class="owl-carousel em-ser-scroll-thumb-main services-scroll">
     @php
    foreach($data['services'] as $key => $service){
          if(request()->is($service['url_slug']) || Request::segment(1) == $service['url_slug'] || (request()->route()->named('home') && $service['service_type_id'] == config('values.default_service_id'))){
               $active_service_id = $service['service_type_id'];
               $active_service_id_key = array_search($active_service_id, array_column($data['services'], 'service_type_id'));
               $data['services'] = array($active_service_id_key=> $data['services'][$active_service_id_key]) + $data['services'];
          }
     }
    @endphp
    @foreach ($data['services'] as $key => $service)
        <div class="item">
            <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img
                        src="{{ asset('images/' . $service['cleaning_icon'] . '?v=' . config('version.img')) }}"
                        alt=""></div>
                <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                    <p>{{ $service['service_type_name'] }}</p>
                    <a data-id="{{$service['service_type_id']}}" href="{{ URL::to($service['url_slug']) }}"> <input value="BOOK NOW"
                            class="{{ request()->is($service['url_slug']) ||Request::segment(1) == $service['url_slug'] ||(request()->route()->named('home') &&$service['service_type_id'] == config('values.default_service_id'))? 'text-field-button active': 'text-field-button' }}"
                            data-id="cont3" type="button" data-service-count="{{ $key }}"></a>
                </div>
            </div>
        </div>
    @endforeach

    <!--<div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/HouseCleaning.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>House Cleaning<br>Service</p>
                         <?php
                         $requestUrl = Request::url();
                         $houseArray = explode('/', $requestUrl);
                         ?>
                         <a href="{{ URL::to('house-cleaning-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('house-cleaning-dubai') || in_array('house-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont1" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/Disinfection.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Disinfection<br>Service</p>
                         <a href="{{ URL::to('disinfection-service-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('disinfection-service-dubai') || in_array('disinfection-service-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont2" type="button"></a>

                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/DeepCleaningGr.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Deep/ Move in <br>Move out Cleaning</p>
                         <a href="{{ URL::to('deep-cleaning-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('deep-cleaning-dubai') || in_array('deep-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont3" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/Carpet.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Carpet<br>Cleaning</p>
                         <a href="{{ URL::to('carpet-cleaning-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('carpet-cleaning-dubai') || in_array('carpet-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont3" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/mattress.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Mattress<br>Cleaning</p>
                         <a href="{{ URL::to('mattress-cleaning-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('mattress-cleaning-dubai') || in_array('mattress-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont3" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/Sofa.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Sofa Cleaning<br>Cleaning</p>
                         <a href="{{ URL::to('sofa-cleaning-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('sofa-cleaning-dubai') || in_array('sofa-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont3" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/floor.jpg') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Floor<br>Scrubbing</p>
                         <a href="{{ URL::to('floor-scrubbing-dubai') }}"> <input value="BOOK NOW" class="{{ request()->is('floor-scrubbing-dubai') || in_array('floor-scrubbing-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}"  data-id="cont3" type="button"></a>
                    </div>
               </div>
          </div>
     </div>
     <div class="item">
          <div>
               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="{{ asset('images/Carpet.png') }}" alt=""></div>
                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                         <p>Nanny<br>services</p>
                         <a href="{{ URL::to('carpet-cleaning-dubai') }}"><input value="BOOK NOW" class="{{ request()->is('carpet-cleaning-dubai') || in_array('carpet-cleaning-dubai', $houseArray) ? 'text-field-button active' : 'text-field-button' }}" data-id="cont4"  type="button"></a>
                    </div>
               </div>
          </div>
     </div>-->
</div>
<?php } ?>
