@extends('layouts.app')
@section('content')
    <form method="post" id="ccavenue_form"
        action="{{config('payment.ccavenue_req_handler')}}?{{ @$query_params }}"
        style="display: none">
        {!! csrf_field() !!}
        <table width="40%" height="100" border='1' align="center">
            <caption>
                <font size="4" color="blue"><b>Integration Kit</b></font>
            </caption>
        </table>
        <table width="40%" height="100" border='1' align="center">
            <tr>
                <td>Parameter Name:</td>
                <td>Parameter Value:</td>
            </tr>
            <tr>
                <td colspan="2"> Compulsory information</td>
            </tr>
            <tr>
                <td>Merchant Id :</td>
                <td><input type="text" name="merchant_id" value="{{config('payment.ccavenue_merchant_id')}}" /></td>
            </tr>
            <tr>
                <td>Order Id :</td>
                <td><input type="text" name="order_id" value="{{$data['bookings']['reference_id'] ?: $data['bookings']['booking_id']}}" /></td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="text" name="amount" value="{{ number_format(@$data['online']['amount'], 2, '.', '') + number_format(@$data['online']['transaction_charge'], 2, '.', '') }}" /></td>
            </tr>
            <tr>
                <td>Currency :</td>
                <td><input type="text" name="currency" value="AED" /></td>
            </tr>
            <tr>
                <td>Redirect URL :</td>
                <td><input type="text" name="redirect_url"
                        value="{{config('payment.ccavenue_resp_handler')}}?{{ @$query_params }}" />
                </td>
            </tr>
            <tr>
                <td>Cancel URL :</td>
                <td><input type="text" name="cancel_url"
                        value="{{config('payment.ccavenue_resp_handler')}}?{{ @$query_params }}" />
                </td>
            </tr>
            <tr>
                <td>Language :</td>
                <td><input type="text" name="language" value="EN" /></td>
            </tr>
            <tr>
                <td colspan="2">Billing information(optional):</td>
            </tr>
            <tr>
                <td>Billing Name :</td>
                <td><input type="text" name="billing_name" value="{{$data['customer']['customer_name']}}" /></td>
            </tr>
            <tr>
                <td>Billing Address :</td>
                <td><input type="text" name="billing_address" value="{{ @$data['customer_address']['customer_address'] }}" /></td>
            </tr>
            <tr>
                <td>Billing City :</td>
                <td><input type="text" name="billing_city" value="" /></td>
            </tr>
            <tr>
                <td>Billing State :</td>
                <td><input type="text" name="billing_state" value="" /></td>
            </tr>
            <tr>
                <td>Billing Zip :</td>
                <td><input type="text" name="billing_zip" value="" /></td>
            </tr>
            <tr>
                <td>Billing Country :</td>
                <td><input type="text" name="billing_country" value="United Arab Emirates" /></td>
            </tr>
            <tr>
                <td>Billing Tel :</td>
                <td><input type="text" name="billing_tel" value="{{ @$data['customer']['mobile_number_1'] }}" /></td>
            </tr>
            <tr>
                <td>Billing Email :</td>
                <td><input type="text" name="billing_email" value="{{ @$data['customer']['email_address'] }}" /></td>
            </tr>
            <tr>
                <td></td>
                <td><INPUT TYPE="submit" value="CheckOut"></td>
            </tr>
        </table>
    </form>
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="row em-booking-content-main ml-0 mr-0">
                    <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                        <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
                            <div class="row em-booking-content-set-main ml-0 mr-0">

                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pl-0 pr-0 d-sm-block d-lg-none d-none">
                                    &nbsp;
                                </div>

                                <div class="col-lg-5 col-md-12 col-sm-12 success-left payment-fail pl-0">
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">

                                        <div class="col-sm-12 success-message pr-0">
                                            <h3>Your payment failed. <br><span>please try again.</span></h3>
                                        </div>
                                        <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                                            <h6>PERSONAL DETAILS</h6>
                                            <div class="col-sm-12 book-details-main service-type">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Name</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="name_p">{{ @$data['customer']['customer_name'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Email ID</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="mail_p">{{ @$data['customer']['email_address'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Contact Number</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ @$data['customer']['mobile_number_1'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Address</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p><span class="address_p">
                                                                @if ($data['customer_address'] != '')
                                                                    {{ @$data['customer_address']['customer_address'] }}
                                                                @else
                                                                    NA
                                                                @endif
                                                            </span>, <span class="area_p">{{ @$data['areaName'] }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="checkout_secret"
                                            value="{{ Config::get('values.checkout_primary_key') }}">
                                        @if (@$data['serviceName'] != 'Other Payments')
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Service Details</h6>
                                                <div class="col-sm-12 book-details-main date-time-det mb-1">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-sm-12 book-det-left pl-0 pr-0">
                                                            <p>
                                                                You've booked
                                                                @if (@$data['serviceName'] == 'House Cleaning')
                                                                    <strong>
                                                                        <span
                                                                            class="maid_id_p">{{ @$data['bookings']['no_of_maids'] }}</span>
                                                                        <?php
                                                                        if (@$data['bookings']['cleaning_material'] == 'Y') {
                                                                            $val = 'with cleaning materials';
                                                                        } else {
                                                                            $val = 'with out cleaning materials';
                                                                        }
                                                                        ?>
                                                                        cleaners (<span
                                                                            id="mat_success_p">{{ $val }}</span>).
                                                                    </strong>
                                                                @endif
                                                                Your
                                                                <strong>
                                                                    @if (@$data['serviceName'] == 'House Cleaning')
                                                                        <?php
                                                                        if (@$data['bookings']['booking_type'] == 'OD') {
                                                                            $val = 'one-time';
                                                                        } elseif (@$data['bookings']['booking_type'] == 'WE') {
                                                                            $val = 'weekly';
                                                                        } else {
                                                                            $val = 'bi-weekly';
                                                                        }
                                                                        ?>
                                                                        <span
                                                                            class="frequency_p">{{ $val }}</span>
                                                                    @endif
                                                                    {{ @$data['serviceName'] }}
                                                                </strong>
                                                                starting on
                                                                <?php
                                                                $date = @$data['bookings']['service_start_date'];
                                                                
                                                                $old_date_timestamp = strtotime($date);
                                                                
                                                                $dateFormat = date('l jS, F', $old_date_timestamp);
                                                                ?>
                                                                <strong class="date_p">{{ $dateFormat }}</strong>
                                                                from
                                                                <strong>
                                                                    <?php
                                                                    $start = date('g:i a', strtotime(@$data['bookings']['time_from']));
                                                                    $end = date('g:i a', strtotime(@$data['bookings']['time_to']));
                                                                    
                                                                    ?>
                                                                    <span class="time_span1">{{ @$start }}</span> -
                                                                    <span class="time_span2">{{ @$end }} </span>.
                                                                </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if (@$data['serviceName'] != 'Other Payments')
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Price Details</h6>
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Price</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="total_p">{{ number_format(@$data['bookings']['service_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if (
                                                    $data['bookings']['coupon_id'] != '' &&
                                                        $data['bookings']['coupon_id'] != null &&
                                                        $data['bookings']['coupon_id'] != 0)
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Discount Price</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p>AED
                                                                    <span>{{ number_format(@$data['bookings']['discount'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>VAT 5%</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="vat_p">{{ number_format(@$data['bookings']['vat_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($data['bookings']['pay_by'] == 'Card')
                                                    <div class="col-sm-12 book-details-main">
                                        <div class="row ml-0 mr-0">
                                            <div class="col-7 book-det-left pl-0 pr-0"><p>Total Price</p></div>
                                            <div class="col-5 book-det-right pl-0 pr-0"><p>AED <span class="total_p">{{ number_format(@$data['online']['amount'], 2, '.', '') }}</span></p></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 book-details-main">
                                        <div class="row ml-0 mr-0">
                                            <div class="col-7 book-det-left pl-0 pr-0"><p>Transaction Charge</p></div>
                                            <div class="col-5 book-det-right pl-0 pr-0"><p>AED <span class="vat_p">{{ number_format(@$data['online']['transaction_charge'], 2, '.', '') }}</span></p></div>
                                        </div>
                                    </div>
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row total-price ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Net Payable</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p>AED <span
                                                                        class="net_p">{{ number_format(@$data['online']['amount'], 2, '.', '') + number_format(@$data['online']['transaction_charge'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row total-price ml-0 mr-0">
                                                            <div class="col-sm-12 em-field-main">

                                                                <button id="book_payment_button"
                                                                    class="text-field-button show6-step"
                                                                    type="button">Retry Payment</button>
                                                                <!--<br>
                                                 <a href="{{ url('cash-payment-success/' . $data['bookings']['reference_id']) }}">Pay By Cash</a>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row total-price ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Total</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p>AED <span
                                                                        class="net_p">{{ number_format(@$data['bookings']['total_amount'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        @else
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Price Details</h6>
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Price</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="total_p">{{ number_format(@$data['paymentData']['amount'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-12 book-details-main">
                                        <div class="row ml-0 mr-0">
                                            <div class="col-7 book-det-left pl-0 pr-0"><p>Transaction Charge</p></div>
                                            <div class="col-5 book-det-right pl-0 pr-0"><p>AED <span class="vat_p">{{ number_format(@$data['paymentData']['transaction_charge'], 2, '.', '') }}</span></p></div>
                                        </div>
                                    </div> -->
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row total-price ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Total</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="net_p">{{ number_format(@$data['paymentData']['amount'], 2, '.', '') + number_format(@$data['paymentData']['transaction_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row total-price ml-0 mr-0">
                                                        <div class="col-sm-12 em-field-main">

                                                            <button id="book_payment_online_button"
                                                                class="text-field-button show6-step" type="button">Retry
                                                                Payment</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- ------------------- show message except OD bookings ------------------- --}}
                                        @if ($data['bookings']['booking_type'] != 'OD')
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-12 book-det-right">
                                                        <p class="text-info"><i class="fa fa-info-circle"
                                                                aria-hidden="true"></i> Charged first service only.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- --------------------------------------------------------------------- --}}


                                    </div>
                                </div>


                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pr-0 d-none d-md-none d-lg-block">
                                    &nbsp;</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
        </section>
    </div>
    <div class="col-sm-12 popup-main" id="payment_popup">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <form id="payment-form" method="POST" action="{{ url('make-pay-checkout') }}">
                    {{ csrf_field() }}
                    <h5 class="">Make Payment <span class="em-forgot-close-btn"><img
                                src="{{ asset('images/el-close-black.png') }}" title="" style=""></span>
                    </h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                        <div class="row m-0">
                            <input type="hidden" id="token_req" name="token_req" value=''>
                            <input type="hidden" name="currency" value="AED">
                            @if (@$data['serviceName'] != 'Other Payments')
                                <?php $amount = $data['online']['amount'];
                                $name_new = preg_replace('/[^a-zA-Z0-9\s]/', '', $data['customer']['customer_name']);
                                ?>
                                <input type="hidden" name="order_id" id="order_id"
                                    value="{{ $data['online']['reference_id'] }}">
                            @else
                                <?php $amount = $data['paymentData']['amount'];
                                $name_new = preg_replace('/[^a-zA-Z0-9\s]/', '', $data['customer']['customer_name']);
                                ?>
                                <input type="hidden" name="order_id" id="order_id"
                                    value="{{ $data['paymentData']['payment_id'] }}">
                            @endif
                            <?php if (isset($_GET['mobile'])) {
                                $mobileParam = '?mobile=active';
                            }
                            ?>
                            <?php if (!isset($_GET['mobile'])) {
                                $mobileParam = '';
                            }
                            ?>
                            <input type="hidden" id="retry_online" name="retry_online">
                            <input type="hidden" name="amount" id="amount"
                                value="{{ number_format(@$amount, 2, '.', '') }}">
                            <input type="hidden" name="merchant_param2" id="merchant_param2"
                                value="{{ $mobileParam }}">
                            <input type="hidden" name="customer_name" id="billing_name" value="{{ $name_new }}">
                            <input type="hidden" name="customer_tel" id="billing_tel"
                                value="{{ $data['customer']['mobile_number_1'] }}">
                            <input type="hidden" name="customer_email" id="billing_email"
                                value="{{ $data['customer']['email_address'] }}">
                            <?php if (isset($_GET['mobile'])) {
                                $mobileParam = '?mobile=active';
                            }
                            ?>
                            <?php if (!isset($_GET['mobile'])) {
                                $mobileParam = '';
                            }
                            ?>
                            <input type="hidden" name="mobile_view" id="mobile_view" value="{{ $mobileParam }}" />
                            <div class="one-liner">
                                <div class="card-frame">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                        <button id="pay-button" disabled>
                            PAY AED {{ number_format(@$amount, 2, '.', '') }} </button>
                    </div>
                </form>
            </div>
        </div>
    </div><!--popup-main end-->

@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.login').hide();
            $('.logout').show();
            $('.make_pay_head').show();
            $('.userPersonel').show();
            // $('#preLoader').fadeOut(300);
        });
        // var payButton = document.getElementById("payment_button");
        var payButton = document.getElementById("pay-button");

        var form = document.getElementById("payment-form");
        Frames.init($('#checkout_secret').val());
        Frames.addEventHandler(
            Frames.Events.CARD_VALIDATION_CHANGED,
            function(event) {
                payButton.disabled = !Frames.isCardValid();
            }
        );
        Frames.addEventHandler(
            Frames.Events.CARD_TOKENIZED,
            function(event) {
                $('#token_req').val(event.token);
                $("#payment-form").submit();
            }
        );
        if (form) {
            form.addEventListener("submit", function(event) {
                event.preventDefault();
                Frames.submitCard();
            });
        }
        $('#book_payment_button').click(function() {
            //$('#payment_popup').show(500);
            $('#retry_online').val('not_online');
            $(".preloader").show();
            //$("#ccavenue_form").submit();
        });
        $('#book_payment_online_button').click(function() {
            $('#payment_popup').show(500);
            $('#retry_online').val('online');
        });
        $('.em-forgot-close-btn').click(function() {
            $('#payment_popup').hide(500);
        });
    </script>
@endpush
