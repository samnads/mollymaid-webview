<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">

    <div class="col-md-12 col-sm-12 em-step-head em-head3 no-left-right-padding">

        <h2>Login / Register</h2>

        <ul>

            <li class="active"><span></span></li>

            <li class="active"><span></span></li>

            <li class="active"><span></span></li>

            <li><span></span></li>

            <li><span></span></li>

        </ul>

    </div>

</div>



<input type="hidden" name="customer_id" id="customer_id">



<div class="col-12 em-booking-content-set pl-0 pr-0">

    <div class="row em-booking-content-set-main ml-0 mr-0">

        <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">

            <form id="step3form" method="post">

                {{ csrf_field() }}

                <input type="hidden" name="typeLogin" id="typeLogin" value="register">

                <div class="col-12 step3 pl-0 pr-0">

                    <div class="col-12 em-booking-det-cont">



                        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box login-section">

                            <div class="row ml-0 mr-0">

                                <div class="col-md-6 login-left-set pl-0 pr-0">



                                    <h5>Sign In <span class="show-register-btn res-log-btn d-sm-block d-md-none"><i
                                                class="fa fa-pencil-square-o"></i> &nbsp; I am not a member yet</span>
                                    </h5>



                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>Username / Email ID</p>

                                        <div class="col-12 em-text-field-main pl-0 pr-0">

                                            <input name="emailUser" class="text-field" type="text">

                                        </div>

                                    </div>







                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>Password</p>

                                        <div class="col-12 em-text-field-main password-set pl-0 pr-0">

                                            <input name="passwordUser" class="text-field" type="password">

                                            <div class="password-set-btn fa fa-fw fa-eye-slash"></div>

                                        </div>

                                    </div>







                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p><a class="cursor" onclick="viewForget();" href="#">Did you forget your
                                                password?</a></p>

                                    </div>





                                    <div class="col-sm-12 em-field-main  log-remb pl-0 pr-0">

                                        <input value="" id="option-01" class="" type="checkbox">

                                        <label for="option-01"> <span></span> &nbsp; Remember Me</label>

                                    </div>





                                </div>





                                <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">

                                    <div class="col-sm-12 login-right-image pr-0"><img
                                            src="{{ asset('images/register.png') }}" alt=""></div>

                                    <div class="col-sm-12 login-right-btn show-register-btn pr-0">

                                        <span><i class="fa fa-pencil-square-o"></i> &nbsp; I am not a member yet</span>

                                    </div>

                                </div>

                            </div>

                        </div><!--login-section end-->





                        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box register-section">

                            <div class="row ml-0 mr-0">

                                <div class="col-md-6 login-left-set pl-0 pr-0">



                                    <h5>Sign Up <span class="show-login-btn res-log-btn d-sm-block d-md-none"><i
                                                class="fa fa-user"></i> &nbsp; Already Signed Up</span></h5>



                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>First Name</p>

                                        <div class="col-12 em-text-field-main pl-0 pr-0">

                                            <input name="firstName" class="text-field" type="text" value="">

                                        </div>

                                    </div>




                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>Last Name</p>

                                        <div class="col-12 em-text-field-main pl-0 pr-0">

                                            <input name="lastName" class="text-field" type="text" value="">

                                        </div>

                                    </div>








                                    <!--<div class="col-sm-12 em-field-main pl-0 pr-0">
                                                  <p>Phone Number TEST</p>
                                                  <div class="col-12 pl-0 pr-0">
                                                       <input styl="padding-left: 50px" name="PHONE_NUMBER" id="PHONE_NUMBER" class="text-field" type="tel">
                                                  </div>
                                             </div>-->


                                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                                        <p>Phone Number</p>
                                        <div class="col-12 em-text-field-main phone-number-set pl-0 pr-0">
                                             <input style="padding-left: 50px" name="phoneNumber" id="phoneNumber" class="text-field" type="tel">
                                        </div>
                                    </div>









                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>Email ID</p>

                                        <div class="col-12 em-text-field-main pl-0 pr-0">

                                            <input name="email" class="text-field" type="text" value="">

                                        </div>

                                    </div>







                                    <div class="col-sm-12 em-field-main pl-0 pr-0">

                                        <p>Password</p>

                                        <div class="col-12 em-text-field-main password-set pl-0 pr-0">

                                            <input name="password" class="text-field" type="password"
                                                value="">

                                            <div class="password-set-btn fa fa-fw fa-eye-slash"></div>

                                        </div>

                                    </div>



















                                </div>

                                <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">

                                    <div class="col-sm-12 login-right-image pr-0"><img
                                            src="{{ asset('images/login.png') }}" alt=""></div>

                                    <div class="col-sm-12 login-right-btn show-login-btn pr-0">

                                        <span><i class="fa fa-user"></i> &nbsp; Already Signed Up</span>

                                    </div>

                                </div>

                            </div>

                        </div><!--register-section end-->

                        <div class="col-sm-12">
                            <p><span id="error_message" class="text-danger"></span></p>
                        </div>

                    </div>



                    <div class="col-12 em-booking-det-cont em-next-btn">

                        <div class="row em-next-btn-set ml-0 mr-0">

                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">

                                <a id="back_houseCleaningStep2"><span class="em-back-arrow back-to2-btn"
                                        title="Previous Step"></span></a>



                                <div class="col-12 sp-total-price-set pl-0 pr-0">

                                    Total<br>

                                    <span>AED <b class="mobile_net_p"></b></span>

                                </div>





                            </div>

                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">

                                <input value="Register" class="text-field-button show4-step" id="button_submit_login"
                                    type="submit">

                            </div>

                        </div>

                    </div>

                </div><!--step3 end-->

            </form>

        </div>

        @include('includes.steps.summary')

    </div>

</div>







<!-- forgot password-->

<div class="col-sm-12 popup-main forgot-password-popup" id="forgot_password">

    <div class="row min-vh-100 d-flex flex-column justify-content-center">

        <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">

            <form id="forgotForm" method="post">

                {{ csrf_field() }}

                <h5 class="">Forgot Password <span class="em-forgot-close-btn"><img
                            src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>

                <p id="success_message">Please enter a valid email address with which you created your account.</p>



                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">

                    <div class="row m-0">

                        <div class="col-12 em-text-field-main pl-0 pr-0">

                            <input name="emailId" plaecholder="Email id" class="text-field" type="text">

                        </div>

                    </div>

                </div>



                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">

                    <input value="Send" id="submit_email" class="text-field-button show6-step" type="submit">

                </div>

            </form>

        </div>

    </div>

</div><!--popup-main end-->



<!-- forgot password success-->

<div class="col-sm-12 popup-main forgot-password-popup" id="success_password">

    <div class="row min-vh-100 d-flex flex-column justify-content-center">

        <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">

            <h5 class="">Forgot Password <span class="em-forgot-close-btn"><img
                        src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>

            <span class="message" id="message">Please enter a valid email address with which you created your
                account.</span>

        </div>

    </div>

</div><!--popup-main end-->

<!-- forgot password success-->





<!--popup-otp-->

<div class="col-sm-12 popup-main otp-popup" id="verify_mobile" style="display: none">

    <div class="row min-vh-100 d-flex flex-column justify-content-center">

        <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">

            <h5 class="">OTP Verification <span class="em-otp-close-btn"><img
                        src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>

            <p>Enter the OTP you received to<br>

                <span id="mobile_span">+971 2345 678</span>
            </p>

            <p>OTP is being sent as email &amp; SMS</p>
            <p id="resend_message"></p>

            <form id="verify_otpForm" method="post">

                <meta name="csrf-token" content="{{ csrf_token() }}">



                <!-- {{ csrf_field() }} -->

                <!-- <input type="hidden" value="{{ @$bookingId }}" id="booking_id" name="booking_id">  -->

                <input type="hidden" value="" id="customerId" name="customerId">



                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">

                    <div class="row m-0">

                        <div class="col-3 em-text-field-main pl-0 pr-0">

                            <input autocomplete="off" name="first" id="codeBox1" type="tel"
                                class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEvent(1, event)"
                                onfocus="onFocusEvent(1)">

                        </div>

                        <div class="col-3 em-text-field-main pl-0 pr-0">

                            <input autocomplete="off" name="second" id="codeBox2" type="tel"
                                class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEvent(2, event)"
                                onfocus="onFocusEvent(2)">

                        </div>

                        <div class="col-3 em-text-field-main pl-0 pr-0">

                            <input autocomplete="off" name="third" id="codeBox3" type="tel"
                                class="text-field border-right-0" maxlength="1" onkeyup="onKeyUpEvent(3, event)"
                                onfocus="onFocusEvent(3)">

                        </div>

                        <div class="col-3 em-text-field-main pl-0 pr-0">

                            <input autocomplete="off" name="fourth" id="codeBox4" type="tel"
                                class="text-field " maxlength="1" onkeyup="onKeyUpEvent(4, event)"
                                onfocus="onFocusEvent(4)">

                        </div>

                    </div>

                </div>

                <span id="otp_error" style="color:red;font-size:13px;"></span>



                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">

                    <button type="button" onclick="resendOTP();" style="display:none" id="resend_button"
                        class="text-field-button show6-step">Resend OTP</button>



                    <input value="Continue" id="otp_submit" class="text-field-button show6-step" type="submit">

                </div>

            </form>

        </div>

    </div>

</div><!--popup-main end-->
@push('scripts')
    <script>
        $(document).ready(function() {
            let defaultOptions = $(".country-codes").html(); // Store the default options HTML
            $(".country-codes").change(function() {
                let countryCode = $(this).val();
                $(this).find(":selected").text('+' + countryCode);
            }).mousedown(function() {
                // Restore the default options in HTML
                $(this).html(defaultOptions);
            });
            $(".country-codes").focusout(function() {
                $(".country-codes").find("option:selected").trigger("change");
            });
            // first load trigger
            $(".country-codes").trigger("change");
        });
    </script>
@endpush
