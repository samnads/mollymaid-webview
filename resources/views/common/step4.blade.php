<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head4 no-left-right-padding">
        <h2>Your Location</h2>
        <ul>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li class="active"><span></span></li>
            <li><span></span></li>
        </ul>
    </div>
</div>

<div class="col-12 em-booking-content-set pl-0 pr-0">
    <div class="row em-booking-content-set-main ml-0 mr-0">
        <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
            <input type="hidden" id="noOfHours" name="noOfHours" value="{{@$responseData['no_of_hrs']}}">
            <!-- <input name="noOfMaids" value="{{@$responseData['no_of_maids']}}" id="noOfMaids" type="hidden"> -->
            <input type="hidden" id="cleaningMaterialStatus" name="cleaningMaterialStatus" value="{{@$responseData['cleaning_material']}}" >
            <input type="hidden" id="extraServices" name="extraServices" value="0" @if(@$responseData['interior_window_clean'] == 1 )  value="4" @elseif(@$responseData['ironing_services'] == 1 ) value="2"
            @elseif(@$responseData['oven_cleaning'] == 1 ) value="3" @elseif(@$responseData['fridge_cleaning'] == 1 ) value="1" @endif>
            <input type="hidden" id="cleaning_date" value="{{@$responseData['service_start_date']}}">
            <input type="hidden" id="monthDurationId" value="{{@$responseData['month_durations']}}">
            <input type="hidden" id="visitType" value="{{@$responseData['booking_type']}}">

            <form method="post" id="addressForm" name="addressForm">
                {{ csrf_field() }}
                <input type="hidden" id="hour_rate" value="" name="hour_rate">                       
                <input type="hidden" id="discount" value="" name="discount">
                <input type="hidden" id="service_charge" value="" name="service_charge">
                <input type="hidden" id="vat_charge" value="" name="vat_charge">
                <input type="hidden" id="total_amount" value="" name="total_amount">
                <input type="hidden" id="net_cleaning_fee" value="" name="net_cleaning_fee">
                <input type="hidden" id="net_service_charge" value="" name="net_service_charge">
                <input type="hidden" id="net_discount" value="" name="net_discount">
                <input type="hidden" id="net_vat_charge" value="" name="net_vat_charge">
                <input type="hidden" id="total_net_amount" value="" name="total_net_amount">
                <input type="hidden" id="cleaning_material_fee" value="" name="cleaning_material_fee">
                <div class="col-12 step4 pl-0 pr-0">
                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                            
                            <h5>Your Address Details</h5>
                            
                            <div class="col-sm-12 em-field-main-set"> 
                                <div class="row m-0">
                                    <div class="col-sm-6 em-field-main">
                                        <p>Area</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <select name="area" type="text" placeholder="" class="el-text-field">
                                                <option value="">-- Select Area --</option>
                                                @foreach($area as $ar)
                                                    <option value="{{$ar['area_id']}}">{{$ar['area_name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-6 em-field-main">
                                            <p>Address Details</p>
                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                <input name="address" class="text-field" type="text" value="{{@$responseData2['customer_address']}}">
                                        </div>
                                    </div>
                                    
                                </div> 
                            </div>
                                
                            <!--<div class="col-sm-12 em-field-main-set"> 
                                <div class="row m-0">
                                    
                                    <div class="col-sm-6 em-field-main">
                                        <p>Address</p>
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                                <input name="" class="text-field" type="text">
                                        </div>
                                    </div>
                                </div> 
                            </div>-->
                                
                            <div class="col-sm-12 em-field-main-set map-view">
                                <input type="hidden"  class="us3-lat" name="latitude">
                                <input type="hidden"  class="us3-lon" name="longitude">
                                <input type="hidden" class="us3-radius" />

                                <div class="map-search"><input name="searched_address" placeholder="Address" class="text-field us3-address" type="text"></div>
                                <div class="us3" style="width: 550px; height: 400px;"></div>
                                <!-- <iframe id="us3" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" height="200px" frameborder="0"></iframe> -->
                            </div>
                                
                            <div class="col-sm-12 em-field-main pt-0">
                                <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often address-option ">
                                    <p>Address Option</p>
                                    <ul>
                                        <li class="home">
                                            <input id="address1" @if(@$responseData2['address_category'] == 'HO') checked @endif value="HO" name="address_opt" class="" type="radio">
                                            <label for="address1"> <span></span> &nbsp; Home</label>
                                        </li>
                                        <li class="office">
                                            <input id="address2" value="OF" name="address_opt" class="" @if(@$responseData2['address_category'] == 'OF') checked @endif  type="radio">
                                            <label for="address2"> <span></span> &nbsp; Office</label>
                                        </li>
                                        <li class="other">
                                            <input id="address3" value="OT" name="address_opt" class="" @if(@$responseData2['address_category'] == 'OT') checked @endif type="radio">
                                            <label for="address3"> <span></span> &nbsp; Other</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                    
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <a id="back_houseCleaningStep3" ><span class="em-back-arrow back-to3-btn" title="Previous Step"></span></a>
                                
                                <div class="col-12 sp-total-price-set pl-0 pr-0">
                                    Total<br>
                                    <span>AED <b class="mobile_net_p"></b></span> 
                                </div>
                                
                        
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Next" class="text-field-button show5-step" id="button_submit" type="submit">
                            </div>
                        </div>
                    </div>
                    
                </div><!--step4 end-->
            </form>   
        </div>
        <div id="result"></div>
        @include('includes.steps.summary')
    </div>
</div>
@push('scripts')
<script>
$(document).ready(function() {
    $('select[name="area"]').select2();
    $(document.body).on("change",'select[name="area"]',function(){
        $(this).valid();
    });
});
</script>
@endpush
