@extends('layouts.app')
@section('title') {{'Booking Success'}} @endsection
@section('content')
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="row em-booking-content-main ml-0 mr-0">
                    <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                        <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
                            <div class="row em-booking-content-set-main ml-0 mr-0">

                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pl-0 pr-0 d-sm-block d-lg-none">&nbsp;
                                </div>

                                <div class="col-lg-5 col-md-12 col-sm-12 success-left pl-0">
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">

                                        <div class="col-sm-12 success-message pr-0">
                                            <h3>We’ve received your service request<br><span>Please check your
                                                    email</span><br>
                                                <span>Our operation team will reach out soon, to confirm your
                                                    booking.</span>
                                            </h3>
                                        </div>
                                        <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                                            <h6>Client Details</h6>
                                            <div class="col-sm-12 book-details-main service-type">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Name</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="name_p">{{ @$data['customer']['customer_name'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Email ID</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="mail_p">{{ @$data['customer']['email_address'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Contact Number</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ @$data['customer']['mobile_number_1'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Address</p>
                                                    </div>
                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p><span class="address_p">
                                                                @if ($data['customer_address'] != '')
                                                                    {{ @$data['customer_address']['customer_address'] }}
                                                                @else
                                                                    NA
                                                                @endif
                                                            </span>, <span class="area_p">{{ @$data['areaName'] }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if (@$data['serviceName'] != 'Other Payments')
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Service Details</h6>
                                                <div class="col-sm-12 book-details-main date-time-det mb-1">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-sm-12 book-det-left pl-0 pr-0">
                                                            <p>
                                                                <!--You've booked
                                                        @if (@$data['serviceName'] == 'House Cleaning')
    <strong>
                                                            <span class="maid_id_p">{{ @$data['bookings']['no_of_maids'] }}</span>
                                                            <?php
                                                            if (@$data['bookings']['cleaning_material'] == 'Y') {
                                                                $val = 'with cleaning materials';
                                                            } else {
                                                                $val = 'with out cleaning materials';
                                                            }
                                                            ?>
                                                            cleaners (<span id="mat_success_p">{{ $val }}</span>).
                                                        </strong>
    @endif
                                                        Your -->
                                                                <strong>
                                                                    @if (@$data['serviceName'] == 'House Cleaning')
                                                                        <?php
                                                                        if (@$data['bookings']['booking_type'] == 'OD') {
                                                                            $val = 'one-time';
                                                                        } elseif (@$data['bookings']['booking_type'] == 'WE') {
                                                                            $val = 'weekly';
                                                                        } else {
                                                                            $val = 'bi-weekly';
                                                                        }
                                                                        ?>
                                                                        <span class="frequency_p">{{ $val }}</span>
                                                                    @endif
                                                                    {{ @$data['serviceName'] }}
                                                                </strong>
                                                                on
                                                                <?php
                                                                $date = @$data['bookings']['service_start_date'];
                                                                
                                                                $old_date_timestamp = strtotime($date);
                                                                
                                                                $dateFormat = date('l jS, F Y', $old_date_timestamp);
                                                                ?>
                                                                <strong class="date_p">{{ $dateFormat }}</strong>
                                                                starting from
                                                                <strong>
                                                                    <?php
                                                                    $start = date('g:i a', strtotime(@$data['bookings']['time_from']));
                                                                    $end = date('g:i a', strtotime(@$data['bookings']['time_to']));
                                                                    ?>
                                                                    <span class="time_span1">{{ @$start }}</span>.
                                                                    <!--<span class="time_span2">{{ @$end }} </span>.-->
                                                                </strong>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if (@$data['serviceName'] != 'Other Payments')
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Price Details</h6>
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Service Fee</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p><span
                                                                    class="total_p">{{ number_format(@$data['bookings']['service_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if (
                                                    $data['bookings']['coupon_id'] != '' &&
                                                        $data['bookings']['coupon_id'] != null &&
                                                        $data['bookings']['coupon_id'] != 0)
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Discount Price</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p><span>{{ number_format(@$data['bookings']['discount'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>VAT 5%</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p><span
                                                                    class="vat_p">{{ number_format(@$data['bookings']['vat_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!----------------------------------------------------------------->
                                                @if (@$data['bookings']['pay_by'] == 'Cash' && @$data['bookings']['pay_by_cash_charge'] > 0)
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Convenience Fee</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p><span
                                                                        class="vat_p">{{ number_format(10, 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif(@$data['bookings']['pay_by'] == 'Card')
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Convenience Fee</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p><span
                                                                        class="">{{ number_format(@$data['online']['transaction_charge'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <!----------------------------------------------------------------->
                                                @if (@$data['bookings']['pay_by'] == 'Cash')
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row total-price ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Total</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p>AED <span
                                                                        class="net_p">{{ number_format(@$data['bookings']['total_amount'] + @$data['bookings']['pay_by_cash_charge'] ?: 0, 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif(@$data['bookings']['pay_by'] == 'Card')
                                                    <div class="col-sm-12 book-details-main">
                                                        <div class="row total-price ml-0 mr-0">
                                                            <div class="col-7 book-det-left pl-0 pr-0">
                                                                <p>Total</p>
                                                            </div>
                                                            <div class="col-5 book-det-right pl-0 pr-0">
                                                                <p>AED <span
                                                                        class="net_p">{{ number_format(@$data['online']['amount'], 2, '.', '') + number_format(@$data['online']['transaction_charge'], 2, '.', '') }}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <!----------------------------------------------------------------->
                                            </div>
                                        @else
                                            <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                                <h6>Price Details</h6>
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Service Fee</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="total_p">{{ number_format(@$data['paymentData']['amount'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-12 book-details-main">
                                            <div class="row ml-0 mr-0">
                                                <div class="col-7 book-det-left pl-0 pr-0"><p>Transaction Charge</p></div>
                                                <div class="col-5 book-det-right pl-0 pr-0"><p>AED <span class="vat_p">{{ number_format(@$data['paymentData']['transaction_charge'], 2, '.', '') }}</span></p></div>
                                            </div>
                                        </div> -->
                                                <div class="col-sm-12 book-details-main">
                                                    <div class="row total-price ml-0 mr-0">
                                                        <div class="col-7 book-det-left pl-0 pr-0">
                                                            <p>Total</p>
                                                        </div>
                                                        <div class="col-5 book-det-right pl-0 pr-0">
                                                            <p>AED <span
                                                                    class="net_p">{{ number_format(@$data['paymentData']['amount'], 2, '.', '') + number_format(@$data['paymentData']['transaction_charge'], 2, '.', '') }}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- ------------------- show message except OD bookings ------------------- --}}
                                        @if ($data['bookings']['booking_type'] != 'OD')
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-12 book-det-right">
                                                        <p class="text-info"><i class="fa fa-info-circle"
                                                                aria-hidden="true"></i> Charged first service only.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        {{-- --------------------------------------------------------------------- --}}
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pr-0 d-none d-md-none d-lg-block">
                                    &nbsp;</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.login').hide();
            $('.logout').show();
            $('.make_pay_head').show();
            $('.userPersonel').show();
        });
    </script>
@endpush
