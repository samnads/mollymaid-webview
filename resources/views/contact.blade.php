@extends('layouts.app')
@section('content')
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="col-md-12 col-lg-12">
                    <form id="support_form">
                        <div class="col-sm-12 em-field-main p-0 pt-1">
                            <p>Name</p>
                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                <input name="name" id="name" type="text" class="text-field">
                            </div>
                        </div>
                        <div class="col-sm-12 em-field-main p-0 pt-1">
                            <p>Email</p>
                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                <input name="email" id="email" type="email" class="text-field">
                            </div>
                        </div>
                        <div class="col-sm-12 em-field-main p-0 pt-1">
                            <p>Phone</p>
                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                <input name="phone" id="phone" type="text" class="text-field">
                            </div>
                        </div>
                        <div class="col-sm-12 em-field-main p-0 pt-1">
                            <p>Message</p>
                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                <textarea name="message" class="text-field-big" style="min-height: 100px;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-center text-danger" id="error"></p>
                        </div>
                        <div class="col-12 em-booking-det-cont m-0 pb-0">
                            <div class="row em-next-btn-set m-0">
                                <div class="col-12 em-next-btn-right pl-0 pr-0">
                                    <button type="submit" class="text-field-button" id="submit_rate_button" style="width: 100%;">Send</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
<script>
_base_url = "{{url('')}}/";
$().ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    support_form_validator = $('#support_form').validate({
        ignore: [],
        rules: {
            name: {
                required: true,
            },
            email: {
                required: '#phone:blank',
            },
            phone: {
                required: '#email:blank',
            },
            message: {
                required: true,
            }
        },
        messages: {
            name: "Please enter your name.",
            email: "Enter your email address.",
            phone: "Enter your phone number.",
            message: "Enter your message.",
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            $(".preloader").show();
            $('#support_form button[type="submit"]').html('Plesae wait...');
            /****************************** */
            // format before send
            var values = $("#support_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "contact-us",
                data: values,
                success: function (data) {
                    if (data.status == true) {
                        $('#support_form button[type="submit"]').prop('disabled', true).html('<i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;'+data.message);
                        $('#support_form :input').prop('disabled', true);
                        $(".preloader").hide();
                        $('#error').empty("");
                    }
                    else {
                        $(".preloader").hide();
                        $('#error').text(data.message);
                        $('#support_form button[type="submit"]').html('Send');
                    }
                },
                error: function (data) {
                    $(".preloader").hide();
                    $('#error').empty("An error occured!");
                    $('#support_form button[type="submit"]').html('Send');
                },
            });
        }
    });
});
</script>
@endpush
