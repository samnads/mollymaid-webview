@if (!isWebView())
    <header>
        <div class="container">
            <div class="row m-0">
                <div class="col-lg-2 col-md-12 col-sm-12 pl-0 pr-0">

                    <!-- <div class="em-back-arrow" title="Previous Step"></div> -->

                    <!--<div class="logo"><a href="{{ url('') }}"><img src="{{ asset('images/main-logo.png') }}"
                                alt=""></a></div>-->
						
						<div class="logo"><a href="{{ url('') }}"><img src="{{ asset('images/header-logo.png') }}"
                                alt=""></a></div>		
								
								
                    <div class="em-mob-icons">
                        <ul>
                            <li class="mob-home logout" style="display:none;"><i class="fa fa-user"></i>
                                <ul>
                                    <!-- <li class="make_pay_head" style="display:none;"><a id="make_pay_id" href="{{ url('payment/') }}" >Make Payment</a></li> -->
                                    <li class="userPersonel" style="display:none;"><a
                                            href="{{ url('view-bookings') }}">My Bookings</a></li>
                                    <li class="userPersonel" style="display:none;"><a href="{{ url('my-accounts') }}">My
                                            Accounts</a></li>
                                    <li><a href="#" onclick="logout();" class="logout">Logout</a></li>
                                </ul>
                            </li>
                            <li class="mob-home login"><a onclick="viewLogin();"><i class="fa fa-user"></i></a></li>
                            <li class="mob-home"><a href="{{ URL::to('house-cleaning-dubai') }}"><i
                                        class="fa fa-home"></i></a></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="col-lg-10 col-sm-12  menu pl-0 pr-0">
                    <nav id="primary_nav_wrap">
                        <ul>
                            <li class="home"><a href="{{ url('') }}">Home</a>|</li>
                            <!--<li><a href="{{ url('packages') }}">Packages</a>|</li>-->
                            <!-- <li class="make_pay_head" style="display:none;"><a href="{{ url('payment/') }}">Make Payment</a>|</li> -->
                            <li class="userPersonel" style="display:none;"><a href="{{ url('view-bookings') }}">My
                                    Bookings</a>|</li>
                            <li class="userPersonel" style="display:none;"><a href="{{ url('my-accounts') }}">My
                                    Accounts</a>|</li>
                            <li class="log-reg login"><a onclick="viewLogin();" class="pr-0 cursor">Login</a></li>
                            <li style="display:none;" class="log-reg cursor logout"><a onclick="logout();"
                                    class="pr-0">Logout</a></li>
                            <div class="clear"></div>
                        </ul>
                        <div class="clear"></div>
                    </nav>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </header>
@endif
<!--Header Section-->
<style>
    header .logo {
        padding-top: 6px;
    }

    @media screen and (max-width: 991px) {
        span.em-back-arrow {
            display: block !important;
            position: fixed;
            top: 0;
            z-index: 9;
            height: 46px !important;
            width: 36px !important;
            left: calc(50% - 345px);
        }

        header .logo {
            transition: margin 0.5s;
            padding-top: 13px;
        }

        header .logo.logo_left {
            margin-left: 32px
        }

    }

    @media screen and (max-width: 767px) {
        span.em-back-arrow {
            left: calc(50% - 260px);
        }
    }

    @media screen and (max-width: 575px) {
        span.em-back-arrow {
            left: 15px;
        }
    }
</style>
