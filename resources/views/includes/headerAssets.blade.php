<title>@if(View::hasSection('title'))        @yield('title')    @else   Mollymaid    @endif</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="icon" type="image/png" href="{{asset('images/favicon.ic')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/owl.carousel.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/pignose.calendar.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/intlTelInput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/gijgo.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css?v=').Config::get('version.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/css_change.css?v=').Config::get('version.css')}}"/>