<div class="col-lg-4 col-md-12 col-sm-12 em-booking-content-right pl-0 pr-0">

    <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">

        <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0" id="summary_section">

            <h5>BOOKING SUMMARY NEW<span class="sp-sum-rem-btn"><img src="{{asset('images/el-close-black.png')}}" title=""></span></h5>

            <span class="summary_message" style="display:none"></span>

            <div class="col-sm-12 book-details-main service-type">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Service Type</p></div>
                    <div class="col-5 book-det-right pl-0 pr-0"><p>{{$data['service']['service_type_name']}}</p></div>

                </div>

            </div>  

            

            <div class="col-sm-12 book-details-main" id="summary_material">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Material</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="cleaning_material_p"></p></div>

                </div>

            </div>
			
			<div class="col-sm-12 book-details-main" id="summary_tools">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Tools</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="cleaning_tools_p">No</p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Duration</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="hrs_p"></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Frequency</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="frequency_p"></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Number of Cleaners</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="maid_id_p"></p></div>

                </div>

            </div>
			
			<div class="col-sm-12 book-details-main mb-3" id="summary_supervisor">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Supervisor</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p class="super_visor_p">No</p></div>

                </div>

            </div>

         

        </div>

        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

            <h6>Date & Time </h6>

            <div class="col-sm-12 book-details-main date-time-det mb-3 date_time" style="display:none;">

                <div class="row ml-0 mr-0">

                <?php

                $date = @$responseData['service_start_date'];

                $old_date_timestamp = strtotime($date);

                $dateFormat = date('l jS, F',$old_date_timestamp); 

                ?>

                    <div class="col-sm-12 book-det-left pl-0 pr-0"><p>Starting on <span class="date_p"></span><br><span class="time_span1"></span><span class="to_p"></span><span class="time_span2"></p></div>

                </div>

            </div>

        </div>

        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

            <h6>Address </h6>

            <div class="col-sm-12 book-details-main date-time-det mb-3 address_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-sm-12 book-det-left pl-0 pr-0">

                    <p><span class="address_p">{{@$addressName}}</span>, <span class="area_p">{{@$areaName}}</span></p></div>

                </div>

            </div>

        </div>

        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

            <h6>Price Details</h6>

            <div class="col-sm-12 book-details-main">

                <div class="col-lg-12 col-md-12 col-sm-12 book-promo-set pl-0 pr-0">

                    <p class="promo-text">Please enter the promo code that you've received</p>

                    <div class="promo-main">

                            <input name="couponCode[]" class="em-apply-field couponClass" value="{{@$couponCode}}" type="text">

                            <input name="coupon_but" class="em-apply-but" value="Check Code" onclick="applyCoupon(true);" type="button">

                    </div>

                                           <!--<span class="coupon_message" style="display:none;font-size: 13px;"></span>-->

                          <div class="coupon_message-main">
                               <span class="coupon_message"><!--This voucher code is only valid for bookings that take place on  Friday.--></span>
                          </div>



                </div>

            </div>

            <div class="col-sm-12 book-details-main">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Price</p></div>

                    <?php

                    $total =  @$summaryArray['service_charge']+@$summaryArray['net_cleaning_fee'];

                    ?>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="total_p">{{$total}}</span></p></div>

                </div>

            </div>
			
			<div class="col-sm-12 book-details-main cleaningtools_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Tools Price</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="cleaningtoolsdiv_p"></span></p></div>

                </div>

            </div>
			
			<div class="col-sm-12 book-details-main cleaningmaterial_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Material Price</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="cleaningmaterialdiv_p"></span></p></div>

                </div>

            </div>
			
			<div class="col-sm-12 book-details-main supervisorcharge_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Supervisor Fee</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="supervisorcharge_div_p"></span></p></div>

                </div>

            </div>
            <div class="col-sm-12 book-details-main discount_value_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Discount</p></div>

                    

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="discount_v">0.00</span></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main discount_div" style="display:none;">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Discount Price</p></div>

                    

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="discount_p">0.00</span></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>VAT 5%</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="vat_p">{{@$summaryArray['net_vat_charge']}}</span></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main transaction_div" style="display:none">

                <div class="row ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Convenience Fee</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="transaction_p"></span></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main total_summary_div">

                <div class="row total-price ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Total</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="net_p">{{@$summaryArray['net_service_charge']}}</span></p></div>

                </div>

            </div>

            <div class="col-sm-12 book-details-main transaction_total_div" style="display:none">

                <div class="row total-price ml-0 mr-0">

                    <div class="col-7 book-det-left pl-0 pr-0"><p>Total Payable</p></div>

                    <div class="col-5 book-det-right pl-0 pr-0"><p id="">AED <span class="trans_net_p"></span></p></div>

                </div>

            </div> 
            <div class="col-sm-12 book-details-main info-price-we-bw" style="display: none;">
                <div class="row ml-0 mr-0">
                    <div class="col-12 book-det-right pl-0 pr-0"><p class="text-info"><i class="fa fa-info-circle"aria-hidden="true"></i> Charged first service only.</p></div>
                </div>
            </div>
        </div>

    </div>

</div>

@push('scripts')

<script>

$(document).ready(function() {

    $('#maid_id_p').text($('#noOfMaids').val());

    var clean_status = $('#cleaningMaterialStatus').val();

    if(clean_status == 'N') 

    {

          $('#cleaning_material_p').text('NO');

    } else if(clean_status == 'Y') 

    {

          $('#cleaning_material_p').text('YES');

    }

    $('#hrs_p').text($('#noOfHours').val());

});

 </script>



@endpush                        