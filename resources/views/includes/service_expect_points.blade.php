<div class="col-sm-12 em-terms-and-condition-main">
    <h6>What to expect from this service ?</h6>
    <div class="col-sm-12 em-terms-and-condition pr-3">
        <ul>
            @foreach ($data['service_expect_points'] as $key => $point)
                <li>
                    <div class="em-num">{{ $key + 1 }}</div>
                    <div class="em-num-cont">
                        <p>{!! $point['point_html'] !!}</p>
                    </div>
                    <div class="clear"></div>
                </li>
            @endforeach
            <li>
                <div class="em-num bg-info"><i class="fa fa-question" aria-hidden="true"></i></div>
                <div class="em-num-cont">
                    <p>If you require any further information on your booking, kindly Call <a
                            href="tel:+971000000000"><b>+971-000000000</b></a>, WhatsApp Helpline <a target="_blank"
                            href="https://api.whatsapp.com/send?phone=971000000000"><b>+971000000000</b></a> or email us at <a
                            href="mailto:office@example.com"><b>office@example.com</b></a></p>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="em-num bg-warning"><i class="fa fa-info" aria-hidden="true"></i></div>
                <div class="em-num-cont">
                    <p>By initiating a request through any means, encompassing but not confined to the App, in-person interactions, telephone communication, Whatsapp correspondence, or electronic mail, the client voluntarily accepts and adheres to the stipulations detailed within the <strong>Mollymaid™</strong> Terms and Conditions on the website.</p>
                </div>
                <div class="clear"></div>
            </li>
        </ul>
    </div>
</div>
