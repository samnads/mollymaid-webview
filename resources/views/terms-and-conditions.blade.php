@extends('layouts.app')
@section('content')
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="col-md-12 col-lg-12">
                    <div>
                        <div class="container mb-5">
		<p>Please read these terms &amp; conditions carefully as they may contain important information regarding your legal rights, remedies, and obligations. These include various limitations and exclusions. By initiating a request through any means, encompassing but not confined to the App, in-person interactions, telephone communication, Whatsapp correspondence, or electronic mail, the client :</p>

<h3>1. Booking &amp; Cancellation Policy</h3>

<p>1.1 Starting 01st January 2022, our new work days are from Monday to Saturday, between 8:00 AM to 6:00 PM.<br />
1.2 Channels for communication:<br />
1.2.1 Office Phone number: +971 4 263 1976 Email: office@example.com<br />
1.2.2 Integrated Whatsapp Helpline: +971 54 350 2510 &ndash; All instructions must be communicated on this helpline for efficient execution, follow up &amp; dispute resolution.<br />
1.2.3 Customer Care contact no: +971 50 1967178 / +971 56 188 5620 / +971 50 412 5620 / +971 56 370 5077. (Whatsapp messages on these numbers will not be attended to.)<br />
1.2.4 Social media accounts of Mollymaid&trade;<br />
1.3 All government holidays as per law will be adhered to and are applicable as such.<br />
1.4 Service other than Working hours/days and on Public Holidays will be charged extra, if service is accepted. Depending on location and time, additional transport charges may apply.<br />
1.5 Our service is booked &amp; paid by hours and not task/s or their completion. DEWA connection must already be activated so Mollymaid crew can access Water &amp; electricity and property has air conditioning functional.<br />
1.6 We reserve the right to amend the initial quotation, should the client&rsquo;s original requirements change.<br />
1.7 The customer service advisors can only give a rough estimate of the duration of the cleaning service, which is based on a basic description of the client&rsquo;s house / office. Please note that the crew may discuss a variation on the planned duration if, in practice, it appears to be required.<br />
1.8 Working outdoors is subject to environmental factors being acceptable to the Mollymaid crew at your property. Client to provide access to inaccessible parts of the property, for crew to perform their job.<br />
1.9 The crew will work at the location booked &amp; shall not be transported or sent to any other location, without prior permission from Mollymaid&trade; office.<br />
1.10 If collection of keys is required from a location outside the local area, a transport fee AED 20 will apply.<br />
1.11 Client agrees to provide a task list and all cleaning chemicals, tools and equipment (vacuum cleaner, mop, bucket and portable ladder) required to carry out the service, unless other arrangements have been made with Mollymaid&trade;. Any cleaning equipment provided by client, should be safe and in full working order.<br />
1.12 Client may cancel or adjust the time of a cleaning visit/s by giving at least 24 hours advanced notice.<br />
1.13 Client agrees to pay the 50% of a cleaning visit if the client cancels or changes the date/time less than 24 hours prior to the scheduled appointment.<br />
1.14 No show case is subject to 100% of the charges as total hours booked.<br />
1.15 Client agrees to pay the full price of the cleaning visit in the event of a lock-out &amp; cleaners being turned away; no one home to let them in; or problem with client&rsquo;s keys.<br />
1.16 Service will be considered started after 30 minutes of crew arrival to your home / office, in the event client is not on site of work or late. Our service is booked by time and must be paid accordingly.<br />
1.17 Mollymaid crew are not allowed to share their contact numbers with clients. For any instructions &amp; need, please contact us on Helpline &amp; we will do the needful.<br />
1.18 Mollymaid crew are mandated to carry their phone on person for safety reasons.<br />
1.19 Client to provide/pay, if applicable, access to the Building / Apartment / Community &amp; Cleaning Supplies, tools, as necessary.<br />
1.20 At any service, only one deal / offer can be availed.<br />
1.21 Same Maid on every service, is available only on&nbsp;<strong>My Maid&nbsp;</strong>Offer, applicable with a minimum once a week or more recurring service.<br />
1.22&nbsp;<strong>My Maid Plan</strong>&nbsp;&ndash; Invoice for the month is always paid in advance for the service scheduled.<br />
1.23 Same maid offer is not applicable with any other promotion or offer. Housekeeper assigned are subject to availability to all services booked except when booked with&nbsp;<strong>My Maid&nbsp;</strong>Offer.<br />
1.24&nbsp;<strong>My Maid Offer</strong>&nbsp;benefits are withdrawn on 2 consecutive or 3 in total occasions No Show or cancellation has occurred. (Specific exclusion as per clause 1.25)<br />
1.25 Client with&nbsp;<strong>My Maid</strong>&nbsp;Plan: Client can terminate the cleaning service by giving 30 days advanced notice in writing or by email and specifying the last cleaning date.<br />
1.26 Client with&nbsp;<strong>My Maid</strong>&nbsp;Plan: Temporary holiday suspension of service requires 2 weeks&rsquo; notice; Cancellation of service requires 1-month notice or payment in lieu of.<br />
1.27 Client to provide us with specific instructions, should you have any objections to our standard cleaning procedures or require specific attention.<br />
1.28 Client to take all reasonable precautions to protect your valuables and properties from theft and damage at all times when our staff is at your premises to carry out their work.<br />
1.29 Our staff works under supervision / instruction of client. It is client&rsquo;s responsibility not to expose them to unreasonable dangers. In case of any eventuality, client is responsible for all related expenses.<br />
1.30 Clothes which require special treatment must be excluded from those given for laundry &amp; Ironing service. Service provider not to be held responsible for damage to such article, in case were not separated as per client preferences.<br />
1.31 You can not hold the crew from leaving your property once agreed service time is up. Please contact us, in the event you have any concerns. We hold the right to initiate action as per governing UAE local laws.<br />
1.32 The client accepts and understands that poor service, breakage/damage or theft must be reported within 24 hours from the service date. Failure to do so will entitle client to no refunds or retouch cleanings.<br />
1.33 Mollymaid&trade; may require the presence of the client or his/hers representative in the beginning and at the end of the cleaning session as an inspection can be carried out and if any corrections, should be made on the same day. Mollymaid&trade; reserves the right not to return a cleaner more than once.</p>

<h3>2. Covid Policy</h3>

<p>2.1 As Covid pandemic continues, the possibility of spread remains a risk on client and service crew.<br />
2.2 We follow DHA &amp; RTA guidelines for monitoring temperature, general health condition of our staff &amp; allowed passenger limit, et al.<br />
2.3 Whilst maintaining all the protocols, if a team member has come in contact with a Covid positive person, client, colleague or otherwise, we adhere to following measures:<br />
a. Necessary support offered b. Medical procedures completed c. Company policy is explained<br />
d. Assess the risk e. Inform At-Risk Clients &amp; employees f. Supported till recovered<br />
2.4 In the event a housekeeper is tested Covid positive, we notify all clients, that were visited in last 3 days so that they can make informed health decisions.<br />
2.3 By the same token &amp; concern, we expect our client to notify us, in case they have come in contact or have any family member tested Covid positive.<br />
2.4 In the event client has Covid case in their premise, we will only resume service once the property is sanitized by&nbsp; Dubai Municipality approved expert.</p>

<h3>3. Non- Solicitation Policy</h3>

<p>3.1 During the term of this Agreement and after termination of this Agreement, client will not, without the prior written consent of Mollymaid&trade; manager, either directly or indirectly, solicit or attempt to solicit, divert or hire away any person employed by Mollymaid&trade;.<br />
3.2 By hiring Mollymaid&trade; services, the client agrees that after the termination of the cleaning service he/she will not hire or use any domestic services provided by a present or past housekeeper of Mollymaid&trade;. If the client wishes to hire or use services provided by such a person then he/she must pay a referral fee of AED 5000/-.</p>

<h3>4. Payment Policy</h3>

<p>4.1 Payment can be made by cash, cheque, and deposit to our bank account or online transfer. In case you choose to pay by card, you may do so by paying a convenience fee.<br />
4.2 On-Call client will pay for the service prior to or at the end of the service.<br />
4.3&nbsp;<strong>My Maid</strong>&nbsp;Plan fees are payable by the client in advance, upon receipt of the invoice as per the agreement.<br />
4.4 We reserve the right to suspend service, without any further notice, if invoice is not settled by 5th of the month.<br />
4.5 Payment to be made towards Grace Quest Cleaning Services, National Bank of Ras Al Khaimah, Account No: 8372 1939 099 01, IBAN Number: AE51 040000 837219 3909901, Swift Code: NRAKAEAK<br />
4.6 VAT or any other Tax to be applied additional to the service, as &amp; when applicable, according to local governing laws.</p>

<h3>5. Refund &amp; Claim Policy</h3>

<p>5.1 Having fulfilled cancellation policy, clients can claim refund or adjustment in subsequent invoice.<br />
5.2 Credit amount can&nbsp;be refunded only if the client does not require cleaning services for more than 4 weeks.<br />
5.3 We are not responsible for damage due to faulty or improper installation of items.<br />
5.4 No refund claims will be considered once the cleaning service has been completed.<br />
5.5 All fragile and highly breakable items must be secured or removed by the client.<br />
5.6 Mollymaid&trade; has public liability insurance cover. In case of an incident, the policy will cover any accidental damages caused by a housekeeper working on behalf of Mollymaid&trade; Company, must be reported within 24 hours of service date. The client is obliged to provide compelling evidence verifying that the damage incurred can be directly attributed to the actions or negligence of the designated housekeeper from Mollymaid company.<br />
5.6.1 Type A claim: Once the claim is established, we will look for possibility to have the item repaired for you. Our liability limits are set at a maximum of 200% of your one visit service fee.&nbsp; Upon approval, this will be added to client credit. Items excluded from liability are: cash, jewelry, one-of-a-kind items or hard to get items; items of sentimental value, art, antiques and electronics.<br />
5.6.2 Type B claim: Claim under public liability insurance, affected item must be of value greater than AED 5000, as per policy terms.&nbsp; To submit a claim for the insurance company review, document required are: Service contract, damaged item invoice to ascertain cost, damaged object or object photo &amp; client ID copies for the claim process. Client must provide any additional requirements raised by the insurance company to complete the assessment. Client will be required to pay a minimum excess for claims on this policy as per insurance policy.<br />
5.7 In the event, client is not able / willing to fulfil the claim documents required, the claim will be considered null &amp; void.<br />
5.8 If &amp; when the insurance company approves the claim, the same will be disbursed according to the legal requirements from Insurance company.<br />
5.9 Mollymaid&trade; will not engage in claim process referred hereto, if the service fee has not been paid by the client. An insurance taken out by the Client may void the insurance policy in this clause in which event and loss will only be recoverable under the policy of the Client.<br />
5.10 In case of a claim regarding missing item / lost item from your property, clients must inform us before the concerned staff leaves your premise or by provide compelling evidence verifying that the damage incurred can be directly attributed to the actions or negligence of the designated housekeeper from Mollymaid company. Mollymaid&trade; will not be responsible for any claims whatsoever and are willing to cooperate as per local law &amp; order to take its befitting course of action.</p>

<h3>6 Liability Policy</h3>

<p>Mollymaid&trade; reserves the right not to be liable for:</p>

<p>6.1 Completing tasks which are not stated on our task list or absence of particular instructions.<br />
6.2 Cleaning jobs not complete due to the lack of suitable cleaning tools and/or equipment in full working order, water or electricity.<br />
6.3 Third party entering or present at the client&rsquo;s premises during the cleaning process.<br />
6.4 Wear or discoloring of floor/tile, glue or paint marks becoming more visible once dirt has been removed.<br />
6.5 Failing to remove old/permanent stains, glue or paint marks that cannot be removed using standard cleaning methods or tools provided by client or Mollymaid&trade;<br />
6.6 Any damages caused by a faulty or not in full working order equipment supplied by the client.<br />
6.7 If the client has items which need special cleaning methods and special cleaning chemical or tools, Mollymaid&trade; reserves the right to refuse the provision of the same.</p>

<h3>7. Privacy Policy</h3>

<p>7.1 Mollymaid&trade; agrees to keep all clients&rsquo; information confidential.<br />
7.2 We will never share, offer, or lease Client&rsquo;s data with anybody without your proper consent or unless requested by a court of law. Information submitted to us is only available to employees managing the operation.</p>

<h3>8. Quality Policy</h3>

<p>8.1 Your satisfaction is our guarantee. At Mollymaid&trade; our professional Maid cleans based on your customized plan. For some reason, if you&rsquo;re not satisfied, our staff will come back and clean whatever area didn&rsquo;t meet your expectations. Simply notify us within 24 hours to avail this benefit.<br />
8.2 We train our staff for their respective roles and are retrained from time to time to upgrade their skills. We strive to provide dependable &amp; efficient service to our clients. We maintain &amp; implement best industry standard as our normal work practice.<br />
8.3 As a consumer you have certain rights, if you are not satisfied with the service rendered.</p>

<ul>
	<li>8.3.1 Right to complain: You have the right to complain, if you are not satisfied with our service, within 24 hrs of the service. We will implement a course of action, in consultation with concerned client.</li>
	<li>8.3.2 Right to Escalate: If you are not happy with the resolution, you can request to escalate the issue to the Manager.</li>
	<li>8.3.3 Right for Refund: If you are not happy with the resolution by the manager, you can request for a refund as per our Refund &amp; Claim policy stated above.</li>
	<li>8.3.4 Under circumstances where client does not report service dissatisfaction to us- above provided course of remedy is not exercised &ndash; &amp; client makes any statement in digital domain which can be deemed as harmful for the company reputation, we reserve the right to legal recourse against such incident &amp; client, as per UAE law.</li>
</ul>

<p>8.4 We reserve the right to service, based on company policies &amp; incident reports.</p>

<p>These terms and conditions shall be governed by the relevant Dubai laws, and by agreeing to be bound by them the client agrees to submit to the exclusive jurisdiction of the relevant courts of the Dubai. Mollymaid&trade; reserves the right to make any changes to any part of these terms and conditions without giving any prior notice. Please check this website for updates.</p>
</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
@endpush
