@extends('layouts.app')
@section('content')
<section class="em-booking-content-section">
    <div class="container em-booking-content-box">
        <div class="row em-booking-content-main ml-0 mr-0">
            <div class="col-12 em-booking-content make_payment_wrapper pl-0 pr-0">
                <div class="col-12 pl-0 pr-0">
                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                        <h5>Confirm Payment</h5>
                            <div class="col-sm-12 em-field-main-set"> 
                                <div class="row m-0">
                                <input type="hidden" id="checkout_secret" value="{{Config::get('values.checkout_primary_key')}}">
                                <?php $address_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['address']);?>
                                    <div class="col-sm-12 em-field-main">
                                        <h4>Hi {{$data['customer_name']}},</h4>
                                    </div>

                                    <div class="col-sm-12 em-field-main">
                                        <div class="col-12 em-text-field-main pl-0 pr-0">
                                            <span>Please confirm your requested payment of <strong>AED {{number_format($data['amount'],2,'.','')}}</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 em-field-main">
                                        <table class="payment_table">
                                            <tbody>
                                                <tr class="borderless">
                                                    <th colspan="2">Address</th>
                                                </tr>

                                                <tr>
                                                    <td colspan="2">{{$address_new}} , {{$data['area']}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Service Amount</th>
                                                    <td>AED {{number_format($data['amount'],2,'.','')}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Transaction Charge</th>
                                                    <td>AED {{number_format($data['transaction_charge'],2,'.','')}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Payable Amount</th>
                                                    <td>AED {{number_format($data['gross_amount'],2,'.','')}}</td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                    </div>
                                    
                                </div> 
                            </div>
                        </div>
                    </div>  
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <a class="cursor" href="{{URL::to('house-cleaning-dubai')}}"><span class="em-back-arrow back-to1-btn" title="Home"></span></a>
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                            <button id="book_payment_button" class="text-field-button show6-step reset-button" type="button">Confirm Payment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
<div class="col-sm-12 popup-main" id="payment_popup">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
          <form id="payment-form" method="POST" action="{{url('make-pay-checkout')}}">
               {{ csrf_field() }}
                    <h5 class="">Make Payment <span class="em-forgot-close-btn"><img src="{{asset('images/el-close-black.png')}}" title="" style=""></span></h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                         <div class="row m-0">
                            <input type="hidden" id="token_req" name="token_req" value=''>
                            <?php
                                    $pay_id = (1000 + $data['pay_details']['payment_id']);
                                    $name_new = preg_replace("/[^a-zA-Z0-9\s]/", "", $data['customer_name']);
                                    ?>
                                    <input type="hidden" id="customerId" name="customerId" value="">

										<input type="hidden" name="order_id" id="order_id" value="<?php echo $data['pay_details']['payment_id'];?>"/>
										<input type="hidden" name="amount" id="amount" value="<?php echo number_format($data['gross_amount'],2,'.','');  ?>"/>
										<input type="hidden" name="currency" value="AED"/>                        
										<input type="hidden" name="language" value="EN"/>
                                        <input type="hidden" id="retry_online" name="retry_online" value="online">                                         
										<input type="hidden" name="billing_zip" value=""/>
										<input type="hidden" name="billing_country" value="UAE"/>
										<input type="hidden" name="billing_name" id="billing_name" value="<?php echo $name_new; ?>"/>
										<input type="hidden" name="billing_tel" id="billing_tel" value="<?php echo $data['phone_number']; ?>"/>
										<input type="hidden" name="billing_email" id="billing_email" value="<?php echo $data['customer_email']; ?>"/>
										<input type="hidden" name="billing_address" id="billing_address" value="<?php echo $address_new; ?>"/>
										<input type="hidden" name="billing_state" id="billing_state" value="Dubai"/>
										<input type="hidden" name="billing_city" id="billing_city" value="Dubai"/>
										 <?php if(isset($_GET['mobile'])) 
                                   $mobileParam = '?mobile=active';
                              ?>
                              <?php if(!isset($_GET['mobile'])) 
                                   $mobileParam = '';
                              ?>
                              <input type="hidden" name="mobile_view" id="mobile_view" value="{{$mobileParam}}"/>
                                <div class="one-liner">
                                <div class="card-frame">
                                </div>                               
                                </div>
                         </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <button id="pay-button" disabled>
                            PAY AED {{number_format($data['gross_amount'],2,'.','')}}   </button>   
                                                 </div>  
               </form>    
          </div>
     </div>
</div><!--popup-main end-->
<style>
.make_payment_wrapper {
    max-width: 500px;
    margin: 0px auto;
}
.make_payment_wrapper form{
    width: 100%;
}
table.payment_table {
    width: 100%;
    border: 1px solid #eee;
}
table.payment_table td, table.payment_table th {
    padding: 6px 10px;
}
.reset-button {
    width: 100% !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
</style>

@endsection
@push('scripts')
<script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
<script>
var payButton = document.getElementById("pay-button");

var form = document.getElementById("payment-form");
Frames.init($('#checkout_secret').val());
Frames.addEventHandler(
  Frames.Events.CARD_VALIDATION_CHANGED,
  function (event) {
    payButton.disabled = !Frames.isCardValid();
  }
);
Frames.addEventHandler(
Frames.Events.CARD_TOKENIZED,
function (event) {
$('#token_req').val(event.token);
$("#payment-form").submit();
}
);
if(form) {
form.addEventListener("submit", function (event) {
    event.preventDefault();
    Frames.submitCard();
});
}
$(document).ready(function() {
    $('.logo').addClass('logo_left');
    if($('#customerId').val() == 0 ) {
        var customerId = localStorage.getItem('customerId');
        if ((customerId) && customerId != '' && customerId != 0 ) {
            $('.login').hide();
            $('.logout').show();
            $('.userPersonel').show();
            $('.make_pay_head').show();
            $('#customerId').val(customerId);
        } else{
            $('.login').show();
            $('.logout').hide();
            $('.userPersonel').hide();
            $('.make_pay_head').hide();
        }
    } else {
        localStorage.setItem('customerId',$('#customerId').val());
        $('.login').hide();
        $('.logout').show();
        $('.userPersonel').show();
        $('.make_pay_head').show();
    }  
});

$('#book_payment_button').click(function() {
    $('#payment_popup').show(500);
   });
   $('.em-forgot-close-btn').click(function(){
$('#payment_popup').hide(500);
});
</script>
@endpush