@extends('layouts.app')

@section('title') {{'Floor Cleaning Services Dubai'}} @endsection

@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          <input type="hidden" id="all_amount" value="{{serialize($allAmount)}}">
          <input type="hidden" id="number_hours" value="0">		  		  
		  <input type="hidden" id="serviceNamePay" value="FloorCleaning">
               @include('Floor.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script  type="text/javascript" src="{{URL::asset('js/select2.full.min.js')}}"></script>
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(".buildingType").click(function(){ 
var selValue = $("input[type='radio']:checked").val();
if (selValue == '15') {
    $('#apart_div').show();
    $('#villa_div').hide();
    $('#aprt_fur').show();
    $('#villa_fur').hide();
    // $('#furnished_hidden').val(7);
   
} else {
    $('#apart_div').hide();
    $('#villa_div').show();
    $('#villa_fur').hide();
    // $('#furnished_hidden').val(9);
    $('#aprt_fur').show();

}
amountCalculation();
});
$(document).ready(function() {
    $('#noOfHours').val(2);
	getAvailability();
    amountCalculation();
    $(".js-select2").select2();

});
function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
}

// var ul = document.getElementById('noHours');
// ul.onclick = function(event) {
//      var target = getEventTarget(event);
//      $('#noOfHours').val(target.innerHTML);
//      $('#number_hours').val(target.innerHTML);
//      $('.hrs_p').text(target.innerHTML);
//      $('#noOfHours-error').hide();
//      amountCalculation();
//          getAvailability();

// };
// // var increment = 2;
// function addNumber()
// {
//      increment = $('#noOfMaids').val();
//      if(increment <= 3) {
//           increment++;
//           console.log(increment);
//           $('#noOfMaids').val(increment);
//           $('.maid_id_p').text(increment);
//           $('#noOfMaids-error').hide();
//           amountCalculation();
//      }
// }
// // console.log(dec);
// function noofmaidchange()
// {
//      var change = $('#noOfMaids').val();
//      if( change < 1 && change !='' ) 
//      change=1;
//      else if(change>10)
//      change=10;
//      $('#noOfMaids').val(change);
//      $('.maid_id_p').text(change);
//      $('#noOfMaids-error').hide();
//      if(change !='')
//      amountCalculation();
// }

// function removeNumber()
// {
//      var dec = $('#noOfMaids').val();
//      // console.log(dec);
//      if( dec > 2 ) {
//      dec--;
//      $('#noOfMaids').val(dec);
//      $('.maid_id_p').text(dec);
//      $('#noOfMaids-error').hide();
//      amountCalculation();
//      }
// }
$('#furnished_unfurnished li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();
}); 
$('#furnished_unfurnished2 li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();
}); 
$('#furnished_unfurnished3 li').on('click', function(){
    // alert("in");
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
     $('#furnished_hidden').val(this.id);
     amountCalculation();

}); 
$('#bedroom_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
});
$('#villa_ul li').on('click', function(){
     $(this).siblings().removeClass('selected');
     $(this).addClass('selected');
}); 
var ul = document.getElementById('villa_ul');
ul.onclick = function(event) { 
    var target = getEventTarget(event);
    $('#villa_id').val(target.innerHTML);
    amountCalculation();
};
var ul = document.getElementById('bedroom_ul');
ul.onclick = function(event) {
    var target = getEventTarget(event);
    $('#bedroom_count').val(target.innerHTML);
    amountCalculation();
};

function amountCalculation()
{
 // debugger;
    var furnished =  $('#furnished_hidden').val();
    var category = $('#service_category').val(); //18 - Home
    var num_hrs=0;
    var num_maids=0;
        var subCategory =  $("input[name='building']:checked").val();
        if(subCategory == 16) { //villa
            var bedroom = $('#villa_id').val();
            var furnishedFirst =  $('#furnished_hidden').val();
            if (furnishedFirst == 7) {  // apartment furnished
                furnished = 9; //villa furnished
                if(bedroom == 2) {
                    num_hrs =3;
                    num_maids =3;
                } else if(bedroom ==3) {
                    num_hrs =4;
                    num_maids =3;
                } else if(bedroom ==4) {
                    num_hrs =4;
                    num_maids =4;
                } else if(bedroom ==5) {
                    num_hrs =6;
                    num_maids =3;
                } else if(bedroom ==6) {
                    num_hrs =6;
                    num_maids =4;
                } else if(bedroom ==7) {
                    num_hrs =8;
                    num_maids =3;
                } else if(bedroom ==8) {
                    num_hrs =8;
                    num_maids =4;
                } 

            } else if (furnishedFirst == 8) { //aprtment unfurnished
                furnished = 10; //villa unfurnished
                if(bedroom == 2) {
                    num_hrs =4;
                    num_maids =2;
                } else if(bedroom ==3) {
                    num_hrs =5;
                    num_maids =2;
                } else if(bedroom ==4) {
                    num_hrs =6;
                    num_maids =2;
                } else if(bedroom ==5) {
                    num_hrs =8;
                    num_maids =2;
                } else if(bedroom ==6) {
                    num_hrs =6;
                    num_maids =3;
                } else if(bedroom ==7) {
                    num_hrs =7;
                    num_maids =3;
                } else if(bedroom ==8) {
                    num_hrs =8;
                    num_maids =3;
                } 
            }
          
        } else { //apartment - 15
            var bedroom =$('#bedroom_count').val();
            var furnished=  $('#furnished_hidden').val();
            if (furnished == 7) {  // apartment furnished
                if(bedroom == 1) {
                    num_hrs =3;
                    num_maids =2
                }
                else if(bedroom == 2) {
                    num_hrs =4;
                    num_maids =2;
                } else if(bedroom ==3) {
                    num_hrs =5;
                    num_maids =2;
                } else if(bedroom ==4) {
                    num_hrs =6;
                    num_maids =2;
                } else if(bedroom ==5) {
                    num_hrs =5;
                    num_maids =3;
                } else if(bedroom ==6) {
                    num_hrs =6;
                    num_maids =3;
                } else if(bedroom ==7) {
                    num_hrs =8;
                    num_maids =3;
                } 

            } else if (furnished == 8) { //aprtment unfurnished
                if(bedroom == 1) {
                    num_hrs =2;
                    num_maids =2;
                }
                else if(bedroom == 2) {
                    num_hrs =3;
                    num_maids =2;
                } else if(bedroom ==3) {
                    num_hrs =4;
                    num_maids =2;
                } else if(bedroom ==4) {
                    num_hrs =5;
                    num_maids =2;
                } else if(bedroom ==5) {
                    num_hrs =6;
                    num_maids =2;
                } else if(bedroom ==6) {
                    num_hrs =7;
                    num_maids =2;
                } else if(bedroom ==7) {
                    num_hrs =8;
                    num_maids =2;
                } 
            }
          
        }
    $('#noOfHours').val(num_hrs);
    $('#noOfMaids').val(num_maids);
         getAvailability();

    var is_scrubbing = 0;
    var serviceId = $('#service_id').val();
    var selectedArray = [];
    var coupon = $('.couponClass').val();
    selectedArray.push({service_id : serviceId,category_id : category,sub_category_id : subCategory,name : bedroom,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    var intialArray = $('#all_amount').val();
  
 
    $.ajax({
          method: 'post',
          url: '{{url('calculate-load')}}',
          data:{'selectedArray':selectedArray,'intialArray':intialArray,'officesqure':$('#officesqure').val(),'category':category,_token:'{{csrf_token()}}'},  
         
          success: function (result) {
               if (result.status == 'success') {
                   $('.total_p').text(result.data['amount']);
                   if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                        $('.vat_p').text(result.data['vat_amount']);
                        $('.net_p').text(result.data['totalAmount']);
                        $('.mobile_net_p').text(result.data['totalAmount']);
                        $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }
                   $('#arrayVal').val(result.data['arrayVal']);
               }
          }
     }); 
}

    function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
    }



function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}
</script>
@endpush

