@extends('layouts.app')
@section('title')
    {{ $package['package_name'] }}
@endsection
@section('content')
    <style>
        .gj-picker-bootstrap [role=header] {
            background: #226db4;
        }

        .gj-picker-bootstrap.datepicker [role=header] [role=year] {
            color: rgb(221, 221, 221);
            ;
        }

        .gj-picker-bootstrap.datepicker [role=header] [role=date] {
            color: rgb(221, 221, 221);
        }

        .gj-picker-bootstrap.datepicker [role=body] {
            background-color: rgb(243, 243, 243);
        }
        .gj-picker-bootstrap table tr td.disabled div {
            background-color: #e9e9e9;
            color: rgb(255, 137, 137);
        }
    </style>
    <form method="post" id="ccavenue_form" action="{{ config('payment.ccavenue_req_handler') }}?{{ @$query_params }}"
        style="display: none">
        {!! csrf_field() !!}
        <table width="40%" height="100" border='1' align="center">
            <caption>
                <font size="4" color="blue"><b>Integration Kit</b></font>
            </caption>
        </table>
        <table width="40%" height="100" border='1' align="center">
            <tr>
                <td>Parameter Name:</td>
                <td>Parameter Value:</td>
            </tr>
            <tr>
                <td colspan="2"> Compulsory information</td>
            </tr>
            <tr>
                <td>Merchant Id :</td>
                <td><input type="text" name="merchant_id" value="{{ config('payment.ccavenue_merchant_id') }}" /></td>
            </tr>
            <tr>
                <td>Order Id :</td>
                <td><input type="text" name="order_id" value="" /></td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="text" name="amount" value="" /></td>
            </tr>
            <tr>
                <td>Currency :</td>
                <td><input type="text" name="currency" value="AED" /></td>
            </tr>
            <tr>
                <td>Redirect URL :</td>
                <td><input type="text" name="redirect_url"
                        value="{{ config('payment.ccavenue_resp_handler') }}?{{ @$query_params }}" /></td>
            </tr>
            <tr>
                <td>Cancel URL :</td>
                <td><input type="text" name="cancel_url"
                        value="{{ config('payment.ccavenue_resp_handler') }}?{{ @$query_params }}" /></td>
            </tr>
            <tr>
                <td>Language :</td>
                <td><input type="text" name="language" value="EN" /></td>
            </tr>
            <tr>
                <td colspan="2">Billing information(optional):</td>
            </tr>
            <tr>
                <td>Billing Name :</td>
                <td><input type="text" name="billing_name" value="" /></td>
            </tr>
            <tr>
                <td>Billing Address :</td>
                <td><input type="text" name="billing_address" value="" /></td>
            </tr>
            <tr>
                <td>Billing City :</td>
                <td><input type="text" name="billing_city" value="" /></td>
            </tr>
            <tr>
                <td>Billing State :</td>
                <td><input type="text" name="billing_state" value="" /></td>
            </tr>
            <tr>
                <td>Billing Zip :</td>
                <td><input type="text" name="billing_zip" value="" /></td>
            </tr>
            <tr>
                <td>Billing Country :</td>
                <td><input type="text" name="billing_country" value="" /></td>
            </tr>
            <tr>
                <td>Billing Tel :</td>
                <td><input type="text" name="billing_tel" value="" /></td>
            </tr>
            <tr>
                <td>Billing Email :</td>
                <td><input type="text" name="billing_email" value="" /></td>
            </tr>
            <tr>
                <td>Merchant Param 1 :</td>
                <!-- success url prefix -->
                <td><input type="text" name="merchant_param1" value="" /></td>
            </tr>
            <tr>
                <td>Merchant Param 2 :</td>
                <!-- failed url prefix -->
                <td><input type="text" name="merchant_param2" value="" /></td>
            </tr>
            <tr>
                <td></td>
                <td><INPUT TYPE="submit" value="CheckOut"></td>
            </tr>
        </table>
    </form>
    <section class="em-booking-content-section">
        <div class="col-sm-12 popup-main" id="payment_popup_applepay">
            <div class="row min-vh-100 d-flex flex-column justify-content-center">
                <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                    <h5 class="">Apple Pay <span class="em-forgot-close-btn close-pay-poup"><img
                                src="{{ asset('images/el-close-black.png') }}" title="" style=""></span></h5>
                    <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                        <div class="row m-0">
                            <input type="hidden" name="order_id" id="ap_order_id">
                            <input type="hidden" name="amount" id="ap_amount">
                            <input type="hidden" name="customer_name" id="ap_billing_name">
                            <input type="hidden" name="customer_tel" id="ap_billing_tel">
                            <input type="hidden" name="customer_email" id="ap_billing_email">
                            <input type="hidden" name="customer_address" id="ap_billing_address">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--popup-main end-->
        <div class="col-sm-12 popup-main" id="payment_popup">
            <div class="row min-vh-100 d-flex flex-column justify-content-center">
                <div class="col-md-3 col-sm-6 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                    <form id="payment-form" method="POST" action="{{ url('package/payment/card/checkout') }}">
                        {{ csrf_field() }}
                        <h5 class="">Make Payment <span class="em-forgot-close-btn close-pay-poup"
                                id="cancel-card-pay"><img src="{{ asset('images/el-close-black.png') }}" title=""
                                    style=""></span>
                        </h5>
                        <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                            <div class="row m-0">
                                <input type="hidden" id="token_req" name="token_req" value=''>
                                <input type="hidden" name="order_id" id="order_id">
                                <input type="hidden" name="payment_id" id="payment_id">
                                <input type="hidden" name="amount" value="{{ $package['amount'] }}">
                                <input type="hidden" name="currency" value="AED">
                                <input type="hidden" id="retry_online" name="retry_online" value="not_online">
                                <?php if (isset($_GET['mobile'])) {
                                    $mobileParam = '?mobile';
                                }
                                ?>
                                <?php if (!isset($_GET['mobile'])) {
                                    $mobileParam = null;
                                }
                                ?>
                                <input type="hidden" name="mobile_view" id="mobile_view"
                                    value="{{ $mobileParam }}" />
                                <input type="hidden" name="merchant_param2" id="merchant_param2"
                                    value="{{ $mobileParam }}">
                                <input type="hidden" name="customer_name" id="billing_name">
                                <input type="hidden" name="customer_tel" id="billing_tel">
                                <input type="hidden" name="customer_email" id="billing_email">
                                <div class="one-liner">
                                    <div class="card-frame">
                                        <!-- form will be added here -->
                                    </div>
                                </div>
                                <p class="error-message text-danger"></p>
                                <p class="success-payment-message"></p>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0 text-center">
                            <button id="pay-button" disabled>PAY AED <span id="amount_span">343</span> </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--popup-main end-->
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                    <div class="col-12 n-packages-details">

                        <div class="col-sm-12 pl-0 pr-0">
                            <div class="pra-image"><img src="{{ $package['promotion_image_url'] }}" /></div>
                        </div>

                        <div class="col-sm-12 pl-0 pr-0">
                            <h4>{{ $package['package_name'] }}</h4>
                            <p class="pt-3">{{ $package['description'] }}</p>
                            <p class="pt-3">Once a week, with {{ $package['working_hours'] }}
                                hour{{ $package['working_hours'] > 1 ? 's' : '' }} per service for
                                {{ $package['no_of_bookings'] }} visits.</p>
                        </div>
                        <form class="row m-0 p-0" id="package-payment-basic-form">
                            <input type="hidden" value="{{ isWebView() ? 'M' : 'W' }}" name="platform">
                            <div class="col-lg-10 col-md-12 p-0">
                                <div class="row n-package-cont-details">

                                    <div class="col-sm-4 col-sm-4 pb-4">
                                        <div class="cm-field-main">
                                            <p>Number of Hours</p>
                                            <input class="input-field disable" type="number"
                                                value="{{ $package['working_hours'] }}" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4  pb-4">
                                        <div class="cm-field-main">
                                            <p>Number of Visit</p>
                                            <input class="input-field disable" type="number"
                                                value="{{ $package['no_of_bookings'] }}" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4  pb-4">
                                        <div class="cm-field-main">
                                            <p>Date</p>
                                            <input class="input-field n-calender" type="text" name="service_date"
                                                id="service_date"
                                                value="{{ $_SERVER['HTTP_HOST'] == 'localhost' ? date('d-m-Y') : '' }}"
                                                readonly>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 pb-4">
                                        <div class="cm-field-main">
                                            <p>Note</p>
                                            <textarea class="input-field-big" spellcheck="true" name="notes" id="notes">{{ $_SERVER['HTTP_HOST'] == '127.0.0.1:8000' ? 'Test' : '' }}</textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <div class="col-lg-10 col-md-12 em-field-main p-0">
                                <div class="col-sm-12 em-extra-ser-set n-package-payment-set p-0">
                                    <p>Payment Mode</p>
                                    <input type="hidden" value="{{ session('customerId') }}" id="customer_id" name="customer_id">
                                    <input type="hidden" value="{{ $package['package_id'] }}" id="package_id" name="package_id">
                                    <input type="hidden" value="{{ $package['amount'] }}" id="amount" name="amount">
                                    <input type="hidden" id="payment_mode" name="payment_mode">
                                    <input type="hidden" id="transaction_charge" name="transaction_charge" value="0">
                                    <input type="hidden" id="total_amount" name="total_amount" value="0">
                                    <div class="row" id="payment_div">
                                        <div class="col-lg-3 col-sm-6 col-6 payment-mode" data-mode="cash"
                                            data-trans-charge="0" data-trans-charge-type="F">
                                            <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                <div class="n-upi-set em-extra-ser-img"
                                                    style="padding-left: 5px !important; padding-right: 5px !important;">
                                                    <img src="{{ url('images/cash-payment.jpg') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-6 payment-mode" data-mode="card"
                                            data-trans-charge="3" data-trans-charge-type="P">
                                            <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                <div class="n-upi-set em-extra-ser-img"
                                                    style="padding-left: 5px !important; padding-right: 5px !important;">
                                                    <img src="{{ url('images/card-payment.jpg') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-lg-3 col-sm-6 col-6 payment-mode" data-mode="gpay">
                                                                    <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                                        <div class="n-upi-set em-extra-ser-img"
                                                                            style="padding-left: 5px !important; padding-right: 5px !important;">
                                                                            <img src="{{ url('images/n-gpay.jpg') }}" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-sm-6 col-6 payment-mode" data-mode="applepay">
                                                                    <div class="row ml-0 mr-0 em-extra-ser-tmb" id="applepaytmb">
                                                                        <div class="n-upi-set em-extra-ser-img"
                                                                            style="padding-left: 5px !important; padding-right: 5px !important;">
                                                                            <img src="{{ url('images/n-applepay.jpg') }}" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-10 col-md-12 em-field-main pl-0 pr-0">
                                <div class="n-pice-st"><span>Service Amount : </span>
                                    <label>&nbsp;AED&nbsp;<span class="service_amount">{{ $package['amount'] }}</span> &nbsp; </label>
                                </div>
                                <div class="n-pice-st"><span>Convenience Fee : </span>
                                    <label>&nbsp;AED&nbsp;<span class="transaction_charge">0</span> &nbsp; </label>
                                </div>
                                <div class="n-pice-st font-weight-bold"><span>Total Payable : </span>
                                    <label>&nbsp;AED&nbsp;<span class="total_amount">{{ $package['amount'] }}</span> &nbsp; </label>
                                </div>
                            </div>


                            <div class="col-lg-6 pt-3 pl-0 pr-0">
                                <button class="text-field-button col-sm-6" type="submit" id="make-payment"><span
                                        class="default">Make Payment</span><span class="loading"
                                        style="display:none;">Please Wait...</span></button>
                                <button type="button" id="applePaybtn" class="text-field-button col-sm-6"
                                    style="display:none;"></button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div>
        <!--page bottom white space-->
    </section>
@endsection
@push('styles')
    <link href="{{ url('css/gijgo.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .bg-color {
            background: #fff;
        }

        #applePaybtn {
            width: 150px;
            height: 45px;
            display: none;
            border-radius: 5px;
            margin-left: auto;
            margin-right: auto;
            background-image: -webkit-named-image(apple-pay-logo-white);
            background-position: 50% 50%;
            background-color: black;
            background-size: 60%;
            background-repeat: no-repeat;
        }
    </style>
@endpush
@push('scripts')
    <script src="{{ url('js/packages.js?v=') . Config::get('version.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.checkout.com/js/framesv2.min.js"></script>
    <script src="{{ url('js/gijgo.min.js') }}" type="text/javascript"></script>
    <script async src="https://pay.google.com/gp/p/js/pay.js"></script>
    <script src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"></script>
    <script type="text/javascript">
        var _payment_ref_id;
        var debug = true;
        //var merchantIdentifier = "{{ Config::get('values.apple_merchant_id') }}"; // Live
        var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser.live'; // Demo
        $.validator.addMethod("customemail",
            function(value, element) {
                return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    .test(value);
            },
            "Please enter a valid email id. "
        );
        var checkoutpkey = "{{ Config::get('values.checkout_primary_key') }}";
        var customer_id = {{ session('customerId') ?: 'null' }};
        var payButton = document.getElementById("pay-button");
        var form = document.getElementById("payment-form");
        var errorStack = [];
        if (window.ApplePaySession) {
            //var merchantIdentifier = 'merchant.com.emaid.elitmaidsUser';
            var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
            promise.then(function(canMakePayments) {
                if (canMakePayments) {
                    logit('ApplePay Supported !');
                } else {
                    logit('ApplePay is possible on this browser, but not currently activated !');
                }
            });
        } else {
            //$("#payment_div div[data-mode='applepay']").hide();
            logit('ApplePay is not available on this browser !');
        }

        function logit(data) {
            if (debug == true) {
                console.log(data);
            }
        };
        Frames.init('{{ Config::get('values.checkout_primary_key') }}');
        Frames.addEventHandler(Frames.Events.CARD_VALIDATION_CHANGED, function(event) {
            logit("CARD_VALIDATION_CHANGED: %o", event);
            payButton.disabled = !Frames.isCardValid();
        });
        Frames.addEventHandler(
            Frames.Events.FRAME_VALIDATION_CHANGED,
            onValidationChanged
        );

        function onValidationChanged(event) {
            logit("FRAME_VALIDATION_CHANGED: %o", event);

            var errorMessageElement = document.querySelector(".error-message");
            var hasError = !event.isValid && !event.isEmpty;

            if (hasError) {
                errorStack.push(event.element);
            } else {
                errorStack = errorStack.filter(function(element) {
                    return element !== event.element;
                });
            }
            var errorMessage = errorStack.length ?
                getErrorMessage(errorStack[errorStack.length - 1]) :
                "";
            errorMessageElement.textContent = errorMessage;
        }

        function getErrorMessage(element) {
            var errors = {
                "card-number": "Please enter a valid card number",
                "expiry-date": "Please enter a valid expiry date",
                cvv: "Please enter a valid cvv code",
            };

            return errors[element];
        }
        Frames.addEventHandler(Frames.Events.FRAME_ACTIVATED, function(event) {});
        Frames.addEventHandler(Frames.Events.FRAME_BLUR, function(event) {});
        Frames.addEventHandler(Frames.Events.CARD_TOKENIZED, function(event) {
            //alert('CARD_TOKENIZED');
            $('#token_req').val(event.token);
            $("#payment-form").submit();
        });
        Frames.addEventHandler(Frames.Events.CARD_TOKENIZATION_FAILED, function(event) {
            Frames.enableSubmitForm();
            alert('CARD_TOKENIZATION_FAILED');
        });
        Frames.addEventHandler(Frames.Events.CARD_TOKENIZED, onCardTokenized);

        function onCardTokenized(event) {
            var el = document.querySelector(".success-payment-message");
        }
        if (form) {
            form.addEventListener("submit", function(event) {
                event.preventDefault();
                logit(event);
                $(".preloader").show();
                Frames.submitCard();
            });
        }
    </script>
    <script>
        $('#service_date').datepicker({
            showRightIcon: false,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            format: 'dd-mm-yyyy',
            disableDaysOfWeek: [{{implode (@$weekends ?: [])}}],
            disableDates: [
                {!! @$holiday_dates ? "moment('".implode ( "','YYYY-MM-DD').format('DD-MM-YYYY'),moment('",@$holiday_dates )."','YYYY-MM-DD').format('DD-MM-YYYY')" : ''!!}
				],
            modal: _is_webview,
            header: true,
            //footer: true,
            minDate: moment().format('DD-MM-YYYY'),
            maxDate: function() {
                var date = new Date();
                date.setDate(date.getDate() + 365);
                return new Date(date.getFullYear(), date.getMonth(), date.getDate());
            },
            change: function(e) {
                $('#service_date').valid();
            }
        });
        $(document).ready(function() {
            $('#package-payment-basic-form').validate({
                rules: {
                    service_date: {
                        required: true,
                    },
                    payment_mode: {
                        required: true,
                    }
                },
                messages: {
                    service_date: {
                        required: "Select service date",
                    },
                    payment_mode: {
                        required: "Select payment method",
                    }
                },
                ignore: [],
                focusInvalid: false,
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if ($(element).attr("id") == "payment_mode") {
                        error.insertAfter($('#payment_div'));
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    if (customer_id == null) {
                        viewLogin();
                        return;
                    }
                    $(".preloader").show();
                    //$('#make-payment .default').hide() && $('#make-payment .loading').show();
                    $.ajax({
                        url: "{{ Config::get('api.api_url') . 'package/make-payment' }}",
                        type: "post",
                        dataType: "json",
                        data: $("#package-payment-basic-form").serialize(),
                        success: function(response) {
                            if (response.status == true) {
                                _payment_ref_id = response.data.payment.reference_id;
                                logit(response);
                                if ($('#payment_mode').val() == "cash") {
                                    window.open(
                                        "{{ url('package/payment/success') }}/" +
                                        response.data.payment.reference_id,
                                        "_self")
                                } else if ($('#payment_mode').val() == "applepay") {
                                    $('#ap_order_id').val(response.data.booking
                                        .reference_id);
                                    $('#ap_amount').val(response.data.payment.amount);
                                    $('#ap_billing_name').val(response.data.customer
                                        .customer_name);
                                    $('#ap_billing_tel').val(response.data.customer
                                        .mobile_number_1);
                                    $('#ap_billing_email').val(response.data.customer
                                        .email_address);
                                    $('#ap_billing_address').val(response.data.address
                                        .customer_address);
                                    $("#make-payment").hide();
                                    $("#applePaybtn").show();
                                    //$("#applePaybtn").trigger('click');
                                    $(".preloader").fadeOut(1000);
                                } else if ($('#payment_mode').val() == "card") {
                                    /******************************************************************/
                                    // for cc-avenue form only
                                    $("#ccavenue_form input[name='order_id']").val(response
                                        .data['booking']['reference_id']);
                                    $("#ccavenue_form input[name='amount']").val(response
                                        .data['payment']['total_amount']);
                                    $("#ccavenue_form input[name='redirect_url']").val(
                                        "{{ url('package/payment/ccavenue_response_handler') }}"
                                    );
                                    $("#ccavenue_form input[name='cancel_url']").val(
                                        "{{ url('package/payment/ccavenue_response_handler') }}"
                                    );
                                    $("#ccavenue_form input[name='billing_name']").val(
                                        response.data['customer']['customer_name']);
                                    $("#ccavenue_form input[name='billing_address']").val(
                                        response.data['customer_address'][
                                            'customer_address'
                                        ]);
                                    //$("#ccavenue_form input[name='billing_city']").val();
                                    //$("#ccavenue_form input[name='billing_state']").val();
                                    //$("#ccavenue_form input[name='billing_zip']").val();
                                    $("#ccavenue_form input[name='billing_country']").val(
                                        "United Arab Emirates");
                                    $("#ccavenue_form input[name='billing_tel']").val(
                                        response.data['customer']['phone_number']);
                                    $("#ccavenue_form input[name='billing_email']").val(
                                        response.data['customer']['email_address']);
                                    $("#ccavenue_form input[name='merchant_param1']").val(
                                        "{{ Config::get('url.package_payment_success_slug_prefix') }}"
                                    ); // success url prefix
                                    $("#ccavenue_form input[name='merchant_param2']").val(
                                        "{{ Config::get('url.package_payment_failed_slug_prefix') }}"
                                    ); // fail url prefix
                                    $("#ccavenue_form").submit();
                                    /******************************************************************/
                                    return false;
                                    var tamount = parseFloat(response.data.payment
                                        .transaction_charge) + parseFloat(response.data
                                        .payment.amount);
                                    tamount = tamount.toFixed(2);
                                    $('#amount_span').html(tamount);
                                    $('#order_id').val(response.data.booking.reference_id);
                                    logit('order_id ~ ' + $('#order_id').val());
                                    $('#payment_id').val(response.data.payment.id);
                                    $('#amount').val(tamount);
                                    $('#billing_name').val(response.data.customer
                                        .customer_name);
                                    $('#billing_tel').val(response.data.customer
                                        .mobile_number_1);
                                    $('#billing_email').val(response.data.customer
                                        .email_address);
                                    //$('#billing_address').val(result.data['customer_address']['customer_address']);
                                    $('#payment_popup').show(0);
                                    $(".preloader").hide();
                                    $("#cancel-card-pay").click(function() {
                                        $(".preloader").show();
                                        window.location.href =
                                            "{{ url('package/payment/failed') }}/" +
                                            response.data.booking.reference_id;
                                    });
                                } else if ($('#payment_mode').val() == 'gpay') {
                                    var total_amount_payable = $('#amount').val();
                                    var refId = response.data.booking.reference_id;
                                    var email = response.data.customer.email_address;
                                    var name = response.data.customer.customer_name;
                                    const baseRequest = {
                                        apiVersion: 2,
                                        apiVersionMinor: 0
                                    };

                                    const tokenizationSpecification = {
                                        type: 'PAYMENT_GATEWAY',
                                        parameters: {
                                            'gateway': 'checkoutltd',
                                            'gatewayMerchantId': checkoutpkey
                                        }
                                    };

                                    const allowedCardNetworks = ["AMEX", "DISCOVER",
                                        "INTERAC", "JCB", "MASTERCARD", "VISA"
                                    ];

                                    const allowedCardAuthMethods = ["PAN_ONLY",
                                        "CRYPTOGRAM_3DS"
                                    ];

                                    const baseCardPaymentMethod = {
                                        type: 'CARD',
                                        parameters: {
                                            allowedAuthMethods: allowedCardAuthMethods,
                                            allowedCardNetworks: allowedCardNetworks
                                        }
                                    };

                                    const cardPaymentMethod = Object.assign({
                                            tokenizationSpecification: tokenizationSpecification
                                        },
                                        baseCardPaymentMethod
                                    );

                                    const paymentsClient = new google.payments.api
                                        .PaymentsClient({
                                            environment: '{{ Config::get('values.package_gpay_environment') }}'
                                        });

                                    const isReadyToPayRequest = Object.assign({},
                                        baseRequest);
                                    isReadyToPayRequest.allowedPaymentMethods = [
                                        baseCardPaymentMethod
                                    ];

                                    paymentsClient.isReadyToPay(isReadyToPayRequest)
                                        .then(function(response) {
                                            if (response.result) {
                                                const button =
                                                    paymentsClient.createButton({
                                                        onClick: () => logit(
                                                            'TODO: click handler'
                                                        ),
                                                        allowedPaymentMethods: []
                                                    }); // make sure to provide an allowed payment method
                                                //document.getElementById('gpaybtn').appendChild(button);
                                            }
                                        })
                                        .catch(function(err) {
                                            $(".preloader").fadeOut(1000);
                                            $("#success_pay_message").text(
                                                "Something went wrong. Try again.");
                                            $("#paymenterrormessagepopup").show();
                                        });

                                    const paymentDataRequest = Object.assign({},
                                        baseRequest);
                                    paymentDataRequest.allowedPaymentMethods = [
                                        cardPaymentMethod
                                    ];
                                    paymentDataRequest.transactionInfo = {
                                        totalPriceStatus: 'FINAL',
                                        totalPrice: total_amount_payable,
                                        currencyCode: 'AED',
                                        countryCode: 'AE'
                                    };
                                    paymentDataRequest.merchantInfo = {
                                        merchantName: 'Elite Advance Building Cleaning LLC',
                                        merchantId: 'BCR2DN4TXLQ3FHS6'
                                    };

                                    paymentsClient.loadPaymentData(paymentDataRequest).then(
                                        function(paymentData) {
                                            logit(paymentData);
                                            paymentToken = paymentData.paymentMethodData
                                                .tokenizationData.token;
                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $(
                                                        'meta[name="csrf-token"]'
                                                    ).attr('content')
                                                }
                                            });

                                            $.ajax({
                                                method: 'post',
                                                url: '{{ url('package/payment/gpay/checkout') }}',
                                                data: {
                                                    'amount': total_amount_payable,
                                                    'reference_id': refId,
                                                    'paymentToken': paymentToken,
                                                    'email': email,
                                                    'name': name,
                                                    _token: '{{ csrf_token() }}'
                                                },
                                                success: function(result) {
                                                    logit(result);
                                                    if (result.status ==
                                                        'success') {
                                                        window.location
                                                            .href = result
                                                            .url;
                                                    } else {
                                                        window.location
                                                            .href = result
                                                            .url;
                                                    }
                                                }
                                            });
                                        }).catch(function(err) {
                                        logit(err);
                                        $(".preloader").show();
                                        window.location.href =
                                            "{{ url('package/payment/failed') }}/" +
                                            response.data.booking.reference_id;
                                    });
                                    //paymentsClient.prefetchPaymentData(paymentDataRequest);
                                }
                            } else {
                                //$('#make-payment .loading').hide()
                                //$('#make-payment .default').show();
                                $(".preloader").hide();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            //$('#make-payment .loading').hide();
                            //$('#make-payment .default').show();
                            $(".preloader").hide();
                        }
                    });
                }
            });
        });
        $(".payment-mode").click(function() {
            $('#payment_mode').val($(this).data("mode")); // save mode
            $('#payment_div .payment-mode').children().removeClass("selected"); // remove selection
            $(this).children().first().addClass("selected"); // highlight current
            $('#payment_mode').valid(); // validate
            $('#transaction_charge').val(0); // reset
            $('#applePaybtn').hide(); // reset
            $('#make-payment').show(); // reset
            if ($(this).data("mode") == 'cash') {
                $('#transaction_charge').val(0); // cash have transaction charge
            } else if ($(this).data("mode") == 'applepay') {
                if (window.ApplePaySession) {
                    var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
                    promise.then(function(canMakePayments) {
                        if (canMakePayments) {
                            if (_payment_ref_id == undefined) $("#package-payment-basic-form").submit();
                            logit("Apple Payment Supported !");
                        } else {
                            $('#payment_mode').val(''); // reset
                            $('#payment_div .payment-mode').children().removeClass(
                                "selected"); // remove selection
                            $(".preloader").fadeOut(1000);
                            $("#applePaybtn").hide();
                            $("#make-payment").show();
                            $("#success_pay_message").text(
                                'ApplePay is possible on this browser, but not currently activated.');
                            $("#paymenterrormessagepopup").show();
                            return false;
                        }
                    });
                } else {
                    $('#payment_mode').val(''); // reset
                    $('#payment_div .payment-mode').children().removeClass("selected"); // remove selection
                    $(".preloader").fadeOut(1000);
                    $("#make-payment").show();
                    $("#applePaybtn").hide();
                    $("#success_pay_message").text('ApplePay is not available on this browser');
                    $("#paymenterrormessagepopup").show();
                    return false;
                    //if (_payment_ref_id == undefined) $("#package-payment-basic-form").submit();
                    //$("#make-payment").hide();
                    //$("#applePaybtn").show();
                }
                $('#payment_mode-error').hide();
            } else if ($(this).data("mode") == 'gpay') {}
        });

        function closepaymenterrorpop() {
            $("#success_pay_message").text("");
            $("#paymenterrormessagepopup").hide();
        }
        document.getElementById("applePaybtn").onclick = function(evt) {
            //$("#package-payment-basic-form").submit();
            //$("#payment_popup_applepay").hide();
            var ap_amount = $("#ap_amount").val();

            var runningAmount = ap_amount;
            var runningPP = 0;
            getShippingCosts('domestic_std', true);
            var runningTotal = function() {
                return runningAmount + runningPP;
            }
            var shippingOption = "";

            var subTotalDescr = "Payment against Cleaning Service";

            function getShippingOptions(shippingCountry) {
                logit('getShippingOptions: ' + shippingCountry);
                if (shippingCountry.toUpperCase() == "AE") {
                    shippingOption = [{
                        label: 'Standard Shipping',
                        amount: getShippingCosts('domestic_std', true),
                        detail: '3-5 days',
                        identifier: 'domestic_std'
                    }, {
                        label: 'Expedited Shipping',
                        amount: getShippingCosts('domestic_exp', false),
                        detail: '1-3 days',
                        identifier: 'domestic_exp'
                    }];
                } else {
                    shippingOption = [{
                        label: 'International Shipping',
                        amount: getShippingCosts('international', true),
                        detail: '5-10 days',
                        identifier: 'international'
                    }];
                }
            }

            function getShippingCosts(shippingIdentifier, updateRunningPP) {
                var shippingCost = 0;

                switch (shippingIdentifier) {
                    case 'domestic_std':
                        shippingCost = 0;
                        break;
                    case 'domestic_exp':
                        shippingCost = 0;
                        break;
                    case 'international':
                        shippingCost = 0;
                        break;
                    default:
                        shippingCost = 0;
                }

                if (updateRunningPP == true) {
                    runningPP = shippingCost;
                }

                logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);

                return shippingCost;
            }

            var paymentRequest = {
                currencyCode: 'AED',
                countryCode: 'AE',
                requiredShippingContactFields: ['postalAddress'],
                lineItems: [{
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    label: 'P&P',
                    amount: runningPP
                }],
                total: {
                    label: 'Elitemaids',
                    amount: runningTotal()
                },
                supportedNetworks: ['amex', 'masterCard', 'visa'],
                merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
            };

            var session = new ApplePaySession(1, paymentRequest);

            // Merchant Validation
            session.onvalidatemerchant = function(event) {
                logit(event);
                var promise = performValidation(event.validationURL);
                promise.then(function(merchantSession) {
                    session.completeMerchantValidation(merchantSession);
                });
            }


            function performValidation(valURL) {
                return new Promise(function(resolve, reject) {
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function() {
                        var data = JSON.parse(this.responseText);
                        logit(data);
                        resolve(data);
                    };
                    xhr.onerror = reject;
                    xhr.open('GET', 'https://elitemaids.emaid.info/applepay/apple_pay_comm.php?u=' +
                        valURL);
                    xhr.send();
                });
            }

            session.onshippingcontactselected = function(event) {
                logit('starting session.onshippingcontactselected');
                logit(
                    'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.'
                );
                logit(event);

                getShippingOptions(event.shippingContact.countryCode);

                var status = ApplePaySession.STATUS_SUCCESS;
                var newShippingMethods = shippingOption;
                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
            }

            session.onshippingmethodselected = function(event) {
                logit('starting session.onshippingmethodselected');
                logit(event);

                getShippingCosts(event.shippingMethod.identifier, true);

                var status = ApplePaySession.STATUS_SUCCESS;
                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completeShippingMethodSelection(status, newTotal, newLineItems);


            }

            session.onpaymentmethodselected = function(event) {
                logit('starting session.onpaymentmethodselected');
                logit(event);

                var newTotal = {
                    type: 'final',
                    label: 'Elitemaids',
                    amount: runningTotal()
                };
                var newLineItems = [{
                    type: 'final',
                    label: subTotalDescr,
                    amount: runningAmount
                }, {
                    type: 'final',
                    label: 'P&P',
                    amount: runningPP
                }];

                session.completePaymentMethodSelection(newTotal, newLineItems);


            }
            session.onpaymentauthorized = function(event) {
                logit('starting session.onpaymentauthorized');
                logit(
                    'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object'
                );
                logit(event.payment.token);
                var eventToken = JSON.stringify(event.payment.token.paymentData);
                var ap_amount = $("#ap_amount").val();
                var ap_billing_name = $("#ap_billing_name").val();
                var ap_billing_tel = $("#ap_billing_tel").val();
                var ap_billing_email = $("#ap_billing_email").val();
                var ap_order_id = $("#ap_order_id").val();
                //$("#package-payment-basic-form").submit();
                $.ajax({
                    method: 'post',
                    url: '{{ url('package/payment/applepay/checkout') }}',
                    data: {
                        'paymentToken': eventToken,
                        'ap_order_id': ap_order_id,
                        'ap_amount': ap_amount,
                        'ap_billing_name': ap_billing_name,
                        'ap_billing_tel': ap_billing_tel,
                        'ap_billing_email': ap_billing_email,
                        _token: '{{ csrf_token() }}'
                    },

                    success: function(result) {
                        logit(result);
                        if (result.status == 'success') {
                            session.completePayment(ApplePaySession.STATUS_SUCCESS);
                            window.location.href = '{{ url('package/payment/success') }}/' +
                                _payment_ref_id + '?transaction_id=' + result.transaction_id;
                        } else {
                            session.completePayment(ApplePaySession.STATUS_FAILURE);
                            window.location.href = '{{ url('package/payment/failed') }}/' +
                                _payment_ref_id;
                        }
                    }
                });
            }

            function sendPaymentToken(paymentToken) {
                return new Promise(function(resolve, reject) {
                    logit('starting function sendPaymentToken()');

                    logit(
                        "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;"
                    );
                    logit(
                        "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like"
                    );
                    if (debug == true)
                        resolve(true);
                    else
                        reject;
                });
            }

            session.oncancel = function(event) {
                window.location.href = '{{ url('package/payment/failed') }}/' + _payment_ref_id;
            }

            session.begin();

        };
    </script>
    <script>
        $('[data-mode]').click(function() {
            var amount = parseFloat($('#package-payment-basic-form input[name="amount"]').val());
            var transaction_charge = parseFloat($(this).attr('data-trans-charge'));
            if ($(this).attr('data-trans-charge-type') == 'P') {
                transaction_charge = (transaction_charge/100) * amount;
            }
            var total_amount = amount + transaction_charge;
            $('#package-payment-basic-form input[name="transaction_charge"]').val(transaction_charge);
            $('#package-payment-basic-form input[name="total_amount"]').val(total_amount);
            $('#package-payment-basic-form .transaction_charge').html(transaction_charge.toFixed(2));
            $('#package-payment-basic-form .total_amount').html(total_amount.toFixed(2));
        });
    </script>
@endpush
