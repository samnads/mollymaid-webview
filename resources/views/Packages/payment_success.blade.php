@extends('layouts.app')
@section('title')
    {{ 'Payment Success' }}
@endsection
@section('content')
    <div class="success">
        <section class="em-booking-content-section">
            <div class="container em-booking-content-box">
                <div class="row em-booking-content-main ml-0 mr-0">
                    <div class="col-12 em-booking-content pl-0 pr-0 min-vh-90 d-flex flex-column justify-content-center">
                        <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
                            <div class="row em-booking-content-set-main ml-0 mr-0">
                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pl-0 pr-0 d-sm-block d-lg-none">&nbsp;
                                </div>
                                <div class="col-lg-5 col-md-12 col-sm-12 success-left pl-0">

                                    <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">



                                        <div class="col-sm-12 success-message pr-0">

                                            <h3>We’ve received your service request<br><span>Please check your email</span><br>

                                                <span>Our operation team will reach out soon, to confirm your subscription.</span><br>
                                                <span>Ref Number :
                                                    <i>{{ @$data['subscription']['reference_id'] }}</i></span>
                                            </h3>

                                        </div>

                                        <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">

                                            <h6>CLIENT DETAILS</h6>

                                            <div class="col-sm-12 book-details-main service-type">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Name</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="name_p">{{ @$data['customer']['customer_name'] }}</p>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Email ID</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="mail_p">{{ @$data['customer']['email_address'] }}</p>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Contact Number</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ @$data['customer']['mobile_number_1'] }}</p>
                                                    </div>

                                                </div>

                                            </div>


                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Address</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p><span class="address_p">
                                                                @if ($data['customer_address'] != '')
                                                                    {{ @$data['customer_address']['customer_address'] }}
                                                                @else
                                                                    NA
                                                                @endif
                                                            </span>, <span
                                                                class="area_p">{{ @$data['area']['areaName'] }}</span></p>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Payment Mode</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ ucfirst(@$data['payment']['payment_mode']) }}
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-5 book-det-left pl-0 pr-0">
                                                        <p>Payment Status</p>
                                                    </div>

                                                    <div class="col-7 book-det-right pl-0 pr-0">
                                                        <p id="contact_p">{{ ucfirst(@$data['payment']['payment_status']) }}
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

                                            <h6>Service Details</h6>

                                            <div class="col-sm-12 book-details-main date-time-det mb-1">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-sm-12 book-det-left pl-0 pr-0">

                                                        <p>You've booked your package
                                                            <strong>{{ @$data['package']['package_name'] }}</strong> on

                                                            <?php
                                                            
                                                            $date = @$data['subscription']['service_date'];
                                                            
                                                            $old_date_timestamp = strtotime($date);
                                                            
                                                            $dateFormat = date('l jS, F', $old_date_timestamp);
                                                            
                                                            ?>

                                                            <strong class="date_p">{{ $dateFormat }}.</strong>

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 book-details-main-set pl-0 pr-0">

                                            <h6>Price Details</h6>

                                            <div class="col-sm-12 book-details-main">

                                                <div class="row ml-0 mr-0">

                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Service Fee</p>
                                                    </div>

                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p><span
                                                                class="total_p text-success">{{ number_format(@$data['payment']['amount'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                            @if(@$data['payment']['transaction_charge'] > 0)
                                            <div class="col-sm-12 book-details-main">
                                                <div class="row ml-0 mr-0">
                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Convenience Fee</p>
                                                    </div>
                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p><span
                                                                class="vat_p text-success">{{ number_format(@$data['payment']['transaction_charge'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-12 book-details-main">

                                                <div class="row total-price ml-0 mr-0">

                                                    <div class="col-7 book-det-left pl-0 pr-0">
                                                        <p>Total</p>
                                                    </div>

                                                    <div class="col-5 book-det-right pl-0 pr-0">
                                                        <p>AED <span
                                                                class="net_p text-success">{{ number_format(@$data['payment']['total_amount'], 2, '.', '') }}</span>
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-7 col-md-12 col-sm-12 success-right pr-0 d-none d-md-none d-lg-block">
                                    &nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-bottom-space">&nbsp;</div>
            <!--page bottom white space-->
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {

            $('.login').hide();

            $('.logout').show();

            $('.make_pay_head').show();

            $('.userPersonel').show();
        });
    </script>
@endpush
