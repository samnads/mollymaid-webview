<?php if(!isset($_GET['mobile'])) {
   ?>
<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head1 no-left-right-padding">
        <h2>Book Your Service</h2>
        <ul>
            <li class="active"><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
        </ul>
    </div>
</div>
<?php } ?>
<form id="step1Home" method="post">
    {{ csrf_field() }}
    <input type="hidden" value="{{ @$data['service']['service_rate'] }}" id="perhr" name="perhr">
    <input type="hidden" value="{{ @$data['service']['special_price'] }}" id="special_price" name="special_price">
    <input type="hidden" value="{{ @$data['service']['service_rate'] }}" id="original_price" name="original_price">
    <div class="col-12 em-booking-content-set pl-0 pr-0">
        <div class="row em-booking-content-set-main ml-0 mr-0">
            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                <div class="col-12 step1 pl-0 pr-0">
                    @include('common.services')
                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">
                        <div class="col-12 book-det-cont-set-main house-cleaning-service ser-cont1 ser-cont"
                            id="cont1">
                            <h5>{{ $data['service']['service_type_name'] }}</h5>
                            <div class="col-sm-12 em-field-main-set">
                                <div class="row m-0">
                                    <div class="col-sm-6 em-field-main" style="display:none;">
                                        <p>Number of Hours</p>
                                        <div class="col-12 em-text-field-main number-of-hours em-box-7 pl-0 pr-0">
                                            <input id="noOfHours" class="houseCleanHours" name="noOfHours"
                                                value="2">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 em-field-main" style="display: none;">
                                        <p>How many maids</p>
                                        <div class="col-12 em-text-field-main how-many-maids pl-0 pr-0">
                                            <ul class="clearfix">
                                                <li class="em-minus" onclick="removeNumber()">&nbsp;</li>
                                                <li class="num-maids">
                                                    <input name="noOfMaids" onkeyup="noofmaidchange()"
                                                        class="text-field" value="1" id="noOfMaids" type="text">
                                                </li>
                                                <li class="em-plus" onclick="addNumber();">&nbsp;</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 em-field-main" id="mainSupervisorDiv" style="display: none;">
                                        <p>With Supervisor ?</p>
                                        <div class="col-12 em-text-fild-main p-0 radio-checkbox-boxed">
                                            <ul>
                                                <li>
                                                    <input id="supervisor_status_y" value="Y"
                                                        name="supervisor_status" type="radio" disabled>
                                                    <label for="supervisor_status_y">YES</label>
                                                </li>
                                                <li>
                                                    <input id="supervisor_status_n" value="N"
                                                        name="supervisor_status" type="radio" checked>
                                                    <label for="supervisor_status_n">NO</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main-set">
                                <div class="row">
                                 <div class="col-sm-12 em-often-section-box">
                                    <div class="row m-0">
                                        <div class="col-sm-6 how-oten p-0 radio-checkbox-boxed" style="display:none;">
                                            <p>Sofa cleaning for ?</p>
                                            <ul class="checkbox-tabs">
                                                @foreach ($data['service_categories'] as $key => $service_category)
                                                <li>
                                                    <input id="category_{{$key}}" value="{{ $service_category['id'] }}" name="category" data-category-name="{{ $service_category['service_category_name'] }}" type="radio">
                                                    <label for="category_{{$key}}" style="width:145px;">{{ $service_category['service_category_name'] }}</label>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 how-oten p-0 radio-checkbox-boxed" style="display:none;">
                                            <p>Deep cleaning for</p>
                                            <ul class="checkbox-tabs">
                                                <li>
                                                    <input id="furnished_1" value="1" name="furnished"
                                                        type="radio" checked>
                                                    <label for="furnished_1" style="width:145px;">Furnished</label>
                                                </li>
                                                <li>
                                                    <input id="furnished_2" value="2" name="furnished"
                                                        type="radio">
                                                    <label for="furnished_2" style="width:145px;">Unfurnished</label>
                                                </li>
                                            </ul>
                                        </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 em-often-section-box pt-1">
                                       <div class="row m-0">
                                        <div class="col-sm-6 how-oten p-0 how-often home-type" style="display:none">
                                            <p>What is your building type?</p>
                                            <ul class="checkbox-tabs">
                                                @foreach ($data['service_sub_categories'] as $key => $service_sub_category)
                                                <li>
                                                    <input id="building_type_{{$key}}" value="{{ $service_sub_category['id'] }}" name="building_type"  data-sub-category-name="{{ $service_sub_category['sub_category_name'] }}" type="radio">
                                                    <label for="building_type_{{$key}}" style="width:145px;"><span></span>{{ $service_sub_category['sub_category_name'] }}</label>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 em-field-main radio-checkbox-boxed apartment_rooms" style="display:none">

                                        <p>No. of seats in sofa ?</p>

                                        <ul>
                                                @foreach ($data['service_category_costs'] as $key => $service_category_cost)
                                                @if($service_category_cost['sub_category_name'] == "Apartment")
                                                <li>
                                                    <input id="apartment_room_{{$key}}" value="{{ $service_category_cost['id'] }}" name="service_category" type="radio" data-hours="{{ $service_category_cost['hours'] }}" data-rate="{{ $service_category_cost['service_cost'] }}" data-room-name="{{ $service_category_cost['name'] }}">
                                                    <label for="apartment_room_{{$key}}">{{ $service_category_cost['name'] }}</label>
                                                </li>
                                                @endif
                                                @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 em-field-main radio-checkbox-boxed villa_rooms"
                                        style="display:none">

                                        <p>How many bedroom in your villa ?</p>

                                        <ul>
                                            @foreach ($data['service_category_costs'] as $key => $service_category_cost)
                                                @if($service_category_cost['sub_category_name'] == "Villa")
                                                <li>
                                                    <input id="villa_room_{{$key}}" value="{{ $service_category_cost['id'] }}" name="villa_room"
                                                        type="radio" data-hours="{{ $service_category_cost['hours'] }}" data-rate="{{ $service_category_cost['service_cost'] }}">
                                                    <label for="villa_room_{{$key}}">{{ $service_category_cost['name'] }}</label>
                                                </li>
                                                @endif
                                                @endforeach
                                        </ul>
                                    </div>
                                 </div>
                                </div>
                                    </div>
                                    
                            </div>
                            <div class="col-sm-12 em-field-main-set office_sqfts" style="display:none">
                                <div class="row m-0">
                                    <div class="col-sm-6 em-time-section-box pt-3">
                                        <p>What is the size of your office?</p>
                                        <select class="js-select2" id="office_sqft" name="office_sqft"
                                            onChange="amountCalculation();">
                                            <option value="" selected>Select Square Feet</option>
                                            @for ($i = 200; $i <= 5000; $i += 10)
                                                <option value="{{ $i }}"
                                                    data-sqft-rate="{{ $i <= 600 ? '.75' : '.5' }}" data-hours="5">
                                                    {{ $i }} square feet</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 em-field-main-set pb-3">
                                <div class="row m-0">

                                    <!--<div class="col-sm-12 em-field-main">
                              <p>Select Tools
                              
                              </p>
                              
                              
                              
                              <div class="col-sm-12 em-text-field-main p-0">
                                  <div class="row tool-select-main tools-no m-0">
                                      <div class="col-lg-3 col-md-6 col-sm-12 tool-select p-0"
                                          data-amount="10" data-input="cleaningVacuumStatus"
                                          data-action="add-tool" nclick="toggleToolsClass(this,'vaccum')">
                                          Vacuum Cleaner</div>
                                      <div class="col-lg-3 col-md-6 col-sm-12 tool-select p-0"
                                          data-amount="10" data-input="cleaningLadderStatus"
                                          data-action="add-tool" nclick="toggleToolsClass(this,'ladder')">
                                          Ladder</div>
                                      <div class="col-lg-3 col-md-6 col-sm-12 tool-select p-0"
                                          data-amount="10" data-input="cleaningMopStatus"
                                          data-action="add-tool" nclick="toggleToolsClass(this,'mop')">Mop
                                          Bucket</div>
                                      <div class="col-lg-3 col-md-6 col-sm-12 tool-select p-0"
                                          data-amount="10" data-input="cleaningIronStatus"
                                          data-action="add-tool"
                                          nclick="toggleToolsClass(this,'electriciron')">Electric Iron</div>
                                      <input type="hidden" data-type="tool" id="cleaningVacuumStatus"
                                          name="cleaningVacuumStatus" value="N">
                                      <input type="hidden" data-type="tool" id="cleaningLadderStatus"
                                          name="cleaningLadderStatus" value="N">
                                      <input type="hidden" data-type="tool" id="cleaningMopStatus"
                                          name="cleaningMopStatus" value="N">
                                      <input type="hidden" data-type="tool" id="cleaningIronStatus"
                                          name="cleaningIronStatus" value="N">
                                  </div>
                              
                              
                                  <div class="row tool-select-main tools-yes m-0" style="display: none;">
                                      <div class="col-md-6 col-sm-12 tool-select p-0 supplyClass"
                                          onclick="toggleSupplyClass(this,'standard')">Standard Cleaning
                                          Supplies</div>
                                      <div class="col-md-6 col-sm-12 tool-select p-0 supplyClass"
                                          onclick="toggleSupplyClass(this,'eco')">Ecological Green Clean
                                          Supplies</div>
                                      <input type="hidden" id="cleaningSupplytatus"
                                          name="cleaningSupplytatus" value="2">
                                  </div>
                                  <label id="cleaningSupplytatus-error" class="error"
                                      style="display: none;">Please select cleaning supply.</label>
                              </div>
                              
                              
                              </div>-->
                                </div>
                            </div>
                        </div>
                        @include('includes.service_expect_points')
                    </div>
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-12 mob-coupon_message p-0">
                                <div class="row coupon_message-main">
                                    <div class="small-close-btn"><img src="{{ asset('images/el-close-red.png') }}"
                                            alt=""></div>
                                    <span class="coupon_message"></span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->
                                <div class="col-12 sp-total-price-set pl-0 pr-0"> Total<br>
                                    <span>AED <b class="mobile_net_p"></b></span>
                                </div>
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Next" class="text-field-button show2-step" id="submit_button"
                                    type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('includes.steps.summaryNew')
        </div>
    </div>
</form>
<style>
    .em-box-7 li {
        width: 14.23%;
    }

    #applePaybtn {
        width: 150px;
        height: 45px;
        display: none;
        border-radius: 5px;
        margin-left: auto;
        margin-right: auto;
        background-image: -webkit-named-image(apple-pay-logo-white);
        background-position: 50% 50%;
        background-color: black;
        background-size: 60%;
        background-repeat: no-repeat;
    }
</style>
@push('scripts')
    <script>
        /********************************************* STEP 1 START ****************************************************/
        /*function toggleToolsClass(element, item) {
                                               var classe = 'col-lg-3 col-md-6 col-sm-12 tool-select p-0 unselect';
       
                                               if (element.className == classe) {
                                                   element.className = classe.replace('unselect', 'selected');
                                                   checkToolsItem(item);
                                               } else {
                                                   element.className = classe;
                                                   unCheckToolsItem(item);
                                               }
                                               var cleaningVacuumStatus = $('#cleaningVacuumStatus').val();
                                               var cleaningLadderStatus = $('#cleaningLadderStatus').val();
                                               var cleaningMopStatus = $('#cleaningMopStatus').val();
                                               var cleaningIronStatus = $('#cleaningIronStatus').val();
                                               if (cleaningVacuumStatus == 'Y' || cleaningLadderStatus == 'Y' || cleaningMopStatus == 'Y' ||
                                                   cleaningIronStatus == 'Y') {
                                                   $('.cleaning_tools_p').text('Yes');
                                               } else {
                                                   $('.cleaning_tools_p').text('No');
                                               }
                                           }*/
        

        function triggerToolsUnSelectAll() {
            $('input[data-type="tool"]').val("N");
            $('[data-action="add-tool"]').removeClass("selected");
            //$('.cleaningtoolsdiv_p').text(tools_rate);
            amountCalculation();
        }
        $('input[name="plan_based_supply"').change(function() {
            if (this.checked) {
                $("#cleaningSupplytatus-error").hide();
                $('input[name="custom_supplies[]"').prop('checked', false);
            }
            calcMaterialsRate();
        });



        $('.houseCleanHours').change(function() {
            calcSupervisorFee();
            calcMaterialsRate();
        });
        $('#noOfMaids').change(function() {
            calcSupervisorFee();
            calcMaterialsRate();
        });
        // toggle super visor
        $('#supervisordiv li').click(function(event) {
            var supervisor_status = $(this).attr("data-value");
            $('#supervisordiv li').removeClass("selected");
            $('.supervisorcharge_div').hide();
            //$('input[name="supervisorStatus"').val($(this).attr("data-value")).trigger("change");
            //$(this).addClass("selected");
            if (supervisor_status == "Y") {
                if ($('#noOfHours').val() > 2 && $('#noOfMaids').val() > 1) {
                    _supervisor_fee = hourly_supervisor_fee * $('#noOfHours').val();
                    $('#supervisordiv li[data-value="Y"]').addClass("selected");
                    $('.supervisorcharge_div').show();
                    $('input[name="supervisorStatus"').val("Y");
                    $('.super_visor_p').text("Yes");
                } else {
                    _supervisor_fee = 0;
                    $('#supervisordiv li[data-value="N"]').addClass("selected");
                    $('input[name="supervisorStatus"').val("N");
                    $('.super_visor_p').text("No");
                    $('.supervisorcharge_div').hide();
                }
            } else {
                $('input[name="supervisorStatus"').val("N");
                $('.super_visor_p').text("No");
                $('#supervisordiv li[data-value="N"]').addClass("selected");
                _supervisor_fee = 0;
            }
            $('input[name="supervisor_charge"').val(_supervisor_fee);
            $(".supervisorcharge_div_p").text(_supervisor_fee.toFixed(2));
            if ($('#noOfHours').val() > 2 && $('#noOfMaids').val() > 1) {
                $('#mainSupervisorDiv').show(500);
            } else {
                $('#mainSupervisorDiv').hide(500);
            }
            amountCalculation();
            localCalculate();
        });

        function calcSupervisorFee() {
            var supervisor_status = $('#supervisordiv li.selected').attr("data-value");
            $('#supervisordiv li[data-value="' + supervisor_status + '"]').trigger("click");
        }

        function calcMaterialsRate() {
            if ($('input[name="plan_based_supply"]').is(':checked')) {
                let hours = $('#noOfHours').val();
                let no_maids = parseInt($('#noOfMaids').val());
                hourly_material_fee = $('input[name="plan_based_supply"]:checked').attr("data-hourly-rate");
                _materials_rate = hourly_material_fee * hours * no_maids;
            } else {
                _materials_rate = 0;
            }
            $('.cleaningmaterialdiv_p').html(_materials_rate);
            amountCalculation();
        }
        // custome supplies toggled
        $('input[name="custom_supplies[]"').change(function() {
            tools_rate = 0;
            //alert(this.checked);
            //$('input[name="plan_based_supply"').prop('checked', false);
            $('.cleaning_tools_p').text('No');
            $('.cleaningtools_div').hide();
            $('input[name="custom_supplies[]"').each(function(index, element) {
                // check anything is selected
                if ($(element).is(':checked')) {
                    tools_rate += parseFloat($(element).attr("data-amount"));
                    $('.cleaning_tools_p').text('Yes');
                    $('.cleaningtools_div').show();
                }
            });
            $('.cleaningtoolsdiv_p').html(tools_rate);
            amountCalculation();
        });
        $('#needCleaningMaterial li').click(function(event) {
            $('#needCleaningMaterial li').removeClass("selected");
            $(this).addClass("selected");
            if ($(this).attr("data-value") == "Y") {
                $('#tools_mandatory').show();
                $('#plan_based_supplies').show();
                $('#custom_supplies').hide();
                $('input[name="custom_supplies[]"').prop('checked', false);
                $('.cleaning_tools_p').text('No');
                $('.cleaningtools_div').hide();
                $('.cleaning_material_p').text('Yes');
                $('.cleaningmaterial_div').show();
                $('.cleaningmaterialdiv_p').html("0.00");
                $("#cleaningSupplytatus").val('2');
                tools_rate = 0;
                $('.cleaningtoolsdiv_p').html(tools_rate);
                calcMaterialsRate();
            } else {
                $('#tools_mandatory').hide();
                $('#plan_based_supplies').hide();
                $('#custom_supplies').show();
                $('input[name="plan_based_supply"').prop('checked', false);
                $('.cleaningmaterial_div').hide();
                $('.cleaningmaterialdiv_p').html("0.00");
                $("#cleaningSupplytatus").val('2');
                $('.cleaning_material_p').text('No');
                $("#cleaningSupplytatus-error").css('display', 'none');
                calcMaterialsRate();
            }
            $('#cleaningMaterialStatus').val($(this).attr("data-value"));
            $('#cleaningMaterialStatus-error').hide();
            amountCalculation();
        });
        /*$('[data-action="add-tool"]').click(function(event) {
            $('.cleaning_tools_p').text('No'); // check and change on flow
            //$('.cleaningtools_div').hide();
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                $('input[name="' + $(this).attr("data-input") + '"').val("N");
            } else {
                $(this).addClass("selected");
                $('input[name="' + $(this).attr("data-input") + '"').val("Y");
            }
            tools_rate = 0;
            $('[data-action="add-tool"].selected').each(function(i, obj) {
                // check anything is selected
                if ($('input[name="' + $(this).attr("data-input") + '"').val() == "Y") {
                    tools_rate += parseFloat($(this).attr("data-amount"));
                    $('.cleaning_tools_p').text('Yes');
                    $('.cleaningtools_div').show();
                }
            });
            //$('.cleaningtoolsdiv_p').text(tools_rate);
            //amountCalculation();
        });*/
        /********************************************* STEP 1 END ****************************************************/
    </script>
@endpush
