@extends('layouts.app')

@section('title') {{'Carpet Cleaning Services Dubai'}} @endsection

@section('content')
<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
     <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          <input type="hidden" id="all_amount" value="{{serialize($allAmount)}}">
          <input type="hidden" id="number_hours" value="0">		  <input type="hidden" id="serviceNamePay" value="CarpetCleaning">
               @include('Carpet.steps')
          </div>
     </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>
@endsection
@push('scripts')
<script  type="text/javascript" src="{{URL::asset('js/select2.full.min.js')}}"></script>
<script>
 $.ajaxSetup({
     headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
$(document).ready(function() {
    $('#noOfHours').val(2);
	getAvailability();
    amountCalculation();
    $(".js-select2").select2();

});
function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
}


function amountCalculation()
{
    var furnished =  0;
    var service_id = $('#service_id').val();
    var is_scrubbing = 1;
    var carpet1Array = [];
   
    var coupon = $('.couponClass').val();
    var carpet = $('#carpet_count').val();
    var category =  $('#service_category').val();
    var bookedDate = $('#cleaning_date').val();
    
        carpet1Array.push({service_id : service_id,category_id : category,sub_category_id : '0',name : carpet,service_sub_funish_id : furnished,is_scrubbing : is_scrubbing,'status':1});
    

    $(".preloader").show();

    var intialArray = $('#all_amount').val();
    $.ajax({
          method: 'post',
          url: '{{url('calculate-carpet')}}',
          data:{'carpet1Array':carpet1Array,'intialArray':intialArray,'category':category,'officesqure':$('#officesqure').val(),'coupon':coupon,_token:'{{csrf_token()}}'},  
          success: function (result) {
               if (result.status == 'success') {
                $(".preloader").hide();

                   $('.total_p').text(result.data['amount']);
                   if(coupon != '' && coupon != 'undefined') {
                        checkCoupon();
                    } else {
                    $('.vat_p').text(result.data['vat_amount']);
                    $('.net_p').text(result.data['totalAmount']);
                    $('.mobile_net_p').text(result.data['totalAmount']);
                    $('.discount_div').hide();
                        $('#coupon_message').hide();
                    }
                   $('#carpetsize1').val(result.data['carpetsize1']);
                  
                   
               }
          }
     }); 
}

    function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement; 
    }



function toggleClass( element ) {
    var classe = 'col-lg-6 col-md-6 col-sm-6 col-6 unselect';

    if ( element.className == classe ){
        element.className = classe.replace('unselect', 'selected');
    } else {
        element.className = classe;
    }
}
</script>
@endpush

