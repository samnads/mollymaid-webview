<?php if(!isset($_GET['mobile'])) {
?>
<div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
    <div class="col-md-12 col-sm-12 em-step-head em-head no-left-right-padding">
        <h2>Book Your Service</h2>
        <ul>
            <li class="active"><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
            <li><span></span></li>
        </ul>
    </div> 
</div>
<?php } ?>
<form id="step1matressCleaning" method="post">
{{ csrf_field() }}
<input type="hidden" value="2" id="noOfHours" name="noOfHours">
    <div class="col-12 em-booking-content-set pl-0 pr-0">
        <div class="row em-booking-content-set-main ml-0 mr-0">
            <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                <div class="col-12 step1 pl-0 pr-0">
                    @include('common.services')
                    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">
                        <div class="col-12 book-det-cont-set-main mattress-cleaning-service  ser-cont6 ser-cont" style="display:block;" id="cont6">
                            <h5>Mattress Cleaning Services</h5>
                                
                                    <div class="col-sm-12 em-field-main-set"> 
                                    <div class="row m-0">
                                    
                                        <div class="col-sm-12 em-often-section-box pt-1 pl-0 pr-0">
                                            <div class="col-sm-12 how-often">
                                            <p>What is your mattress size?</p>
                                            <input type="hidden" id="mattress_hidden1" value="15">
                                            <input type="hidden" id="mattress_hidden2">
                                            <input type="hidden" id="mattress_hidden3">

                                            <ul>
                                                    <li>
                                                        <input id="mattress1" value="15" name="mattress" class="" checked="" type="checkbox" onclick="checkCheckBox(this.id,this.value)">
                                                        <label for="mattress1"> <span></span> &nbsp; Single (90 x 190 cm) </label>
                                                    </li>
                                                    
                                                    
                                                    <li>
                                                        <input id="mattress2" value="16" name="mattress" class="" type="checkbox" onclick="checkCheckBox2(this.id,this.value)">
                                                        <label for="mattress2"> <span></span> &nbsp; Queen (135 x 190 cm) </label>
                                                    </li>
                                                    
                                                    
                                                    <li>
                                                        <input id="mattress3" value="17" name="mattress" class="" type="checkbox" onclick="checkCheckBox3(this.id,this.value)">
                                                        <label for="mattress3"> <span></span> &nbsp; King (150 x 200 cm) </label>
                                                    </li>
                                                    
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        
                                    
                                        </div> 
                                </div>
                                    
                                    
                                    <div class="col-sm-12 em-field-main-set"> 
                                    <div class="row m-0">  
                                        
                                        
                                        <div class="col-sm-6 em-field-main" id="single_div">
                                            <p>How many <span>Single mattress</span> will be cleaned?</p>
                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                            <input type="hidden" id="single_hidden" value="3">
                                                    <ul class="clearfix only-valuebox

" id="single_ul">
                                                        <li>1</li>
                                                        <li>2</li>
                                                        <li class="selected">3</li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        
                                                    </ul>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        <div class="col-sm-6 em-field-main" id="queen_div"  style="display:none;">
                                            <p>How many <span>Queen mattress</span> will be cleaned?</p>
                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                            <input type="hidden" id="queen_hidden" value="2">
                                                    <ul class="clearfix only-valuebox

" id="queen_ul">
                                                        <li>1</li>
                                                        <li class="selected">2</li>
                                                        <li>3</li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        
                                                    </ul>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        <div class="col-sm-6 em-field-main" id="king_div"  style="display:none;">
                                            <p>How many <span>King mattress</span> will be cleaned?</p>
                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                            <input type="hidden" id="king_hidden" value="3">
                                                    <ul class="clearfix only-valuebox

" id="king_ul">
                                                        <li>1</li>
                                                        <li>2</li>
                                                        <li class="selected">3</li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        <li class="em-disable"></li>
                                                        
                                                    </ul>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        <div class="col-sm-6 em-field-main" id="double_div" style="display:none;">
                                            <p>How many <span>Double King mattress</span> will be cleaned?</p>
                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                            <input type="hidden" id="double_hidden" value="5">
                                                    <ul class="clearfix only-valuebox

" id="double_ul">
                                                        <li>1</li>
                                                        <li>2</li>
                                                        <li>3</li>
                                                        <li>4</li>
                                                        <li class="selected">5</li>
                                                        <li>6</li>
                                                        <li>7</li>
                                                        <li>8</li>
                                                        
                                                    </ul>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>   
                            <div class="col-sm-12 em-terms-and-condition-main"> 
                                <h6>What to expect from the service?</h6>
                                <div class="col-sm-12 em-terms-and-condition">
                                    <ul class="">
                                        <li>
                                            <div class="em-num">1</div>
                                            <div class="em-num-cont"><p>Removal of the dust mites through vacuuming and sanitization</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">2</div>
                                            <div class="em-num-cont"><p>Removal of stains and spots</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">3</div>
                                            <div class="em-num-cont"><p>Shampoo</p></div>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <div class="em-num">4</div>
                                            <div class="em-num-cont"><p>Complete disinfection and sanitization through vacuuming and shampooing</p></div>
                                            <div class="clear"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div> 
                        </div>    
                    <div class="col-12 em-booking-det-cont em-next-btn">
                        <div class="row em-next-btn-set ml-0 mr-0">
                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                <!--<span class="em-back-arrow " title="Previous Step"></span>-->
                                <div class="col-12 sp-total-price-set pl-0 pr-0">
                                    Total<br>
                                    <span>AED <b class="net_p"></b></span> 
                                </div>
                            
                            </div>
                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                <input value="Next" class="text-field-button show2-step" id="submit_button" type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             
            @include('includes.steps.summary')

        </div>
    </div>
</form>
<style>
#applePaybtn {  
	width: 150px;  
	height: 45px;  
	display: none;   
	border-radius: 5px;    
	margin-left: auto;
	margin-right: auto;
	background-image: -webkit-named-image(apple-pay-logo-white); 
	background-position: 50% 50%;
	background-color: black;
	background-size: 60%; 
	background-repeat: no-repeat;  
}
</style>
                                                      