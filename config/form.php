<?php

return [
    'phone_min_digit' => 9,
    'phone_max_digit' => 15,
];