<?php

namespace App\Http\Controllers;

use App\Mail\SupportMailToAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WebviewController extends Controller
{
    public function about_us()
    {
        return view('about-us', []);
    }
    public function terms_and_conditions()
    {
        return view('terms-and-conditions', []);
    }
    public function contact_form()
    {
        return view('contact', []);
    }
    public function contact_form_send(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $response = $client->request('POST', Config::get('api.api_url') . 'contact-us', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $request->all(),
        ]);
        $response = json_decode((string) $response->getBody(), true);
        if ($response['status'] == true) {
            try {
                Mail::send(new SupportMailToAdmin($request->all()));
            } catch (\Exception $e) {
                $response['status'] = false;
                $response['message'] = "Failed to send mail, please try again !";
                $response['error'] = $e->getMessage();
            }
        }
        return response()->json($response, 200, array(), JSON_PRETTY_PRINT);
    }
}
