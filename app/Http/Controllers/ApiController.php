<?php

namespace App\Http\Controllers;

use Request;
use Session;
use Config;

class ApiController extends Controller
{
    public function telr_process(Request $request, $payment_id)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        /******************************************** */
        $response = $client->request('POST', Config::get('url.api_url') . 'telr/process_order', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'payment_id' => $payment_id,
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $online_payment = $responseBody['online_payment'];
        if(Request::exists('test')){
            dd($responseBody);
        }
        if(@$responseBody['telr']['order']['status']['code'] == 3){
            return redirect('payment-success/' . $online_payment['reference_id'] . '/' . $online_payment['transaction_id']);
        }
        else{
            return redirect('payment-failed/' . $online_payment['reference_id'] . '/' . $online_payment['transaction_id']);
        }
    }
}
