<?php

namespace App\Http\Controllers;

class CCAvenueController extends Controller
{
    public function ccavRequestHandler()
    {
        return view('ccavRequestHandler', []);
    }
    public function ccavResponseHandler()
    {
        error_reporting(0);
        $workingKey = Config::get('values.ccavenue_workingkey'); //Working Key should be provided here.
        $encResponse = $_POST["encResp"]; //This is the response sent by the CCAvenue Server
        $rcvdString = ccavDecrypt($encResponse, $workingKey); //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        echo "<center>";
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) {
                $order_status = $information[1];
            }

        }
        parse_str($rcvdString, $params);
        /******************************************************************* */
        // update db and sgow success
        if ($order_status === "Success") {
           return redirect('payment-success/' . $params['order_id'] . '/' . $params['tracking_id']);

        } else if ($order_status === "Aborted") {
            return redirect('payment-error/' . $params['order_id'] . '/' . $params['tracking_id']);
        }
        /******************************************************************* */
        if ($order_status === "Success") {
            echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";

        } else if ($order_status === "Aborted") {
            echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";

        } else if ($order_status === "Failure") {
            echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
        } else {
            echo "<br>Security Error. Illegal access detected";

        }
        echo "<br><br>";
        echo "<table cellspacing=4 cellpadding=4>";
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            echo '<tr><td>' . $information[0] . '</td><td>' . $information[1] . '</td></tr>';
        }
        echo "</table><br>";
        echo "</center>";
    }
}
