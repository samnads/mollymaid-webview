<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\OnlineSuccessMailAdmin;
use App\Mail\OnlineSuccessMail;
use App\Mail\CancelBookingAdminMail;
use App\Mail\CancelBookingUserMail;
use App\Mail\SuccessMailAdmin;
use App\Mail\SuccessMail;
use Redirect;
class InvoiceController extends Controller
{
    public function invoivePayLink(Request $request)
    {
        if(isset($request->amount)){
            $amount = $request->amount;
        } else {
            $amount = 0;
        }
        if(isset($request->id)){
            $customerId = $request->id;
        } else {
            $customerId = 0;
        }
        if(isset($request->message)){
            $message = $request->message;
        } else {
            $message = 0;
        }
         if(isset($request->inv_id)){
            $inv_id = $request->inv_id;
        } else {
            $inv_id = 0;
        }
		
		$client = new \GuzzleHttp\Client([
            'verify' => false
        ]);    
        $response = $client->request('POST',Config::get('api.api_url').'get-customer-odoo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => $customerId,
            ],
            
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        // dd($responseBody);
        $responseStatus = $responseBody['status'];
		
		if($responseStatus == "success")
		{
			$customerId = $responseBody['data']['customerId'];
			return view('InvoicePay.make-payment',['customerId'=>$customerId,'amount'=>$amount,'message'=>$message,'inv_id'=>$inv_id]);
		} else {
			return view('NotFound.not_found');
		}
    }

    public function saveInvoicePay(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'save-invoice-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'inv_id'=>$request->input('inv_id'),
                'customerId'=>$request->input('customerId'),
                'amount'=>$request->input('amount'),
                'description'=>$request->input('description')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return view('InvoicePay.confirm-payment',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }
    }

    public function invoicePaymentSuccess(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true);
            if($result != null) {
                $trackId = $result['id'];
                $payId = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'invoice-payment-success', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'payId' => $payId, 
                        'trackId' => $trackId,
                        'payment_type' => 'card',
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
        
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == "success")
				{
					$paymentData = $responseBody['data']['paymentData'];
					$customer = $responseBody['data']['customer'];
					$customerAddress =  $responseBody['data']['customer_address'];
					$areaName = $responseBody['data']['areaName'];
					$services = $responseBody['data']['serviceName'];
					Mail::send(new OnlineSuccessMail($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					Mail::send(new OnlineSuccessMailAdmin($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					return view('common.success',['data'=>$responseBody['data']]);
                } else if($responseStatus == 'refresh_success') {
                    return redirect('/');
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
        
    }
	
	public function invoicePaymentSuccessTest(Request $request)
    {
       $trackId = 'pay_vjzgnmcfpjckpa2uzikwyjbvou';
                $payId = 8762;
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'invoice-payment-success', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'payId' => $payId, 
                        'trackId' => $trackId,
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
        
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == 'success') 
				{
					$paymentData = $responseBody['data']['paymentData'];
					$customer = $responseBody['data']['customer'];
					$customerAddress =  $responseBody['data']['customer_address'];
					$areaName = $responseBody['data']['areaName'];
					$services = $responseBody['data']['serviceName'];
					Mail::send(new OnlineSuccessMail($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					Mail::send(new OnlineSuccessMailAdmin($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					return view('common.success',['data'=>$responseBody['data']]);
                } else if($responseStatus == 'refresh_success') {
                    return redirect('/');
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
        
    }
	
    public function invoicePaymentFailed(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true); 
            $trackId = $result['id'];
            $payId = $result['reference'];
            if($result != null) { 
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'invoice-payment-failed', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'payId' => $payId, // hard coded service id for cleaning
                        'trackId' => $trackId,
                        'payment_type' => 'card',
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == 'success') {
                    return view('common.fail',['data'=>$responseBody['data']]);
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        }  else {
            return view('NotFound.not_found');
        }
       
    }
    public function onlineErrorFailed($ref,$id)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'invoice-payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'payId' => $ref, // hard coded service id for cleaning
                'trackId' => $id,
				'payment_type' => 'card',
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
             return view('NotFound.not_found');
        }
    }
    public function bookinglistPayment(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'save-bookinglist-pay', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'is_device'=>'web', 
                'bookingId'=>$request->input('bookingId')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => $responseBody['messages'],
                    'data' => $responseBody['data']
                ]
            );
            // return view('Payment.confirm-payment',['data'=>$responseBody['data']]);
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        }   
    }
    public function cancelBooking(Request $request)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('POST', Config::get('api.api_url').'cancel-booking', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'bookingId'=>$request->input('bookingId'),
                'customerId'=>session('customerId'),
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];
        if($responseStatus == 'success') {
               $data = $responseBody['data'];
            try {
                Mail::send(new CancelBookingAdminMail($data['customer_email'],$data));
                Mail::send(new CancelBookingUserMail($data['customer_email'],$data));
    
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => $responseBody['messages'],
                    ]
                );
            } catch (\Exception $e) {
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => $responseBody['messages'],
                    ]
                );      
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $responseBody['messages'],
                ]
            );
        } 
    }

    public function payment(Request $request)
    { 
        $retryOnline = $request['retry_online'];
        $mobileParam = $request['mobile_view'];
        $data=array();
        $data['source']= array("type" => "token","token" => $request['token_req']);
        $data['customer']= array("email" => $request['customer_email'],"name" => $request['customer_name']);
        $data['3ds']= array("enabled" => true);
        // $data['amount']= $request['amount'];
        // $amt = 1;
        $data['amount']= (int)($request['amount'] * 100);
        $data['currency']= 'AED';
        $data['reference']= $request['order_id'];
        if($retryOnline == 'invoice') {
            $data['success_url']= Config::get('values.checkout_invoice_success').$mobileParam;
            $data['failure_url']= Config::get('values.checkout_invoice_fail').$mobileParam;
            $urlVal = 'payment-invoice-failed';
        } 
        $post_datas=json_encode($data);
        $secret_key=Config::get('values.checkout_secret_key');
        $ServiceURL=Config::get('values.checkout_service_url');
        $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
        $orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_datas);
        $result=json_decode($orderCreateResponse,true);
        if(!isset($result['error_type'])) {
            $status=$result['status'];
            if ($status == 'Pending') { 
                $redirectUrl = $result['_links']['redirect']['href'];
                return Redirect::to($redirectUrl);
            } else {
                if($mobileParam != '') {
                    return Redirect::to($urlVal.'/'.$result['reference'].'/'.$result['id'].'?mobile=active');
                } else {
                    return Redirect::to($urlVal.'/'.$result['reference'].'/'.$result['id']);
                }
            }
        } else{
            if($mobileParam != '') {
                return Redirect::to($urlVal.'/'.$request['order_id'].'/'.$result['request_id'].'?mobile=active');
            } else {
                return Redirect::to($urlVal.'/'.$request['order_id'].'/'.$result['request_id']);
            }
        }
    }
	
	public function invoiceGpayPaymentSuccess(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true);
            if($result != null) {
                $trackId = $result['id'];
                $payId = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'invoice-payment-success', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'payId' => $payId, 
                        'trackId' => $trackId,
                        'payment_type' => 'gpay',
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
        
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == "success")
				{
					$paymentData = $responseBody['data']['paymentData'];
					$customer = $responseBody['data']['customer'];
					$customerAddress =  $responseBody['data']['customer_address'];
					$areaName = $responseBody['data']['areaName'];
					$services = $responseBody['data']['serviceName'];
					Mail::send(new OnlineSuccessMail($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					Mail::send(new OnlineSuccessMailAdmin($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
					return view('common.success',['data'=>$responseBody['data']]);
                } else if($responseStatus == 'refresh_success') {
                    return redirect('/');
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
        
    }
	
	public function appleInvoicePaymentSuccess($ref,$id)
    {
		$trackId = $id;
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
			'verify' => false
		]);
		$response = $client->request('post', Config::get('api.api_url').'invoice-payment-success', [
			'headers' => [
				'cache-control' => 'no-cache',
				'Content-Type' => 'application/x-www-form-urlencoded'
			],
			'form_params' => [
				'payId' => $referenceNo, 
				'trackId' => $trackId,
				'payment_type' => 'applepay',
			],
		]);
		$responseBody = json_decode((string) $response->getBody(), true);

		$responseStatus = $responseBody['status'];

		if($responseStatus == "success")
		{
			$paymentData = $responseBody['data']['paymentData'];
			$customer = $responseBody['data']['customer'];
			$customerAddress =  $responseBody['data']['customer_address'];
			$areaName = $responseBody['data']['areaName'];
			$services = $responseBody['data']['serviceName'];
			Mail::send(new OnlineSuccessMail($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
			Mail::send(new OnlineSuccessMailAdmin($customer['email_address'],$customer,$customerAddress,$areaName,$services,$paymentData));
			return view('common.success',['data'=>$responseBody['data']]);
		} else if($responseStatus == 'refresh_success') {
			return redirect('/');
		} else {
			return response()->json(
				[
					'status' => 'failed',
					'messages' => $responseBody['message'],
				]
			);
		}
    }

    public static function curlFun($url,$Headers,$post_datas)
    {
        $ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_datas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }
	public static function curlFunOdoo($curl_post,$url)
    {
        $curl_header = array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($curl_post)
		);
		$ch2 = curl_init();
		curl_setopt($ch2, CURLOPT_URL, $url);
		curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch2, CURLOPT_POST, 1);
		curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
		curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
		$user_datas = curl_exec($ch2);
		
		if (curl_errno($ch2)) {
			echo $error_msg = curl_error($ch2);
			exit();
		}
		
		return $user_datas;
    }
	
    public function paymentSuccess(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true);
            if($result != null) { 
                $trackId = $result['id'];
                $referenceNo = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'payment-success', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'ref_no' => $referenceNo, // hard coded service id for cleaning
                        'trackId' => $trackId,
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == 'success') {
                    try {
                        $customer = $responseBody['data']['customer'];
                        $customerAddress =  $responseBody['data']['customer_address'];
                        $bookings =  $responseBody['data']['bookings'];
                        $areaName = $responseBody['data']['areaName'];
                        $services = $responseBody['data']['services'];
                        $online = $responseBody['data']['online'];
                        Mail::send(new SuccessMail($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
                        Mail::send(new SuccessMailAdmin($customer['email_address'],$customer,$bookings,$customerAddress,$areaName,$services,$online));
                        return view('common.success',['data'=>$responseBody['data']]);
                    } catch (\Exception $e) {
                        return view('common.success',['data'=>$responseBody['data']]);
                    }
                } else if($responseStatus == 'refresh_success') {
                    return redirect('/');
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
    }
    public function errorFailed($ref,$id)
    {
        $trackId = $id;
        $referenceNo = $ref;
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'ref_no' => $referenceNo, // hard coded service id for cleaning
                'trackId' => $trackId,
            ],
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $responseStatus = $responseBody['status'];

        if($responseStatus == 'success') {
            return view('common.fail',['data'=>$responseBody['data']]);
        } else {
            return view('NotFound.not_found');
        }
    }
    public function paymentFailed(Request $request)
    {
        if(isset($_GET['cko-session-id'])) {
            $sessionID = $_GET['cko-session-id'];
            $secret_key=Config::get('values.checkout_secret_key');
            $ServiceURL=Config::get('values.checkout_service_url').$sessionID;
            $Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
            $orderCreateResponse = $this->curlFunget($ServiceURL, $Headers);
            $result=json_decode($orderCreateResponse,true);
            if($result != null) { 
                $trackId = $result['id'];
                $referenceNo = $result['reference'];
                $client = new \GuzzleHttp\Client([
                    'verify' => false
                ]);
                $response = $client->request('post', Config::get('api.api_url').'payment-failed', [
                    'headers' => [
                        'cache-control' => 'no-cache',
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'form_params' => [
                        'ref_no' => $referenceNo, // hard coded service id for cleaning
                        'trackId' => $trackId,
                    ],
                ]);
                $responseBody = json_decode((string) $response->getBody(), true);
                $responseStatus = $responseBody['status'];
        
                if($responseStatus == 'success') {
                    return view('common.fail',['data'=>$responseBody['data']]);
                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'messages' => $responseBody['message'],
                        ]
                    );
                }
            } else{
                return redirect('house-cleaning-dubai');
            }
        } else {
            return view('NotFound.not_found');
        }
    }
    public static function curlFunget($url,$Headers)
    {
        $ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
        }
        curl_close($ch);

        return $response;
    }
    public function checkOdoo(Request $request)
    {
        

	
	
	             	$post['params']['user_id'] =(int)1;
					$post['params']['inv_id'] = 12;
					$post['params']['amount'] = 630;
					$post['params']['comment'] = 'test';
					$post['params']['date'] = '2021-12-08';
					$post['params']['transaction_id'] ='test';
					$post_values=json_encode($post);

					$url = Config::get('values.odoo_url')."create_payment";
					$login_check = $this->curl_api_service($post_values,$url);
					$returnData=json_decode($login_check);
					
					
        // $client_odoo = new \GuzzleHttp\Client([
        //                         'verify' => false
        //                     ]);
        //                     $response_odoo = $client_odoo->request('post', Config::get('values.odoo_url').'create_payment', [
        //                         'headers' => [
        //                             'cache-control' => 'no-cache',
        //                             'Content-Type' => 'application/x-www-form-urlencoded'
        //                         ],
        //                         'form_params' => [
        //                             'user_id' => (int)1, 
        //                             'inv_id' => 12,
        //                             'amount' => 630,
        //                             'comment' => 'test',
        //                             'date' => '2021-12-08',
        //                             'transaction_id' => 'test',

        //                         ],
        //                     ]);
        //                     $responseBody_odoo = json_decode((string) $response_odoo->getBody(), true);
        //             dd($responseBody_odoo);
        //                     $responseStatusOdoo = $responseBody_odoo['status'];
    }
    function curl_api_service($curl_post)
    {
        	$curl_header = array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($curl_post)
	);
	$ch2 = curl_init();
	curl_setopt($ch2, CURLOPT_URL,Config::get('values.odoo_url').'create_payment');
	curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch2, CURLOPT_POST, 1);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
	curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
	$user_datas = curl_exec($ch2);
	
	if (curl_errno($ch2)) {
		echo $error_msg = curl_error($ch2);
		exit();
	}
	
	return $user_datas;
    }
    
    
}
