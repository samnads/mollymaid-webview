<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;


class SuccessMailAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $customer;
    public $bookings;
    public $customerAddress;
    public $areaName;
    public $services;
    public $online;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$customer,$bookings,$customerAddress,$areaName,$services,$online,$data)
    {
        $this->email = $email;
        $this->customer = $customer;
        $this->bookings = $bookings;
        $this->customerAddress = $customerAddress;
        $this->areaName = $areaName;
        $this->services = $services;
        $this->online = $online;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            //->to(Config::get('values.mail_admin'),'Admin')
            ->to('samnad.s@azinova.info','Admin')
            ->subject('Cleaning Booking Confirmation - '.$this->bookings['reference_id'])
            ->view('emails.booking_admin_template_email');  
    }
}
