<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class RatingConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $booking;
    public $serviceDate;
    public $rating;
    public $review;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($booking,$serviceDate,$rating,$review)
    {
        $this->booking = $booking;
        $this->serviceDate = $serviceDate;
        $this->rating = $rating;
        $this->review = $review;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'),Config::get('values.mail_name'))
            ->to(Config::get('mail.feedback_to'),'Admin')
            ->bcc('samnad.s@azinova.info', 'Developer BCC') // developer
            ->subject('New Feedback Received')
            ->view('emails.rating_confirmation');  
    }
}
