<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class SupportMailToAdmin extends Mailable
{
    use Queueable, SerializesModels;
    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), Config::get('values.mail_name'))
            //->to(Config::get('values.mail_admin'), 'Admin')
            ->bcc('samnad.s@azinova.info', 'Developer BCC') // developer
            ->subject('New Support Message')
            ->view('emails.support_admin_template');
    }
}
